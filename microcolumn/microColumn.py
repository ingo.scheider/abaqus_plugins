# -*- coding: mbcs -*-
from part import *
from material import *
from section import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from optimization import *
from job import *
from sketch import *
from visualization import *
from connectorBehavior import *
##
from math import sqrt
#
#
#
def microcolumn(contactFlag,model3d,angle3d,footradius,topradius,columnheight,
        bottomradius,bottomheight,filletradius,loadvalue, inputname):
    meshsize=topradius/10.
    totalheight=bottomheight+columnheight
    #
    # COLUMN
    mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
    mdb.models['Model-1'].sketches['__profile__'].ConstructionLine(point1=(0.0, 
        -100.0), point2=(0.0, 100.0))
    mdb.models['Model-1'].sketches['__profile__'].Line(point1=(0.0, 0.0), point2=(
        bottomradius, 0.0))
    mdb.models['Model-1'].sketches['__profile__'].Line(point1=(bottomradius, 0.0), point2=(
        bottomradius, bottomheight))
    mdb.models['Model-1'].sketches['__profile__'].Line(point1=(bottomradius, bottomheight), point2=
        (footradius, bottomheight))
    mdb.models['Model-1'].sketches['__profile__'].Line(point1=(footradius, bottomheight), point2=
        (topradius, columnheight+bottomheight))
    mdb.models['Model-1'].sketches['__profile__'].Line(point1=(topradius, totalheight), point2=(
        0.0, columnheight+bottomheight))
    mdb.models['Model-1'].sketches['__profile__'].Line(point1=(0.0, totalheight), point2=(
        0.0, 0.0))
    mdb.models['Model-1'].sketches['__profile__'].FilletByRadius(curve1=
        mdb.models['Model-1'].sketches['__profile__'].geometry.findAt(((bottomradius+footradius)/2., bottomheight), ), 
        curve2=mdb.models['Model-1'].sketches['__profile__'].geometry.findAt(((footradius+topradius)/2., bottomheight+columnheight/2.), ), 
        nearPoint1=(footradius+filletradius, bottomheight), 
        nearPoint2=(footradius, bottomheight+filletradius), radius=filletradius)
    if model3d:
        columnPart=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='column', type=
            DEFORMABLE_BODY)
        columnPart.BaseSolidRevolve(angle=angle3d, 
            flipRevolveDirection=OFF, sketch=mdb.models['Model-1'].sketches['__profile__'])
        mdb.models['Model-1'].parts['column'].Set(cells=columnPart.cells[:], name='column')
    else:
        columnPart=mdb.models['Model-1'].Part(dimensionality=AXISYMMETRIC, name='column', 
            type=DEFORMABLE_BODY)
        columnPart.BaseShell(sketch=mdb.models['Model-1'].sketches['__profile__'])
        columnPart.Set(faces=columnPart.faces[:], name='column')
    del mdb.models['Model-1'].sketches['__profile__']
    #
    # PUNCH
    if contactFlag:
        mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
        mdb.models['Model-1'].sketches['__profile__'].ConstructionLine(point1=(0.0, 
            -100.0), point2=(0.0, 100.0))
        mdb.models['Model-1'].sketches['__profile__'].Line(point1=(0.0, 0.0), point2=(
            bottomradius, 0.0))
        if model3d:
            punchPart=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='punch', type=
                ANALYTIC_RIGID_SURFACE)
            punchPart.AnalyticRigidSurfRevolve(sketch=mdb.models['Model-1'].sketches['__profile__'])
        else:
            punchPart=mdb.models['Model-1'].Part(dimensionality=AXISYMMETRIC, name='punch', 
                type=ANALYTIC_RIGID_SURFACE)
            punchPart.AnalyticRigidSurf2DPlanar(sketch=mdb.models['Model-1'].sketches['__profile__'])
        del mdb.models['Model-1'].sketches['__profile__']
    #
    #
    #
    mdb.models['Model-1'].Material(name='composite')
    mdb.models['Model-1'].materials['composite'].Hyperelastic(materialType=
        ISOTROPIC, table=((29000.0, 0.2, 1e-06), ), testData=OFF, type=OGDEN, 
        volumetricResponse=VOLUMETRIC_DATA)
    mdb.models['Model-1'].HomogeneousSolidSection(material='composite', name=
        'column', thickness=None)
    mdb.models['Model-1'].parts['column'].SectionAssignment(offset=0.0, 
        offsetField='', offsetType=MIDDLE_SURFACE, region=columnPart.sets['column'], 
        sectionName='column', thicknessAssignment=FROM_SECTION)
    #
    # ASSEMBLY
    #
    mdb.models['Model-1'].rootAssembly.DatumCsysByDefault(CARTESIAN)
    asm=mdb.models['Model-1'].rootAssembly
    columnInstance=asm.Instance(dependent=ON, name='column-1', part=columnPart)
    if contactFlag:
        punchInstance=asm.Instance(dependent=ON, name='punch-1', part=punchPart)
        asm.translate(instanceList=('punch-1', ), vector=(0.0, totalheight, 0.0))
    #
        if model3d:
            punchFace=punchInstance.faces.findAt(((bottomradius/2., totalheight, bottomradius/2.),(0.0, 1.0, 0.0)),)
            asm.Surface(name='m_Surf-1', side2Faces=punchFace)
        else:
            punchFace=punchInstance.edges.findAt(((bottomradius/2., totalheight, 0.0 ),(0.0, 1.0, 0.0)),)
            asm.Surface(name='m_Surf-1', side2Edges=punchFace)
        refPoint=asm.ReferencePoint(point=punchInstance.vertices.findAt((0.0, totalheight, 0.0), ))
        mdb.models['Model-1'].RigidBody(name='Constraint-1', refPointRegion=Region(
            referencePoints=(asm.referencePoints[refPoint.id], )), surfaceRegion=Region(side2Faces=punchFace) )
    else:
        refPoint=asm.ReferencePoint(point=columnInstance.vertices.findAt((topradius, totalheight, 0.0), ))
    asm.Set(name='loadpoint', referencePoints=(asm.referencePoints[refPoint.id], ) )
    if model3d:
        localCSYS=asm.DatumCsysByThreePoints(coordSysType=CYLINDRICAL, name='cylinderCSYS', 
            origin=(0.0, 0.0, 0.0), 
            point1=columnInstance.vertices.findAt((bottomradius, 0.0, 0.0), ), 
            point2=columnInstance.vertices.findAt((topradius, totalheight, 0.0), ))
    else:
        localCSYS=None
    #
    # STEP
    #
    mdb.models['Model-1'].StaticStep(initialInc=0.01, maxInc=0.01, maxNumInc=10000, 
        minInc=1e-12, name='Step-1', nlgeom=ON, previous='Initial')
    #
    # CONTACT
    #
    if model3d:
        asm.Surface(name='columnTopsurf', side1Faces=columnInstance.faces.findAt(((
            topradius/2., totalheight, topradius/2.), )))
    else:
        asm.Surface(name='columnTopsurf', side1Edges=columnInstance.edges.findAt(((
            topradius/2., totalheight, 0.0), )))
    if contactFlag:
        mdb.models['Model-1'].ContactProperty('IntProp-1')
        mdb.models['Model-1'].interactionProperties['IntProp-1'].TangentialBehavior(
            dependencies=0, directionality=ISOTROPIC, elasticSlipStiffness=None, 
            formulation=PENALTY, fraction=0.005, maximumElasticSlip=FRACTION, 
            pressureDependency=OFF, shearStressLimit=None, slipRateDependency=OFF, 
            table=((0.1, ), ), temperatureDependency=OFF)
        mdb.models['Model-1'].SurfaceToSurfaceContactStd(adjustMethod=TOLERANCE, 
            adjustTolerance=1e-05, clearanceRegion=None, createStepName='Step-1', 
            datumAxis=None, initialClearance=OMIT, interactionProperty='IntProp-1', 
            master=asm.surfaces['m_Surf-1'], name='contact', 
            slave=asm.surfaces['columnTopsurf'], sliding=SMALL, thickness=ON, tied=OFF)
    #
    else:
        mdb.models['Model-1'].Coupling(controlPoint=asm.sets['loadpoint'], couplingType=KINEMATIC, 
            influenceRadius=WHOLE_SURFACE, localCsys=None, name='Constraint-2', 
            surface=asm.surfaces['columnTopsurf'], 
            u1=OFF, u2=ON, u3=OFF, ur1=OFF, ur2=OFF, ur3=OFF)
    #
    # BOUNDARY CONDITIONS
    #
    if model3d:
        asm.Set(faces=columnInstance.faces.findAt(((bottomradius/2., bottomheight/2., 0.0), ), ), 
            name='symmBC2' )
        mdb.models['Model-1'].YsymmBC(createStepName='Step-1', localCsys=asm.datums[localCSYS.id], 
            name='BC-1', region=Region(
            faces=columnInstance.faces.findAt(((0.0, bottomheight/2., bottomradius/2.), ), )))
        mdb.models['Model-1'].YsymmBC(createStepName='Step-1', localCsys=asm.datums[localCSYS.id], 
            name='BC-2', region=asm.sets['symmBC2'])
        mdb.models['Model-1'].ZsymmBC(createStepName='Step-1', localCsys=asm.datums[localCSYS.id], 
            name='BC-3', region=Region(
            faces=columnInstance.faces.findAt(((bottomradius/2., 0.0, bottomradius/2.), ), )))
        mdb.models['Model-1'].XsymmBC(createStepName='Step-1', localCsys=asm.datums[localCSYS.id],
            name='BC-4', region=Region(
            faces=columnInstance.faces.findAt(
            ((bottomradius/2., bottomheight/2., bottomradius*sqrt(3)/2.), ), )))
    else:
        mdb.models['Model-1'].YsymmBC(createStepName='Step-1', localCsys=None, 
            name='BC-3', region=Region(
            edges=columnInstance.edges.findAt(((bottomradius/2., 0.0, 0.0), ), )))
        mdb.models['Model-1'].XsymmBC(createStepName='Step-1', localCsys=None,
            name='BC-4', region=Region(
            edges=columnInstance.edges.findAt(((bottomradius, bottomheight/2., 0.0), ), )))
    mdb.models['Model-1'].DisplacementBC(amplitude=UNSET, createStepName='Step-1', 
        distributionType=UNIFORM, fieldName='', fixed=OFF, 
        localCsys=None, name='loadpoint', region=asm.sets['loadpoint'], 
        u1=0.0, u2=-loadvalue, u3=0.0, ur1=0.0, ur2=0.0, ur3=0.0)
    #
    # PARTITIONING AND MESHING
    #
    edgeCenterY=bottomheight+filletradius*(1-sqrt(0.5))
    edgeCenterR=footradius+filletradius*(1-sqrt(0.5))
    if model3d:
        filletEdge=columnPart.edges.getClosest(coordinates=((0.0, edgeCenterY, edgeCenterR),) )[0]
        columnPart.PartitionEdgeByParam(edges=filletEdge[0], parameter=0.5)
        filletPoint=columnPart.vertices.getClosest(coordinates=((0., edgeCenterY, edgeCenterR),))[0]
        columnPart.PartitionCellByPlanePointNormal(cells=columnPart.cells[0], 
            normal=columnPart.datums[1], 
            point=filletPoint[0] )
        edgeCenterX=edgeCenterR*sqrt(0.5)
        edgeCenterZ=edgeCenterR*sqrt(0.5)
        filletEdge=columnPart.edges.getClosest(coordinates=((edgeCenterX, edgeCenterY, edgeCenterZ),) )[0]
        columnPart.PartitionCellByExtrudeEdge(
            cells=columnPart.cells.findAt(((bottomradius/2., bottomheight/2., bottomradius/2.), )), 
            edges=filletEdge[0], line=columnPart.datums[1], sense=FORWARD)
    else:
        filletEdge=columnPart.edges.getClosest(coordinates=((0.0, edgeCenterY, 0.0),) )[0]
        paramValue=bottomheight/float(totalheight)
	print repr(paramValue)
        columnPart.PartitionEdgeByParam(edges=filletEdge[0], parameter=paramValue)    
    #
    meshsizeFine=meshsize/3.
    meshsizeCoarse=meshsize*2.
    columnPart.seedPart(deviationFactor=0.1, 
        minSizeFactor=0.1, size=meshsize)
    if model3d:
        columnPart.setMeshControls(algorithm=ADVANCING_FRONT, 
            elemShape=HEX_DOMINATED, regions=columnPart.cells[:], 
            technique=SWEEP)
        columnPart.setElementType(elemTypes=(ElemType(elemCode=C3D8H, elemLibrary=STANDARD), 
            ElemType(elemCode=C3D6, elemLibrary=STANDARD), 
            ElemType(elemCode=C3D4, elemLibrary=STANDARD)), 
            regions=Region(cells=columnPart.cells[:] ) )
        columnPart.setSweepPath(edge=columnPart.edges.findAt((0., bottomheight+columnheight/2., 0.), ), 
            region=columnPart.cells.findAt((topradius/2., bottomheight+columnheight/2., topradius/2.), ), 
            sense=FORWARD)
        columnPart.setSweepPath(edge=columnPart.edges.findAt((0., bottomheight/2., 0.), ), 
            region=columnPart.cells.findAt((topradius/2., bottomheight/2., topradius/2.), ), 
            sense=FORWARD)
        columnPart.seedEdgeByBias(biasMethod=SINGLE, constraint=FINER, 
            end1Edges=columnPart.edges.findAt((( 0.0, bottomheight+columnheight/2., (topradius+footradius)/2.), ), ), 
            maxSize=meshsize, minSize=meshsizeFine)
        columnPart.seedEdgeBySize(constraint=FINER, 
            deviationFactor=0.1, edges=(columnPart.edges.findAt((0., 0., (footradius+bottomradius)/2.),),),
            minSizeFactor=0.1, size=meshsizeCoarse)
        columnPart.seedEdgeBySize(constraint=FINER, 
            deviationFactor=0.1, edges=(columnPart.edges.findAt((0., bottomheight, (footradius+bottomradius)/2.),),),
            minSizeFactor=0.1, size=meshsizeCoarse)
    else:
        columnPart.setElementType(elemTypes=(ElemType(elemCode=CAX4H, elemLibrary=STANDARD), 
            ElemType(elemCode=CAX3, elemLibrary=STANDARD)), 
            regions=(columnPart.faces[:], ) )
        columnPart.seedEdgeBySize(constraint=FINER, 
            deviationFactor=0.1, edges=(columnPart.edges.findAt((topradius/2., totalheight, 0.0),),),
            minSizeFactor=0.1, size=meshsizeFine)
        columnPart.seedEdgeBySize(constraint=FINER, 
            deviationFactor=0.1, edges=(columnPart.edges.findAt((topradius/2., 0.0, 0.0),),),
            minSizeFactor=0.1, size=meshsizeCoarse)
    columnPart.seedEdgeByBias(biasMethod=SINGLE, 
        constraint=FINER, end1Edges=columnPart.edges.findAt(((0.0, bottomheight+columnheight/2., 0.0 ), ), ), 
        maxSize=meshsize, minSize=meshsizeFine)
    columnPart.seedEdgeByBias(biasMethod=SINGLE, constraint=FINER, 
        end2Edges=columnPart.edges.findAt((((topradius+footradius)/2., bottomheight+columnheight/2., 0.0), ), ), 
        maxSize=meshsize, minSize=meshsizeFine)
    columnPart.seedEdgeBySize(constraint=FINER, 
        deviationFactor=0.1, edges=(columnPart.edges.findAt((0., bottomheight/2., 0.0),),),
        minSizeFactor=0.1, size=meshsizeCoarse)
    columnPart.seedEdgeBySize(constraint=FINER, 
        deviationFactor=0.1, edges=(columnPart.edges.findAt((bottomradius, bottomheight/2., 0.0),),),
        minSizeFactor=0.1, size=meshsizeCoarse)
    columnPart.generateMesh()
    mdb.models['Model-1'].rootAssembly.regenerate()

    mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(variables=(
        'S', 'LE', 'U',  'CSTRESS', 'CDISP'))
    mdb.models['Model-1'].historyOutputRequests['H-Output-1'].setValues(rebar=
        EXCLUDE, region=asm.sets['loadpoint'], 
        sectionPoints=DEFAULT, variables=('RF2', 'U2'))

    description="Micro column beam simulation in "
    if model3d:
        description+="3D\n"
    else:
        description+="2D\n"
    description+="Parameters:\n"
    description+="column dimensions:\n"
    description+="foot radius="+repr(footradius)+"\n"
    description+="top radius="+repr(topradius)+"\n"
    description+="height="+repr(columnheight)+"\n"
    description+="Support dimensions:\n"
    description+="Fillet radius="+repr(filletradius)+"\n"
    description+="radius="+repr(bottomradius)+"\n"
    description+="height="+repr(bottomheight)+"\n"
    if contactFlag:
        description+="Loading applied bypunch contact\n"
    description+="prescripted displacement="+repr(loadvalue)
    mdb.models['Model-1'].setValues(description)
    mdb.Job(atTime=None, contactPrint=OFF, description='', echoPrint=OFF, 
        explicitPrecision=SINGLE, getMemoryFromAnalysis=True, historyPrint=OFF, 
        memory=90, memoryUnits=PERCENTAGE, model='Model-1', modelPrint=OFF, 
        multiprocessingMode=DEFAULT, name=inputname, nodalOutputPrecision=
        SINGLE, numCpus=1, numGPUs=0, queue=None, resultsFormat=ODB, scratch='', 
        type=ANALYSIS, userSubroutine='', waitHours=0, waitMinutes=0)
    mdb.jobs[inputname].writeInput()
    mdb.saveAs(pathName=inputname+'.cae')

if __name__ == "__main__":
    contactFlag=False
    model3d=True
    angle3d=360.
    footradius=1.735
    topradius=1.385
    columnheight=7.02
    bottomradius=4.
    bottomheight=5.
    filletradius=0.1
    loadvalue=0.2
    inputname="microcolumn1"
    microcolumn(contactFlag,model3d,angle3d,footradius,topradius,columnheight,
        bottomradius,bottomheight,filletradius,loadvalue, inputname)