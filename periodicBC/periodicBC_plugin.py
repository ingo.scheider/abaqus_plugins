from abaqusGui import getAFXApp, Activator, AFXMode
from abaqusConstants import ALL
import os
thisPath = os.path.abspath(__file__)
thisDir = os.path.dirname(thisPath)

toolset = getAFXApp().getAFXMainWindow().getPluginToolset()
toolset.registerGuiMenuButton(
    buttonText='periodicBoundaryConditions', 
    object=Activator(os.path.join(thisDir, 'periodicBCDB.py')),
    kernelInitString='import periodicBC_module',
    messageId=AFXMode.ID_ACTIVATE,
    icon=None,
    applicableModules=('Visualization',),
    version='12.08.2021',
    author='Ingo Scheider, Hereon',
    description='Definition of periodic boundary conditions based on an existing odb and input file. Additionally, a dummy element can be generated for application of arbitrary loading directions. \n#################################\nScript can only be submitted within the Viewer module\n#################################',
    helpUrl='N/A'
)
