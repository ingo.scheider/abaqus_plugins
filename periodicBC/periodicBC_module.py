'''
This script generates periodic boundary conditions in 2D and 3D models
execution command line:
abaqus python periodic-bc.py <problemname> [2D|3D] [<instancename]
the default for the two optional parameters is 3D and PART-1-1
#
It generates a file name <problemname>_nset.inc
This file contains all node sets and equations necessary for the periodic boundary conditions
  and is automatically included into the .inp file
#
The node sets relevant for the user are:
2D: xref, yref, fixpoint
3D: xref, yref, zref, fixpoint
#
Please define the following boundary conditions:
**2D, displacement controlled:
*BOUNDARY
fixpoint,1,2,0.0
xref,1,1,xxdisp
yref,1,1,yxdisp
xref,2,2,xydisp
yref,2,2,yydisp
#
**2D, load controlled (note that the element sets are not given!)
*BOUNDARY
fixpoint,1,2,0.0
yref,1,1,0.0
xref,2,2,0.0
*DLOAD
xminElset,Pn, xload
xmaxElset,Pn, xload
yminElset,Pn, yload
ymaxElset,Pn, yload
#
**3D:
*BOUNDARY
fixpoint,1,3,0.0
xref,1,1,xxdisp
yref,1,1,yxdisp
zref,1,1,zxdisp
xref,2,2,xydisp
yref,2,2,yydisp
zref,2,2,zydisp
xref,3,3,xzdisp
yref,3,3,yzdisp
zref,3,3,zzdisp

**3D, load controlled (note that the element sets are not identified by the script!)
see 2D case, everything is similar...
#----------------------------------------------------------
Version history:
30.7.2014: Start version history
22.9.2014: *include command for <problemname>_nset.inc file and
           load boundary conditions are now included in the .inp
           file automatically (BUTZKE)
22.4.2015(IS): - For tie constraints at the boundaries,
                 exclude the respective slave nodes from the equations
               - include parameter in input file
               - search keywords case insensitive in input file
               - retain original input file as <problemname>.inp.bak
2.07.2015: (IS)Bug Fix: Now the minimum coordinate of the structure is
           not necessarily at (0,0,0), but may be anywhere
28.05.2021: (IS) adding flag to write dummy file only.
                 For this, restructuring the program was necessary
12.08.2021: (IS) adding a second rotation direction.
#----------------------------------------------------------
'''
from odbAccess import *
from sys import argv
import numpy as np
from time import time
from os import path
import fileinput


def initial():
    '''
    initialization routine
    '''
    e = 5.
    c = 1.
    sq3d2 = sqrt(3.)/2.
    e3d2 = sq3d2*e
    c3d2 = sq3d2*c
    xmax = 3.*e+2.*c3d2
    ymax = sqrt(3.)*e+c
    zmax = 100.
    return ([0, 0, 0], [xmax, ymax, zmax])


def printSet(report, name, nset, *instance):
    '''
    Subroutine for printout of node sets
    '''
    if len(nset) == 0:
        return
    if len(instance) > 0:
        report.write("*Nset, nset=%s, instance=%s\n" % (name, instance[0]))
    else:
        report.write("*Nset, nset=%s\n," % (name))
    nodesperline = 12
    lines = len(nset)/nodesperline
    inode = 0
    for i in range(lines):
        for j in range(nodesperline):
            report.write("%7d," % (nset[inode],))
            inode += 1
        report.write("\n")
    for i in range(len(nset)-nodesperline*lines):
        report.write("%7d," % (nset[inode],))
        inode += 1
    report.write("\n")
    return


def printEquat(report, printlist, directions, factors, refnodes, *instance):
    '''
    Subroutine for printout of equations
    '''
    terms = len(printlist)
    nodeformats = []
    for node in printlist:
        try:
            eval(node)
            nodeformats.append("%d")
        except:
            nodeformats.append("%s")
    if type(directions) == int:
        directions = [directions for i in range(terms)]
    try:
        list(factors)
    except:
        factors = [factors] + [-factors for i in range(terms-1)]
    PartAssembly = len(instance) > 0
    formatlist = []
    formatstring = ""
    for i in range(0, terms):
        if PartAssembly:
            if printlist[i] in refnodes:
                formatstring += nodeformats[i]+",%d,%.5f, "
            else:
                formatstring += instance[0]+"."+nodeformats[i]+",%d,%.5f, "
        else:
            formatstring += "      "+nodeformats[i]+",%d,%.5f, "
        formatlist.append(printlist[i])
        formatlist.append(directions[i])
        formatlist.append(factors[i])
        if (i+1) % 4 == 0:
            formatstring = formatstring[:-1] + "\n"
    report.write(" %s\n" % (terms,))
    equatstring = formatstring[:-2] + "\n"
    report.write(equatstring % tuple(formatlist))
    return


def writeDummy(PartAssembly,
               nodeLabel,
               coordMin,
               coordMax,
               namestring,
               rotationDirection1,
               loadingAngle1,
               loadingAngle2flag,
               rotationDirection2,
               loadingAngle2,
               ):
    '''
    Subroutine for
    calculating the final rotation matrix,
    constructing all equations, and
    writing the dummy file
    '''
    nodeLabel += 4
    dummynode = [nodeLabel+i for i in range(8)]
    dummyrefnode = [nodeLabel+8+i for i in range(3)]
    dummysurfmaster = [nodeLabel+11+i for i in range(3)]
    refnodecoord = [coordMin[i]+(coordMax[i]-coordMin[i])/2. for i in range(3)]
    dummyfilename = namestring + "_%s%d" % (rotationDirection1, loadingAngle1)
    if loadingAngle2flag:
        dummyfilename += "_%s%d" % (rotationDirection2, loadingAngle2)
    dummyfilename += "deg_Dummy.inc"
    with open(dummyfilename, "w") as dummyfile:
        dummyfile.write("**\n")
        if PartAssembly:
            dummyfile.write("** DUMMY INSTANCE\n")
            dummyfile.write("**\n")
            dummyfile.write("*Instance, name=dummy-1, part=dummy\n")
        dummyfile.write("*Node, nset=DUMMY\n")
        dummyfile.write("%5d, %14.7g, %14.7g, %14.7g\n" %
                        (dummynode[0], coordMax[0], coordMax[1], coordMax[2]))
        dummyfile.write("%5d, %14.7g, %14.7g, %14.7g\n" %
                        (dummynode[1], coordMax[0], coordMin[1], coordMax[2]))
        dummyfile.write("%5d, %14.7g, %14.7g, %14.7g\n" %
                        (dummynode[2], coordMax[0], coordMax[1], coordMin[2]))
        dummyfile.write("%5d, %14.7g, %14.7g, %14.7g\n" %
                        (dummynode[3], coordMax[0], coordMin[1], coordMin[2]))
        dummyfile.write("%5d, %14.7g, %14.7g, %14.7g\n" %
                        (dummynode[4], coordMin[0], coordMax[1], coordMax[2]))
        dummyfile.write("%5d, %14.7g, %14.7g, %14.7g\n" %
                        (dummynode[5], coordMin[0], coordMin[1], coordMax[2]))
        dummyfile.write("%5d, %14.7g, %14.7g, %14.7g\n" %
                        (dummynode[6], coordMin[0], coordMax[1], coordMin[2]))
        dummyfile.write("%5d, %14.7g, %14.7g, %14.7g\n" %
                        (dummynode[7], coordMin[0], coordMin[1], coordMin[2]))
        # inserting global reference nodes
        dummyfile.write('*Node,nset=xrefglob\n')
        dummyfile.write("%7d, %14.7g, %14.7g, %14.7g\n" % (
            dummyrefnode[0], coordMax[0], refnodecoord[1], refnodecoord[2]))
        dummyfile.write('*Node,nset=yrefglob\n')
        dummyfile.write("%7d, %14.7g, %14.7g, %14.7g\n" % (
            dummyrefnode[1], refnodecoord[0], coordMax[1], refnodecoord[2]))
        dummyfile.write('*Node,nset=zrefglob\n')
        dummyfile.write("%7d, %14.7g, %14.7g, %14.7g\n" % (
            dummyrefnode[2], refnodecoord[0], refnodecoord[1], coordMax[2]))
        dummyfile.write('*Nset,nset=fixpointglob\n')
        dummyfile.write('  %7d,\n' % dummynode[7])
        dummyfile.write('*Node,nset=xSurfMaster\n')
        dummyfile.write("%7d, %14.7g, %14.7g, %14.7g\n" % (
            dummysurfmaster[0], coordMax[0], refnodecoord[1], refnodecoord[2]))
        dummyfile.write('*Node,nset=ySurfMaster\n')
        dummyfile.write("%7d, %14.7g, %14.7g, %14.7g\n" % (
            dummysurfmaster[1], refnodecoord[0], coordMax[1], refnodecoord[2]))
        dummyfile.write('*Node,nset=zSurfMaster\n')
        dummyfile.write("%7d, %14.7g, %14.7g, %14.7g\n" % (
            dummysurfmaster[2], refnodecoord[0], refnodecoord[1], coordMax[2]))
        dummyfile.write('**\n')
        # inserting dummy element
        elementnodes = [dummynode[i] for i in [4, 5, 7, 6, 0, 1, 3, 2]]
        dummyfile.write('*Element, elset=DUMMY, type=C3D8\n')
        dummyfile.write("1000000, "+", ".join(map(repr, elementnodes))+"\n")
        dummyfile.write('**\n')
        dummyfile.write('** Section: DUMMY\n')
        dummyfile.write('*Solid Section, elset=DUMMY, material=DUMMY\n')
        dummyfile.write(', \n')
        dummyfile.write('**\n')
        #
        # equations
        #
        dummyfile.write(
            "**\n**Coupling of the dummy nodes to the RVE reference nodes\n** (periodic boundaries of the dummy)\n")
        dummyfile.write(
            "**\n**Coupling of the dummy nodes to the RVE reference nodes\n")
        dummyfile.write("** (periodic boundaries of the dummy)\n")
        factors = 1.0
        refnodes = []
        for directions in range(1, 4):
            dummyfile.write('*Equation\n')
            nodelist = [dummynode[2], dummyrefnode[0],
                        dummyrefnode[1], dummynode[7]]
            printEquat(dummyfile, nodelist, directions, factors, refnodes)
            nodelist = [dummynode[6], dummyrefnode[1], dummynode[7]]
            printEquat(dummyfile, nodelist, directions, factors, refnodes)
            nodelist = [dummynode[4], dummyrefnode[1],
                        dummyrefnode[2], dummynode[7]]
            printEquat(dummyfile, nodelist, directions, factors, refnodes)
            nodelist = [dummynode[5], dummyrefnode[2], dummynode[7]]
            printEquat(dummyfile, nodelist, directions, factors, refnodes)
            nodelist = [dummynode[3], dummyrefnode[0], dummynode[7]]
            printEquat(dummyfile, nodelist, directions, factors, refnodes)
            nodelist = [dummynode[0], dummyrefnode[0],
                        dummyrefnode[1], dummyrefnode[2], dummynode[7]]
            printEquat(dummyfile, nodelist, directions, factors, refnodes)
            nodelist = [dummynode[1], dummyrefnode[0],
                        dummyrefnode[2], dummynode[7]]
            printEquat(dummyfile, nodelist, directions, factors, refnodes)
        # surface masters
        dummyfile.write(
            "**\n**Calculation of the displacement of the surface average master\n**\n")
        factors = [-4.0] + 4*[-1.0, 1.0]
        for directions in range(1, 4):
            dummyfile.write('*Equation\n')
            nodelist = [dummysurfmaster[0]] + [dummynode[i]
                                               for i in [4, 0, 5, 1, 6, 2, 7, 3]]
            printEquat(dummyfile, nodelist, directions, factors, refnodes)
            nodelist = [dummysurfmaster[1]] + [dummynode[i]
                                               for i in [1, 4, 3, 0, 5, 2, 7, 6]]
            printEquat(dummyfile, nodelist, directions, factors, refnodes)
            nodelist = [dummysurfmaster[2]] + [dummynode[i]
                                               for i in [7, 4, 3, 5, 2, 0, 6, 1]]
            printEquat(dummyfile, nodelist, directions, factors, refnodes)
        #
        if PartAssembly:
            dummyfile.write("*End Instance\n")
        #
        # rotation of the RVE relative to Dummy element
        R = calcRotMatrix(rotationDirection1, loadingAngle1)
        if loadingAngle2flag:
            R = np.matmul(calcRotMatrix(rotationDirection2, loadingAngle2), R)
        #
        surfmaster = ["xSurfMaster", "ySurfMaster", "zSurfMaster"]
        refnodes = ["xref", "yref", "zref"]
        dummyfile.write(
            "**\n** Transformation equation according to the loading direction ({:d}deg)\n".format(int(loadingAngle1)))
        dummyfile.write("** (Rotation around the %s axis)\n" %
                        (rotationDirection1,))
        if loadingAngle2flag:
            dummyfile.write(
                "**\n** Second transformation to the loading direction ({:d}deg)\n".format(int(loadingAngle2)))
            dummyfile.write(
                "** (Second rotation around the %s axis)\n" % (rotationDirection2,))
        for line in str(R).split("\n"):
            dummyfile.write("** "+line+"\n")
        dummyfile.write('**\n*Equation\n')
        for iref, ref in enumerate(refnodes):
            for dir in [1, 2, 3]:
                nodelist = [ref]
                factors = [-1.0]
                directions = [dir]
                for j in range(3):
                    for k in range(3):
                        factor = R[j, iref]*R[k, dir-1]
                        if abs(factor) > 1.e-6:
                            nodelist.append(surfmaster[j])
                            factors.append(factor)
                            directions.append(k+1)
                if PartAssembly:
                    printEquat(dummyfile, nodelist, directions,
                               factors, refnodes, "dummy-1")
                    nsetsuffix = ", instance=dummy-1"
                else:
                    printEquat(dummyfile, nodelist,
                               directions, factors, refnodes)
                    nsetsuffix = ""
        dummyfile.write('*Nset, nset=xrefglob%s\n' % nsetsuffix)
        dummyfile.write("%7d\n" % (dummyrefnode[0]))
        dummyfile.write('*Nset, nset=yrefglob%s\n' % nsetsuffix)
        dummyfile.write("%7d\n" % (dummyrefnode[1]))
        dummyfile.write('*Nset, nset=zrefglob%s\n' % nsetsuffix)
        dummyfile.write("%7d\n" % (dummyrefnode[2]))
        dummyfile.write('*Nset, nset=fixpointglob%s\n' % nsetsuffix)
        dummyfile.write('  %7d,\n' % dummynode[7])
    return dummyfilename
#
#


def calcRotMatrix(dir, angle):
    '''
    Calculation of the rotation matrix based on given direction and angle
    '''
    rad = np.deg2rad(angle)
    c = np.cos(rad)
    s = np.sin(rad)
    if dir == "X":
        return np.array([[1, 0, 0], [0, c, -s], [0, s, c]])
    elif dir == "Y":
        return np.array([[c, 0, s], [0, 1, 0], [-s, 0, c]])
    elif dir == "Z":
        return np.array([[c, -s, 0], [s, c, 0], [0, 0, 1]])
#
# =======================
# MAIN PROGRAM
# =======================
#


def periodicBC(dimension,
               fileName,
               instance,
               dummyFlag,
               rotationDirection1,
               loadingAngle1,
               dummyOnlyFlag,
               loadingAngle2flag,
               rotationDirection2,
               loadingAngle2
               ):
    '''
    This is the main program of the module.
    All parameters are entered by the GUI.
    '''
    namestring = fileName[:-4]
    reportbase = path.basename(namestring)
    nset_filename = reportbase+"_nset.inc"
    # prefix for all periodic boundary node sets:
    setbase = "bcset"
    #
    # nodal tolerance measure
    EPS = 1.e-05
    # Maximum distance between opposite nodes
    DISTMAX = 1.
    #
    # boundary conditions (maximum values taken from function initial()
    # (coordMin,coordMax) = initial()
    coordMin = [1e5, 1e5, 1e5]
    coordMax = [-1e5, -1e5, -1e5]
    #
    if dimension == "2D":
        DIMENSION = 2
        FLAG3D = False
        coordMin[2] = 0.
        coordMax[2] = 0.
    elif dimension == "3D":
        DIMENSION = 3
        FLAG3D = True
    else:
        print "second parameter on the command line is either 2D or 3D!"
        exit()
    VALKIND = ["xmin", "xmax", "ymin", "ymax", "zmin", "zmax"]
    #
    try:
        odb = openOdb(namestring+".odb", readInternalSets=True, readOnly=True)
    except:
        print "=============================================================="
        print "Could not open odb %s" % (namestring,)
        print "=============================================================="
        exit()
    try:
        part = odb.rootAssembly.instances[instance]
    except:
        print "=============================================================="
        print "Could not find instance %s" % (instance,)
        print "Available instances: %s" % (tuple(odb.rootAssembly.instances.keys()),)
        print "Please run program again using the command:"
        print "abaqus python %s [2D|3D] %s" % (namestring, odb.rootAssembly.instances.keys()[0])
        print "=============================================================="
        exit()

    tieNodes = []
    nsets = odb.rootAssembly.nodeSets
    nties = 0
    for set in nsets.keys():
        if set.find("TIED_SLAVE") >= 0:
            nties += 1
            nset = nsets[set].nodes[0]
            for node in nset:
                if node.label not in tieNodes:
                    tieNodes.append(node.label)
    if nties > 0:
        print "%d nodes are slave nodes in %d tie conditions" % (len(tieNodes), nties)
    nsets = part.nodeSets.keys()
    if "XREF" in nsets or "YREF" in nsets or "ZREF" in nsets:
        print "=============================================================="
        print "This odb already contains reference node sets."
        print "Please rerun an input file without reference node sets again"
        print "to get a clean odb"
        print "=============================================================="
        exit()

    PartAssembly = False  # are there Parts and Assemblies in .inp file?

    #
    # ##########################################
    # Part 0 : Finding boundary sets and min/max coordinates of the structure
    # ##########################################
    # loop over all nodes
    nsets = {}
    nodeCoords = {}
    maxLabel = 0
    start = time()
    for n in part.nodes:
        nodeCoords[n.label] = n.coordinates
    maxLabel = max(nodeCoords.keys())
    a_nodeCoords = np.array(nodeCoords.values())
    a_nodeLabels = np.array(nodeCoords.keys(), dtype=int)
    coordMin = np.min(a_nodeCoords, axis=0)
    coordMax = np.max(a_nodeCoords, axis=0)
    for dim in range(3):
        coomin = int(coordMin[dim]/EPS)
        coomax = int(coordMax[dim]/EPS)
        coovalues = (a_nodeCoords[:, dim]/EPS).astype(int)
        # minimum
        setname = setbase + "%d" % (dim*2,)
        minindizes = (coovalues <= (coomin+1)).nonzero()
        nsets[setname] = list(a_nodeLabels[minindizes])
        # maximum
        setname = setbase + "%d" % (dim*2+1,)
        maxindizes = (coovalues >= (coomax-1)).nonzero()
        nsets[setname] = list(a_nodeLabels[maxindizes])

    print "Minimum of the structure is (%f,%f,%f)" % (tuple(coordMin))
    print "Maximum of the structure is (%f,%f,%f)" % (tuple(coordMax))
    if abs(coordMin[2]-coordMax[2]) < EPS and FLAG3D:
        print "=============================================================="
        print "z-coordinates are all equal. It seems to be a 2D problem."
        print "Please run program again using the command:"
        print "abaqus python %s 2D %s" % (namestring, instance)
        print "=============================================================="
        exit()
    #
    # ##########################################
    # Part Ia : If only Dummy file is to be written, do it and exit
    # ##########################################
    #
    if dummyFlag:
        lines = open(namestring+'.inp', "r")
        for line in lines.readlines():
            lineUpper = line.upper()
            if lineUpper.startswith('*ASSEMBLY'):
                PartAssembly = True
                break
        lines.close()
        dummyname = writeDummy(PartAssembly, maxLabel, coordMin, coordMax, namestring, rotationDirection1, loadingAngle1,
                               loadingAngle2flag, rotationDirection2, loadingAngle2)
        print "The dummy file %s has been generated" % dummyname
        if dummyOnlyFlag:
            if PartAssembly:
                print "Before the *Assembly command you have to add the following lines:"
                print "*Part, name=dummy"
                print "*End Part"
                print "Within the assembly block you have to include the following line:"
                print "*Include, input=%s" % dummyname
            else:
                print "However, you have to include the following line into the input file:"
                print "*Include, input=%s" % dummyname
            return
    #
    # ##########################################
    # Part Ib : Inserting PBC includes in input file
    # ##########################################
    #
    lines = fileinput.input(namestring+'.inp', inplace=1, backup=".bak")
    includefileAdded = False
    for line in lines:
        lineUpper = line.upper()
        if lineUpper.startswith('*HEADING'):
            print "*Parameter"
            print "xxdisp = 0.0"
            print "xydisp = 0.0"
            print "xzdisp = 0.0"
            print "yxdisp = 0.0"
            print "yydisp = 0.0"
            print "yzdisp = 0.0"
            print "zxdisp = 0.0"
            print "zydisp = 0.0"
            print "zzdisp = 0.0"
            print "**"
            print line,
        elif lineUpper.startswith('*END PART') and dummyFlag:
            print line,
            print '**'
            print '** DUMMY PART'
            print '**'
            print "*Part, name=dummy"
            print "*End Part"
            print '**'
        elif lineUpper.startswith('*END ASSEMBLY'):
            PartAssembly = True
            includefileAdded = True
            print '*INCLUDE, INPUT=' + namestring + '_nset.inc'
            if dummyFlag:
                print '*INCLUDE, INPUT=%s' % dummyname
            print '**'
            print line,
        elif lineUpper.startswith('*STEP'):
            if dummyFlag:
                print('**\n*Material, name=DUMMY')
                print('*Elastic')
                print('0.01, 0.3 ')
                print('*CONDUCTIVITY')
                print('1.0')
                print('*SPECIFIC HEAT ')
                print('1.0')
                print('*DENSITY')
                print('1.e-09')
                print '**'
            if not includefileAdded:
                includefileAdded = True
                print '** Include Files\n*INCLUDE, INPUT=\"' + namestring + '_nset.inc\"'
                if dummyFlag:
                    print '*INCLUDE, INPUT=%s' % dummyname
                print '**'
                print '**'
            print line,
        elif lineUpper.startswith('*END STEP'):
            print '*BOUNDARY'
            refset = ""
            if dummyFlag:
                refset += "glob"
            maxdim = 3 if FLAG3D else 2
            refnodes = ["x", "y", "z"]
            print 'fixpoint, 1,%d, 0.0' % maxdim
            if dummyFlag:
                print 'fixpointglob, 1,%d, 0.0' % maxdim
            for dim1 in range(maxdim):
                dir1char = refnodes[dim1]
                for dim2 in range(maxdim):
                    dir2char = refnodes[dim2]
                    print "%sref%s, %d,%d, <%s%sdisp>" % (dir1char, refset, dim2+1, dim2+1, dir1char, dir2char)
            print line,
        else:
            print line,
    fileinput.close()

    #
    # ##########################################
    # Part II : Generate new boundary sets
    # ##########################################
    #
    report = open(reportbase+"_nset.inc", "w")
    print "The nodesets and equations are written to "+nset_filename

    for key in nsets.keys():
        lenOrig = len(nsets[key])
        v = eval(key.strip(setbase))
        print "%s originally contains %d nodes" % (VALKIND[v], lenOrig)
        tmplist = []
        if (len(tieNodes) > 0):
            for nd in nsets[key]:
                if nd not in tieNodes:
                    tmplist.append(nd)
            nsets[key] = tmplist
            lenReduc = len(nsets[key])
            print "%d tie nodes have been deleted (Now contains %d nodes)" % (lenOrig-lenReduc, lenReduc)

    for iset1coo in range(DIMENSION-1):
        for iset1MinMax in range(2):
            iset1 = iset1coo*2+iset1MinMax
            setname1 = setbase+"%d" % (iset1,)
            inode1 = 0
            while inode1 < len(nsets[setname1]):
                node = nsets[setname1][inode1]
                for iset2coo in range(iset1coo+1, DIMENSION):
                    for iset2MinMax in range(2):
                        iset2 = iset2coo*2+iset2MinMax
                        setname2 = setbase+"%d" % (iset2,)
                        # search node from set1 within set2
                        if node in nsets[setname2]:
                            setnewname = setbase+"%d%d" % (iset1, iset2)
                            if DIMENSION-iset1coo == 3 and iset2coo == 1:  # if iset1coo=0 and iset2coo=1 for 3D structures
                                if node in nsets["bcset4"]:
                                    setnewname = setbase + \
                                        "%d%d%d" % (iset1, iset2, 4)
                                    nsets["bcset4"].remove(node)
                                elif node in nsets["bcset5"]:
                                    setnewname = setbase + \
                                        "%d%d%d" % (iset1, iset2, 5)
                                    nsets["bcset5"].remove(node)
                            nsets[setname1].remove(node)
                            inode1 -= 1
                            nsets[setname2].remove(node)
                            if setnewname in nsets:
                                nsets[setnewname].append(node)
                            else:
                                nsets[setnewname] = [node]
                            break
                inode1 += 1
    #
    # write boundary sets
    for key in nsets.keys():
        print "%s contains %s nodes" % (key, len(nsets[key]))
        if PartAssembly:
            printSet(report, key, nsets[key], instance)
        else:
            printSet(report, key, nsets[key])
    #
    print "Finding %d boundary conditions took %d seconds" % (len(nsets), time()-start)
    #
    # write reference nodes
    refnodes = [maxLabel+1, maxLabel+2, maxLabel+3]
    # xmax:
    report.write("*Node,nset=xref\n")
    report.write("%6d,%12.6f,%12.6f,%12.6f\n" %
                 (refnodes[0], coordMax[0], coordMax[1]/2., coordMax[2]/2.))
    # ymax:
    report.write("*Node,nset=yref\n")
    report.write("%6d,%12.6f,%12.6f,%12.6f\n" %
                 (refnodes[1], coordMax[0]/2., coordMax[1], coordMax[2]/2.))
    # zmax:
    if FLAG3D:
        report.write("*Node,nset=zref\n")
        report.write("%6d,%12.6f,%12.6f,%12.6f\n" %
                     (refnodes[2], coordMax[0]/2., coordMax[1]/2., coordMax[2]))
        # writing rigid body motion suppression node sets
        if PartAssembly:
            report.write("*Nset,nset=fixpoint, instance=%s\n" % (instance,))
            report.write("%6d\n" % (nsets["bcset024"][0]))
        else:
            report.write("*Nset,nset=fixpoint\n")
            report.write("%6d\n" % (nsets["bcset024"][0]))
        report.write("**\n")
    else:
        if PartAssembly:
            report.write("*Nset,nset=fixpoint, instance=%s\n" % (instance,))
            report.write("%6d\n" % (nsets["bcset02"][0]))
        else:
            report.write("*Nset,nset=fixpoint\n")
            report.write("%6d\n" % (nsets["bcset02"][0]))
        report.write("**\n")
    #
    # ##########################################
    # Part III : Finding equations
    # ##########################################
    # search for nearest equivalent node and write equations

    def distance(c1, c2): return np.sqrt(
        np.square(c1[0]-c2[0])+np.square(c1[1]-c2[1])+np.square(c1[2]-c2[2]))
    # --------------------------------
    # first: faces(3D) or edges(2D)
    # --------------------------------
    report.write("**Equations for the faces/edges\n")
    for idim in range(DIMENSION):
        start = time()
        report.write(
            "**Equations for the faces/edges on coordinate %d\n" % (idim,))
        report.write("*Equation\n")
        # masterset at minimum coordinate (direction idim)
        masterset = setbase+"%d" % (idim*2,)
        masternodes = [node for node in nsets[masterset]]
        mastercoords = [nodeCoords[nd] for nd in masternodes]
        # slaveset at maximum coordinate (direction idim)
        slaveset = setbase+"%d" % (idim*2+1,)
        print "Search for equal coordinates between %s and %s (%d/%d nodes)" % (
               masterset, slaveset, len(nsets[slaveset]), len(masternodes))
        notfound = []
        for node in nsets[slaveset]:
            coordslave = np.array(nodeCoords[node])
            coordslave[idim] -= coordMax[idim]-coordMin[idim]
            #
            distances = np.linalg.norm(coordslave-mastercoords, axis=1)
            dminIndex = np.argmin(distances)
            dmin = distances[dminIndex]
            nearest = masternodes[dminIndex]
            del masternodes[dminIndex]
            del mastercoords[dminIndex]
            # storing the maximum distance between all the correspondences
            if dmin > DISTMAX:
                notfound.append((node, dmin))
            else:
                equatlist = [node, refnodes[idim], nearest]
                for j in range(DIMENSION):
                    if PartAssembly:
                        printEquat(report, equatlist, j+1,
                                   1.0, refnodes, instance)
                    else:
                        printEquat(report, equatlist, j+1, 1.0, refnodes)
        if len(notfound) > 0:
            print "Did not find any corresponding node for %d nodes" % (len(notfound))
        print "These %d nodes took %d seconds" % (len(nsets[slaveset]), time()-start)
    # --------------------------------
    # second: edges (only 3D)
    # --------------------------------
    if FLAG3D:
        report.write("**Equations between edges\n")
        slaveshift1 = [1, 0, 1]
        slaveshift2 = [0, 1, 1]
        for idim in range(DIMENSION):
            start = time()
            i1 = (idim+1) % 3
            i2 = (idim+2) % 3
            side1 = min(i1, i2)*2
            side2 = max(i1, i2)*2
            masterset = setbase+"%d%d" % (side1, side2)
            if masterset not in nsets:
                continue
            # 3 resp. 2 slave edges /vertices
            for islave in range(DIMENSION):
                slaveset = setbase + \
                    "%d%d" % (side1+slaveshift1[islave],
                              side2+slaveshift2[islave])
                if slaveset not in nsets:
                    continue
                masternodes = [node for node in nsets[masterset]]
                mastercoords = [nodeCoords[nd] for nd in masternodes]
                report.write("**Equations between edges %s and %s\n" %
                             (masterset, slaveset))
                report.write("*Equation\n")
                print "Search for equal coordinates between %s and %s (%d/%d nodes)" % \
                    (masterset, slaveset, len(
                        nsets[slaveset]), len(masternodes))
                # calculating the distance for each node in each slave set
                for node in nsets[slaveset]:
                    coordslave = np.array(nodeCoords[node])
                    coordslave[side1/2] -= (coordMax[side1/2] -
                                            coordMin[side1/2])*slaveshift1[islave]
                    coordslave[side2/2] -= (coordMax[side2/2] -
                                            coordMin[side2/2])*slaveshift2[islave]
                    distances = np.linalg.norm(coordslave-mastercoords, axis=1)
                    dminIndex = np.argmin(distances)
                    dmin = distances[dminIndex]
                    nearest = masternodes[dminIndex]
                    del masternodes[dminIndex]
                    del mastercoords[dminIndex]
                    if dmin > DISTMAX:
                        print "Did not find any corresponding (slave=%f,%f,%f, master=%f,%f,%f)" % \
                            (tuple(coordslave)+tuple(nodeCoords[nearest]))
                    else:
                        formatlist = [node]
                        if slaveshift1[islave] == 1:
                            formatlist.append(refnodes[side1/2])
                        if slaveshift2[islave] == 1:
                            formatlist.append(refnodes[side2/2])
                        formatlist.append(nearest)
                        for j in range(DIMENSION):
                            if PartAssembly:
                                printEquat(report, formatlist, j+1,
                                           1.0, refnodes, instance)
                            else:
                                printEquat(report, formatlist,
                                           j+1, 1.0, refnodes)
                print "These %d nodes took %d seconds" % (len(nsets[slaveset]), time()-start)
    #
    # --------------------------------
    # third: vertices
    # --------------------------------
    report.write("**Equations between corner nodes\n")
    if FLAG3D:
        setMstr = setbase+"024"
    else:
        setMstr = setbase+"02"
    nodeMstr = nsets[setMstr][0]
    coordmaster = nodeCoords[nodeMstr]
    for set in nsets.keys():
        if set == setMstr:
            continue
        cornerset = set.strip(setbase)
        if len(cornerset) != DIMENSION:
            continue
        report.write("**Equations between sets %s and %s\n" % (setMstr, set))
        report.write("*Equation\n")
        nodeSlave = nsets[set][0]
        coordslave = nodeCoords[nodeSlave]
        formatlist = [nodeSlave]
        for idir in range(DIMENSION):
            charcoord = eval(cornerset[idir])
            icomax = charcoord % 2
            if icomax == 1:
                formatlist += [refnodes[idir]]
        formatlist += [nodeMstr]
        for idir in range(DIMENSION):
            if PartAssembly:
                printEquat(report, formatlist, idir+1, 1.0, refnodes, instance)
            else:
                printEquat(report, formatlist, idir+1, 1.0, refnodes)

    odb.close()
    report.close()


if __name__ == "__main__":
    dimension = "3D"
    fileName = "test.odb"
    instance = "PART-1-1"
    dummyFlag = True
    dummyOnlyFlag = False
    rotationDirection1 = "Z"
    loadingAngle1 = 45
    loadingAngle2flag = False
    rotationDirection2 = "X"
    loadingAngle2 = 0
    periodicBC(dimension, fileName, instance,
               dummyFlag,
               rotationDirection1, loadingAngle1,
               dummyOnlyFlag, loadingAngle2flag,
               rotationDirection2, loadingAngle2)
