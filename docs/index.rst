.. abaqus plugins and tools documentation master file, created by
   sphinx-quickstart on Mon Oct 17 16:02:34 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

abaqus plugins and tools documentation
====================================================

Even though it is designated as the documentation of my gitlab repository, it only contains the documentation on the odb2vtk tool. Information on the other tools can be found in the README.md file.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   usage
   additionalhints
   xml-file format<xmlformat>

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

.. * :ref:`modindex`

