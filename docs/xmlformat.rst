xml control file format
=======================

.. code::

    <?xml version="1.0" ?>
    <parameter>
      <version>versionnumber</version>
      <odb>odbfilename</odb>
      <vtu>subdirectory_for_vtufiles</vtu>
      <vtk>file_for_vtkfile</vtk>
      <projectpath>/path/to/odbfile</projectpath>
      <includeInstance>list,of,included,instances</includeInstance>
      <excludeInstance>list,of,excluded,instances</excludeInstance>
      <includeVar>list,of,included,variables</includeVar>
      <excludeVar>list,of,excluded,variables</excludeVar>
      <step>1</step>
      <frame step="1">0:21:1</frame>
    </parameter>


.. note::

    1. All parameters except <odb> are optional.
    2. If <vtu> is not given, it is taken as odbfilename.vtu 
    3. If <vtk> is not given, it is taken as odbfilename.vtk 
    4. if <projectpath> is not given, the current directory is used.
    5. In the variables definition :code:`<includeVar>/<excludeVar>` you may also enter the * wildcard at the beginning or the end of the variablename, e.g., :code:`S*` in-/excludes all stress related variables, while :code:`*PRINCIPAL` in-/excludes all principal stresses, strains, etc.
    6. You may either define <includeInstance> or <excludeInstance>, but not both.
    7. You may either define <includeVar> or <excludeVar>, but not both.
    8. If you neither define <includeInstance> nor <excludeInstance>, all instances are converted.
    9. If you neither define <includeVar> nor <excludeVar>, all instances are converted.
    10. If no <step> definition is given, all steps are transferred.
    11. If no <frame> definitions are given for a step, all frames are converted.
    12. The step number in the <frame> parameter must be given in quotation marks.