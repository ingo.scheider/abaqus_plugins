Introduction 
=============

The evaluation of results is crucial for any simulation. An alternative to using ABAQUS/Viewer is the freeware product Paraview. However, Paraview cannot read abaqus odb files, but it reads vtk Files, thus a converter is necessary to create vtk files from an ABAQUS odb. This task has been accomplished by Ingo Scheider based on some preliminary work by Qingbin Liu.

The current version of the software described here is 1.3