Usage
======

The converter can be used as a standalone program or it can be imported as a module in abaqus python. Both application methods are described in the following.

Standalone program
------------------

The program to be called from the command line is

.. code:: sh

   $> convertVTK

If you type convertVTK -h, you get the extended help message:

.. code:: sh

   $> convertVTK -h
   usage: convertVTK [-h] [--abaqus ABAQUS] (-x XMLFILE | -o ABQFILE | -w ABQFILE)

   optional arguments:
      -h, --help            show this help message and exit

      --abaqus ABAQUS       abaqus command to be used

      -x XMLFILE, --xml XMLFILE
                           Run convertVTK with an existing xml controlfile

      -o ABQFILE, --odb ABQFILE
                        Run convertVTK on an abaqus odb file

      -w ABQFILE, --writeXML ABQFILE
                        Write xml control file for the given odb without conversion

Method 1: from odb file
^^^^^^^^^^^^^^^^^^^^^^^^

This tool creates a set of vtk files from the given odb file in the directory <odbname>.vtk.

.. code::

   $> convertVTK --odb odbfile 

An xml control file is then created as well.

.. note::
   All instances, steps, frames and variables are written, which may lead to exhaustive time and space needed!

Method 2: from xml control file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code::

   $> convertVTK --xml xmlfile

The method with an xml control file is significantly more versatile, since the user can control, which data is transferred to the vtk files. The format of the xml file is described below.

Utility: Write xml control file from odb 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code::

   $> convertVTK writexml=odbfile

With this tool an xml file is created based on the given odb file. This is useful, since the user can then first edit the xml file and run the above tool based with a specified xml file instead of the odb.

Class convertVTK
----------------

It is planned that useful commands are provided in a collection of classes and methods at a central place. For the use of the command as a class, a module can be loaded:
from miscEvaluate import ConvertVTK

This module contains a class for the conversion:
ConvertVTK(xml=<filename> odb=<filename)
(you may provide either an xml or an odb file). 

The Class itself contains a number of methods:

   - :code:`getParameterFromXml(<filename>)` Used to get the parameters from an xml control file. This method is automatically called if the xml option is used in the class definition. It returns an instance of the parameter class, if the call is successful.

   - :code:`getParameterFromOdb(<filename>)` Used to get the parameters from an odb file. This method is automatically called if the xml option is used in the class definition. it returns an instance of the parameter class, if the call is successful.

   - :code:`readOdb()`   This actually reads the main content (instances, nodes, elements, steps, and frames), but not the result data. This method is automatically called, if the class definition is called with either xml or odb option.

   - :code:`showParameter()`   The parameters that are currently active are printed to stdout when called.

   - :code:`writeXml()`   This method is used to write an xml control file from the current parameters, which is useful for later modification of the conversion.

   - :code:`showElementTypes()` This methods shows the currently implemented element types.

   - :code:`addElementType(<ABAqusElementType>, <vtkElementNumber>, <nodes>)` If an element type is not implemented, it can be added using this method. One has to know the vtkCellNumber, which is given in https://vtk.org/wp-content/uploads/2015/04/file-formats.pdf, page 9ff.

   - :code:`do()` This is the actual method that does the conversion based on the current parameters.


Further undocumented methods:

   - :code:`writeMapfile()`

The tree commands mentioned can also be called by python commands, as simple as:

1. The command :code:`ConvertVTK --xml <filename>` is equivalent to

.. code::

    from myEvalCls import ConvertVTK
    vtk = ConvertVTK(xml=<filename>)
    vtk.do()

2. The command :code:`ConvertVTK --writexml <filename>` is equivalent to

.. code::

    from myEvalCls import ConvertVTK
    vtk = ConvertVTK(writeXml=<filename>)
    vtk.writeXml()

3. The command :code:`ConvertVTK --odb <filename>` is equivalent to

.. code::

    from myEvalCls import ConvertVTK
    vtk = ConvertVTK(odb=<filename>)
    vtk.writeXml()
    vtk.do()

One can also read the odb, then modify the parameters, for example eliminate some instances or frames in the conversion, and then convert the remaining content.