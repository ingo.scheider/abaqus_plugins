# -*- coding: mbcs -*-
from part import *
from material import *
from section import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from optimization import *
from job import *
from sketch import *
from visualization import *
from connectorBehavior import *

def createFCCstructure(particleRadius,particleDist,nParticles,inputName):
    particleSpacing=(2*particleRadius+particleDist)*sqrt(2.)
    rveLength=particleSpacing*(nParticles-1)
    loadAmplitude=rveLength/10.
    elementLength=particleRadius/5.
    #
    eps=particleDist/100.
    totalParticles = 8/8. #vertices
    totalParticles += 12*(nParticles-2)/4. #edges
    totalParticles += 6*((nParticles-2)*(nParticles-2)+(nParticles-1)*(nParticles-1))/2. #faces
    totalParticles +=2*(nParticles-2)*(nParticles-1)*(nParticles-1)+((nParticles-2)*(nParticles-2)+(nParticles-1)*(nParticles-1))*(nParticles-2)
    volumeParticle = 4./3.*pi*particleRadius**3
    volumeParticles=totalParticles*volumeParticle
    volumeRatio=volumeParticles/rveLength**3
    print "The volume ratio is {0:.1f}%".format(volumeRatio*100)
    print "The length of the RVE is {0}".format(rveLength)
    inputDescriptor="RVE pRad=10., pDist=1.,nParticle=2, length={0}, volRatio={1}".format(rveLength,volumeRatio)
    ###################
    # PARTS
    ###################
    # full sphere
    mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
    mdb.models['Model-1'].sketches['__profile__'].ConstructionLine(point1=(0.0, 
        -100.0), point2=(0.0, 100.0))
    mdb.models['Model-1'].sketches['__profile__'].ArcByCenterEnds(center=(0.0, 0.0)
        , direction=CLOCKWISE, point1=(0.0, particleRadius), point2=(0.0, -particleRadius))
    mdb.models['Model-1'].sketches['__profile__'].Line(point1=(0.0, -particleRadius), point2=
        (0.0, particleRadius))
    p_particle=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='partikel', type=
        DEFORMABLE_BODY)
    p_particle.BaseSolidRevolve(angle=360.0, flipRevolveDirection=OFF, 
        sketch=mdb.models['Model-1'].sketches['__profile__'])
    del mdb.models['Model-1'].sketches['__profile__']
    #
    xAxis=p_particle.DatumAxisByPrincipalAxis(principalAxis=XAXIS)
    yAxis=p_particle.DatumAxisByPrincipalAxis(principalAxis=YAXIS)
    zAxis=p_particle.DatumAxisByPrincipalAxis(principalAxis=ZAXIS)
    centerPoint=p_particle.DatumPointByCoordinate(coords=(0.,0.,0.))
    p_particle.PartitionCellByPlanePointNormal(cells=p_particle.cells[:], 
        normal=p_particle.datum[xAxis.id], point=p_particle.datum[centerPoint.id])
    p_particle.PartitionCellByPlanePointNormal(cells=p_particle.cells[:], 
        normal=p_particle.datum[yAxis.id], point=p_particle.datum[centerPoint.id])
    p_particle.PartitionCellByPlanePointNormal(cells=p_particle.cells[:], 
        normal=p_particle.datum[zAxis.id], point=p_particle.datum[centerPoint.id])
    # matrix
    mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
    mdb.models['Model-1'].sketches['__profile__'].rectangle(point1=(0.0, 0.0), 
        point2=(rveLength,rveLength))
    p_matrix=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='matrix', 
        type=DEFORMABLE_BODY)
    p_matrix.BaseSolidExtrude(depth=rveLength, sketch=
        mdb.models['Model-1'].sketches['__profile__'])
    del mdb.models['Model-1'].sketches['__profile__']
    # cutout
    mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
    mdb.models['Model-1'].sketches['__profile__'].rectangle(point1=(0.0, 0.0), 
        point2=(rveLength*5,rveLength*5))
    p_cutout=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='cutout', 
        type=DEFORMABLE_BODY)
    p_cutout.BaseSolidExtrude(depth=rveLength*5, 
        sketch=mdb.models['Model-1'].sketches['__profile__'])
    del mdb.models['Model-1'].sketches['__profile__']
    #
    ###################
    # PROPERTIES
    ###################
    mdb.models['Model-1'].Material(name='PolyButagen')
    mdb.models['Model-1'].materials['PolyButagen'].Hyperelastic(materialType=
        ISOTROPIC, table=((1.5, 1e-05), ), testData=OFF, type=NEO_HOOKE, 
        volumetricResponse=VOLUMETRIC_DATA)
    mdb.models['Model-1'].Material(name='Eisenoxid')
    mdb.models['Model-1'].materials['Eisenoxid'].Elastic(table=((160000.0, 0.3), ))
    mdb.models['Model-1'].HomogeneousSolidSection(material='Eisenoxid', 
        name='particle', thickness=None)
    mdb.models['Model-1'].HomogeneousSolidSection(material='PolyButagen', 
        name='matrix', thickness=None)
    #
    ###################
    # ASSEMBLY
    ###################
    mdb.models['Model-1'].rootAssembly.DatumCsysByDefault(CARTESIAN)
    asm=mdb.models['Model-1'].rootAssembly
    asm.Instance(dependent=ON, name='matrix-1', part=p_matrix)
    asm.Instance(dependent=ON, name='partikel-1', part=p_particle)
    # asm.translate(instanceList=('partikel-1', ), 
        # vector=(10.0, 10.0, 10.0))
    asm.LinearInstancePattern(direction1=(1.0, 0.0, 0.0), direction2=(0.0, 1.0, 0.0), 
        instanceList=('partikel-1', ), number1=int(nParticles),number2=int(nParticles), 
        spacing1=particleSpacing, spacing2=particleSpacing)
    asm.features.changeKey(fromName='partikel-1', toName='partikel-1-lin-1-1')
    particleList1 = []
    for dir1 in range(nParticles):
        for dir2 in range(nParticles):
            particleList1.append("partikel-1-lin-{0}-{1}".format(dir1+1,dir2+1))
    asm.LinearInstancePattern(direction1=(1.0, 1.0, 0.0), direction2=(-1.0, 1.0, 0.0), 
        instanceList=particleList1, 
        number1=2, number2=1, spacing1=(2*particleRadius+particleDist), spacing2=1.)
    for dir1 in range(nParticles):
        for dir2 in range(nParticles):
            oldInstance="partikel-1-lin-{0}-{1}-lin-2-1".format(dir1+1,dir2+1)
            newInstance="partikel-1-lin-{0}-{1}a".format(dir1+1,dir2+1)
            asm.features.changeKey(fromName=oldInstance, toName=newInstance)
            particleList1.append(newInstance)
    asm.LinearInstancePattern(direction1=(0.0, 0.0, 1.0), direction2=(0.0, 1.0, 0.0), 
        instanceList=(particleList1), number1=nParticles, 
        number2=1, spacing1=particleSpacing, spacing2=1.0)
    particleList2 = []
    for particleName in particleList1:
        for dir in range(1,nParticles):
            oldInstance="{0}-lin-{1}-1".format(particleName,dir+1)
            newInstance=oldInstance.replace("1",repr(dir),1)
            particleList2.append(newInstance)
            asm.features.changeKey(fromName=oldInstance, toName=newInstance)
    asm.LinearInstancePattern(direction1=(-1.0, 0.0, 1.0), direction2=(0.0, -1.0, 1.0), 
        instanceList=particleList1+particleList2, number1=2, number2=2, 
        spacing1=particleSpacing/sqrt(2), spacing2=particleSpacing/sqrt(2))
    #
    # merging the particles with the matrix
    mergeList=[]
    for inst in asm.instances.keys():
        if inst[:4]=="part" or inst[:4]=="matr":
            mergeList.append(asm.instances[inst])
    asm.InstanceFromBooleanMerge(domain=GEOMETRY, 
        instances=mergeList, 
        keepIntersections=ON, name='composite', originalInstances=DELETE)
    #
    # generating the mask
    asm.Instance(dependent=ON, name='matrix-cutout', part=p_matrix)
    asm.Instance(dependent=ON, name='cutout-1', part=p_cutout)
    asm.translate(instanceList=('cutout-1', ), 
        vector=(-rveLength*2,-rveLength*2,-rveLength*2))
    asm.InstanceFromBooleanCut(cuttingInstances=(
        asm.instances['matrix-cutout'], ), 
        instanceToBeCut=asm.instances['cutout-1'], 
        name='mask', originalInstances=DELETE)
    #
    # cutting the rve
    asm.InstanceFromBooleanCut(cuttingInstances=(
        asm.instances['mask-1'], ), 
        instanceToBeCut=asm.instances['composite-1'], 
        name='rve', originalInstances=DELETE)
    p_rve=mdb.models['Model-1'].parts['rve']
    #
    ###################
    # STEP
    ###################
    mdb.models['Model-1'].StaticStep(initialInc=0.05, maxNumInc=10000, 
        name='Step-1', nlgeom=ON, previous='Initial')
    # 
    xminSequence = ()
    xmaxSequence = ()
    yminSequence = ()
    ymaxSequence = ()
    zminSequence = ()
    zmaxSequence = ()
    #
    p_rve.Set(name="pset_matrix",cells=p_rve.cells.findAt(((0.,0.,particleRadius+0.5*particleDist),),))
    p_rve.Set(name="pset_allElements",cells=p_rve.cells[:])
    p_rve.SetByBoolean(name="pset_particle",operation=DIFFERENCE,sets=(p_rve.sets["pset_allElements"],p_rve.sets["pset_matrix"]))
    p_rve.SectionAssignment(offset=0.0, 
        offsetField='', offsetType=MIDDLE_SURFACE, region=
        p_rve.sets['pset_particle'], sectionName=
        'particle', thicknessAssignment=FROM_SECTION)
    p_rve.SectionAssignment(offset=0.0, 
        offsetField='', offsetType=MIDDLE_SURFACE, region=p_rve.sets['pset_matrix'], 
        sectionName='matrix', thicknessAssignment=FROM_SECTION)
    ###################
    # BOUNDARY CONDITIONS
    ###################
    for face in asm.instances['rve-1'].faces:
        point = face.pointOn[0]
        if point[0]<eps:
            xminSequence += (asm.instances['rve-1'].faces[face.index:face.index+1],)
            # belongs to xmin
        elif point[0]>rveLength-eps:
            xmaxSequence += (asm.instances['rve-1'].faces[face.index:face.index+1],)
            # belongs to xmax
        if point[1]<eps:
            yminSequence += (asm.instances['rve-1'].faces[face.index:face.index+1],)
            # belongs to ymin
        elif point[1]>rveLength-eps:
            ymaxSequence += (asm.instances['rve-1'].faces[face.index:face.index+1],)
            # belongs to ymax
        if point[2]<eps:
            zminSequence += (asm.instances['rve-1'].faces[face.index:face.index+1],)
            # belongs to zmin
        elif point[2]>rveLength-eps:
            zmaxSequence += (asm.instances['rve-1'].faces[face.index:face.index+1],)
            # belongs to zmax
    asm.Set(faces=xminSequence, name='xMin')
    asm.Set(faces=xmaxSequence, name='xMax')
    asm.Set(faces=yminSequence, name='yMin')
    asm.Set(faces=ymaxSequence, name='yMax')
    asm.Set(faces=zminSequence, name='zMin')
    asm.Set(faces=zmaxSequence, name='zMax')
    mdb.models['Model-1'].ZsymmBC(createStepName='Step-1', localCsys=None, name=
        'zMin', region=asm.sets['zMin'])
    mdb.models['Model-1'].XsymmBC(createStepName='Step-1', localCsys=None, name=
        'xMin', region=asm.sets['xMin'])
    mdb.models['Model-1'].XsymmBC(createStepName='Step-1', localCsys=None, name=
        'xMax', region=asm.sets['xMax'])
    mdb.models['Model-1'].YsymmBC(createStepName='Step-1', localCsys=None, name=
        'yMin', region=asm.sets['yMin'])
    mdb.models['Model-1'].YsymmBC(createStepName='Step-1', localCsys=None, name=
        'yMax', region=asm.sets['yMax'])
    #refDatum=asm.DatumPointByCoordinate(coords=(rveLength/2.,rveLength/2.,rveLength))
    refPoint=asm.ReferencePoint(point=(rveLength/2.,rveLength/2.,rveLength))
    asm.Set(referencePoints=(asm.referencePoints[refPoint.id],),name="refLoad")
    mdb.models['Model-1'].Equation(name='Constraint-2', terms=((1.0, 'zMax', 3), (
        -1.0, 'refLoad', 3)))
    mdb.models['Model-1'].DisplacementBC(amplitude=UNSET, createStepName='Step-1', 
        distributionType=UNIFORM, fieldName='', localCsys=None, name='load', 
        region=asm.sets['refLoad'], u1=UNSET, u2=UNSET, 
        u3=loadAmplitude, ur1=UNSET, ur2=UNSET, ur3=UNSET)
    ###################
    # MESHING
    ###################
    p_rve.setMeshControls(elemShape=TET, regions=(p_rve.cells[:]), technique=FREE)
    p_rve.setElementType(elemTypes=(ElemType(
        elemCode=C3D20R, elemLibrary=STANDARD), ElemType(elemCode=C3D15, 
        elemLibrary=STANDARD), ElemType(elemCode=C3D10H, elemLibrary=STANDARD)), 
        regions=p_rve.sets['pset_allElements'])
    mdb.models['Model-1'].parts['rve'].seedPart(deviationFactor=0.1, 
        minSizeFactor=0.1, size=elementLength)
    mdb.models['Model-1'].parts['rve'].generateMesh()
    ###################
    # JOB
    ###################
    mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(variables=(
        'S', 'LE', 'U'))
    mdb.models['Model-1'].historyOutputRequests['H-Output-1'].setValues(rebar=
        EXCLUDE, region=mdb.models['Model-1'].rootAssembly.sets['refLoad'], 
        sectionPoints=DEFAULT, variables=('RF3', ))
    mdb.Job(description=inputDescriptor, 
        contactPrint=OFF, echoPrint=OFF, historyPrint=OFF, modelPrint=OFF, 
        getMemoryFromAnalysis=True, memory=90, memoryUnits=PERCENTAGE, model='Model-1', 
        multiprocessingMode=DEFAULT, name=inputName, 
        numCpus=1, queue=None, resultsFormat=ODB, scratch='', 
        type=ANALYSIS, userSubroutine='')

###################
# PARameters
###################
particleRadius=10.
particleDist=1.
nParticles=2
inputName="rveFCC2-10-1"

createFCCstructure(particleRadius,particleDist,nParticles,inputName)
