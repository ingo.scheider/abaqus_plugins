# ABAQUS_Plugins

This repository contains a number of tools which may help for use of ABAQUS/Viewer or ABAQUS/CAE. Some of the tools are available as abaqus plugins, other are just in the abaqus_plugins directory in order to make it available as module for other scripts

## Installation

This set of tools must be placed into the abaqus_plugins directory, which is usually the $HOME/abaqus_plugins directory. If the directory is not found by abaqus (for example, in case the line `include myEvalCls` in the abaqus/Viewer script window throws an error), put the abaqus_plugins directory into the python path, i.e.:

`export PYTHONPATH=$HOME/abaqus_plugins:$PYTHONPATH`

## Tools description

### periodicBC

This is an ABAQUS/Viewer plugin, which reads an odb and applies periodic boundary conditions and optionally a dummy element for prescribing a load in an arbitrary direction. 

### odb2vtk

This is a python script which converts the results in an odb file into Paraview-readable vtk files.
The control file is written in xml format. Here, an example for the control file is placed: `odb2vtk.xml`.
The optional commands therein are commented out.
An additional documentation exists: `odb2vtk_Documentation_1.3.pdf`

### Indenter

This is an ABAQUS/CAE plugin for the generation of a 3D indentation model for arbitrarily faced indenters. Number and face angle of the indenter faces can be chosen. It is always a full 3D model, no symmetries are used.

### Microcolumn


This is an ABAQUS/CAE plugin for the generation of a micro column model for the simulation of a micro compression test. At this time, only a 90degree section can be chosen (double symmetry). The compression can be employed by contact of a rigid plate or by displacement boundary conditions.

### cantileverHalf

This is a very particular ABAQUS/CAE plugin for a pentagon shaped micro cantilever beam model. The loading can be chosen either by contact of a ridig indenter or by displacement boundary conditions at the free end. Extra material at the fixed end of the contilever can be added.

### FracSpecBuilder

This is an ABAQUS/CAE plugin for the generation of a fracture specimen for a crack propagation analysis. Several sizes and boundary conditions are possible to model either a C(T), SE(T) or M(T) specimen.

### myEvalCls/myEvalClsG

This is a module wthout graphical interface. It can be loaded by other scripts in order to get curves easily from history outputs of an odb or even from a number of odbs.
