import pytest

import sys 
from os import getcwd
from os import path
cwd = getcwd()
print(cwd)
splitpath = path.split(cwd)
newpath = list(splitpath[:-1]) + ["abqInputRead"]
newpathstr = path.join(*newpath)
sys.path.append(newpathstr)

from abqInputRead import readABQinput

@pytest.mark.parametrize("inputfile, nnodes_test, nelem_test, nmodelbcs_test, nmats_test, nsteps_test, nstepbcs_test", 
                         [("cubehex8", 27, 8, 3, 1, 1, 1),
                          ("cubehex8_parts", 27, 8, 0, 1, 1, 4),
                          ("cubehex8_inst", 27, 8, 0, 1, 1, 4)
                         ]
                        )

def test_abqInputRead(inputfile, nnodes_test, nelem_test, nmodelbcs_test, nmats_test, nsteps_test, nstepbcs_test):
    (asm,parts,bcs, mats, steps) = readABQinput(path.join(newpathstr, inputfile))
    nnodes = len(asm.nodes)
    nelem = len(asm.element)
    for inst in asm.instances:
      nnodes += len(asm.instances[inst].nodes)
      nelem += len(asm.instances[inst].element)
    nbc = len(bcs)
    nmats = len(mats)
    print("%d nodes, %d elements, %d model boundary conditions, %d materials" % 
    (nnodes, nelem, nbc, nmats))
    nsteps = len(steps)
    for i,step in enumerate(steps.values()):
        print("Step %d: %d step boundary conditions" % (i+1, len(step.bconditions)))
    assert nnodes==nnodes_test and nelem==nelem_test and nsteps==nsteps_test

    
