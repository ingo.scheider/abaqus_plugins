# -*- coding: mbcs -*-
from part import *
from material import *
from section import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from job import *
from sketch import *
def createIndenter(eindringtiefe,angle,faces,elasticIndent,bodyradius,bodyheight,
	innerCircleHeightFraction,outerMeshsize,innerMeshsize,inputstyle):
    beta=360./faces
    realradius=eindringtiefe*tan(degreeToRadian(angle))/(1.-cos(degreeToRadian(beta/2.)))
    radius=realradius*1.2
    realheight=radius/tan(degreeToRadian(angle))
    height=realheight*1.2

    innerCircleRadius=radius     # originally 2.5
    mediumMeshsize=sqrt(innerMeshsize*outerMeshsize)  # originally 0.5
    loadvalue=eindringtiefe
    
    fullModel=True

    print "Minimum radius of the indenter: ",realradius
    print "Constructed radius of the indenter: ",radius
    print "Minimum height of the indenter", realheight
    print "Constructed height of the indenter", height

    #
    # INDENTER
    #
    mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
    mdb.models['Model-1'].sketches['__profile__'].CircleByCenterPerimeter(center=(0., 0.), 
        point1=(radius, 0.0))
    berkovichBase=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='berkovichBase', 
        type=DEFORMABLE_BODY)
    berkovichBase.BaseSolidExtrude(depth=height, sketch=mdb.models['Model-1'].sketches['__profile__'])
    del mdb.models['Model-1'].sketches['__profile__']
    #
    mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
    mdb.models['Model-1'].sketches['__profile__'].rectangle(point1=(-2*radius, -2*radius), 
        point2=(2*radius, 2*radius))
    cuttingBody=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='cuttingBody', type=DEFORMABLE_BODY)
    cuttingBody.BaseSolidExtrude(depth=2*height, sketch=mdb.models['Model-1'].sketches['__profile__'])
    del mdb.models['Model-1'].sketches['__profile__']
    #
    # ASSEMBLY
    #
    mdb.models['Model-1'].rootAssembly.DatumCsysByDefault(CARTESIAN)
    asm=mdb.models['Model-1'].rootAssembly
    berk0=asm.Instance(dependent=ON, name='berk0-1', part=berkovichBase)
    cut1=asm.Instance(dependent=ON, name='cuttingBody-1', part=cuttingBody)
    cut1.translate(vector=(0.0, 0.0, -2*height))
    cut1.rotateAboutAxis(angle=(90.-angle), axisDirection=(1.0, 0.0, 0.0), 
        axisPoint=(0.0, 0.0, 0.0))
    asm.InstanceFromBooleanCut(cuttingInstances=(cut1, ), 
        instanceToBeCut=asm.instances['berk0-1'], name='berk1', originalInstances=DELETE)
    #
    if fullModel:
        for face in range(1,faces):
            rotationAxis=asm.DatumAxisByRotation(angle=face*beta, axis=asm.datums[1].axis3, 
                line=asm.datums[1].axis1)
            cut1=asm.Instance(dependent=ON, name='cuttingBody-1', part=cuttingBody)
            cut1.translate(vector=(0.0, 0.0, -2*height))
            cut1.rotateAboutAxis(angle=(90.-angle), axisDirection=asm.datums[rotationAxis.id].direction,
                axisPoint=(0.0, 0.0, 0.0))
            asm.InstanceFromBooleanCut(cuttingInstances=(cut1, ), 
                instanceToBeCut=asm.instances['berk'+repr(face)+'-1'], name='berk'+repr(face+1), originalInstances=DELETE)

        berkovich=mdb.models['Model-1'].parts['berk'+repr(faces)]
        berkovichInstance=asm.instances['berk'+repr(faces)+'-1']
    #
    #
    if elasticIndent:
        asm.Set(name='loadpoint', 
            faces=berkovichInstance.faces.getByBoundingBox(zMin=height*0.99) )
    else:
        berkovich.RemoveCells(cellList=berkovich.cells[:])
        berkovich.setValues(space=THREE_D, type=DISCRETE_RIGID_SURFACE)
        berkovich.ReferencePoint(point=berkovich.InterestingPoint(berkovich.edges.findAt((0.,radius, height),), 
            CENTER))
        asm.regenerate()
        refPointId=berkovichInstance.referencePoints.keys()[0]
        asm.Set(name='loadpoint', referencePoints=(berkovichInstance.referencePoints[refPointId], ) )

    asm.Surface(name='IndenterSurf', side1Faces=berkovichInstance.faces.getByBoundingBox(zMax=height*0.99) )
#
# Generation of the body
#
    innerCircleHeight=(1.-innerCircleHeightFraction)*bodyheight
    inputName="indent_r{0:d}_h{1:d}".format(int(bodyradius),int(bodyheight))
    print inputName
    #
    mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
    mdb.models['Model-1'].sketches['__profile__'].ConstructionLine(point1=(0.0, 
        -100.0), point2=(0.0, 100.0))
    mdb.models['Model-1'].sketches['__profile__'].rectangle(point1=(0.0, 0.0), 
        point2=(bodyradius,bodyheight))
    structurePart=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='structure', 
        type=DEFORMABLE_BODY)
    structurePart.BaseSolidRevolve(angle=360.0, 
        flipRevolveDirection=OFF, sketch=mdb.models['Model-1'].sketches['__profile__'])
    del mdb.models['Model-1'].sketches['__profile__']
    #
    mdb.models['Model-1'].ConstrainedSketch(gridSpacing=3.83, name='__profile__', 
        sheetSize=153.33, transform=structurePart.MakeSketchTransform(
        sketchPlane=structurePart.faces[0],sketchPlaneSide=SIDE1, 
        sketchUpEdge=structurePart.edges[0], sketchOrientation=RIGHT, origin=(0.0, bodyheight, 0.0)))
    structurePart.projectReferencesOntoSketch(filter=
        COPLANAR_EDGES, sketch=mdb.models['Model-1'].sketches['__profile__'])
    mdb.models['Model-1'].sketches['__profile__'].CircleByCenterPerimeter(center=(
        0.0, 0.0), point1=(innerCircleRadius, 0.0))
    structurePart.PartitionFaceBySketch(faces=
        structurePart.faces.findAt(((0.,bodyheight,0.),),), 
        sketch=mdb.models['Model-1'].sketches['__profile__'], 
        sketchUpEdge=structurePart.edges[0])
    del mdb.models['Model-1'].sketches['__profile__']
    #
    structurePart.PartitionEdgeByParam(edges=
        structurePart.edges.findAt(((bodyradius, bodyheight/2., 0.),),), 
        parameter=innerCircleHeightFraction)
    structurePart.PartitionCellByPlanePointNormal(cells=
        structurePart.cells[:], 
        normal=structurePart.edges.findAt((bodyradius, bodyheight/2., 0.),), 
        point=structurePart.vertices[2])
    structurePart.PartitionCellByExtrudeEdge(cells=
        structurePart.cells.getSequenceFromMask(('[#2 ]', ), ), 
        edges=(structurePart.edges[3], ), 
        line=structurePart.edges[1], sense=FORWARD)
    structurePart.PartitionCellByExtrudeEdge(cells=
        structurePart.cells.getSequenceFromMask(('[#2 ]', ), ), 
        edges=(structurePart.edges[1], ), 
        line=structurePart.edges[5], sense=FORWARD)
    #
    # PROPERTIES
    #
    mdb.models['Model-1'].Material(name='indenter')
    mdb.models['Model-1'].materials['indenter'].Elastic(type=ISOTROPIC, 
        table=((1141000, 0.07), ))
    mdb.models['Model-1'].HomogeneousSolidSection(material='indenter', 
        name='indenter', thickness=None)
    if elasticIndent:
        berkovich.Set(cells=berkovich.cells[:], name='indenter')
        berkovich.SectionAssignment(offset=0.0, offsetField='', 
            offsetType=MIDDLE_SURFACE, region=berkovich.sets['indenter'], 
            sectionName='indenter', thicknessAssignment=FROM_SECTION)
    else:
        berkovich.Set(name='indenter', faces=berkovich.faces[:])
    #
    mdb.models['Model-1'].Material(name='average')
    mdb.models['Model-1'].materials['average'].Hyperelastic(materialType=ISOTROPIC, 
        table=((1000.0, 2.0, 0.0001), ), testData=OFF, type=OGDEN, 
        volumetricResponse=VOLUMETRIC_DATA)
    mdb.models['Model-1'].HomogeneousSolidSection(material='average', 
        name='structure', thickness=None)
    structurePart.Set(cells=structurePart.cells[:], name='structure')
    structurePart.SectionAssignment(offset=0.0, 
        offsetField='', offsetType=MIDDLE_SURFACE, region=
        structurePart.sets['structure'], 
        sectionName='structure', thicknessAssignment=FROM_SECTION)
    #
    # ASSEMBLY
    #
    mdb.models['Model-1'].rootAssembly.DatumCsysByDefault(CARTESIAN)
    myAsm = mdb.models['Model-1'].rootAssembly
    structureInstance=myAsm.Instance(dependent=ON, name='structure-1', part=structurePart)
    #
    # INTERACTION 
    #
    mdb.models['Model-1'].ContactProperty('IntProp-1')
    mdb.models['Model-1'].interactionProperties['IntProp-1'].TangentialBehavior(
        dependencies=0, directionality=ISOTROPIC, elasticSlipStiffness=None, 
        formulation=PENALTY, fraction=0.005, maximumElasticSlip=FRACTION, 
        pressureDependency=OFF, shearStressLimit=None, slipRateDependency=OFF, 
        table=((0.1, ), ), temperatureDependency=OFF)
    mdb.models['Model-1'].interactionProperties['IntProp-1'].NormalBehavior(
        allowSeparation=ON, constraintEnforcementMethod=DEFAULT, 
        pressureOverclosure=HARD)
    myAsm.Surface(name='StructureSurf', side1Faces=structureInstance.faces.findAt(((0.,bodyheight,0.),),) )
    mdb.models['Model-1'].SurfaceToSurfaceContactStd(adjustMethod=NONE, 
        clearanceRegion=None, createStepName='Initial', datumAxis=None, 
        initialClearance=OMIT, interactionProperty='IntProp-1', 
        master=myAsm.surfaces['IndenterSurf'], name='Int-1', 
        slave=myAsm.surfaces['StructureSurf'], sliding=SMALL, thickness=ON)
    #
    # STEP
    #
    mdb.models['Model-1'].StaticStep(initialInc=0.01, maxInc=0.1, maxNumInc=1000, 
        name='Step-1', nlgeom=ON, previous='Initial')
    #
    # BOUNDARY CONDITIONS
    #
    localCSys=myAsm.DatumCsysByThreePoints(coordSysType=
        CYLINDRICAL, name='cylinderCoord', origin=(0.0, 0.0, 0.0), 
        point1=(1.0, 0.0, 0.0), point2=(0.0, 1.0, 0.0))
    mdb.models['Model-1'].DisplacementBC(amplitude=UNSET, createStepName='Step-1', 
        distributionType=UNIFORM, fieldName='', fixed=OFF, 
        localCsys=myAsm.datums[localCSys.id], name='loadpoint', region=myAsm.sets['loadpoint'], 
        u1=0.0, u2=0.0, u3=-loadvalue, ur1=0.0, ur2=0.0, ur3=0.0)
    mdb.models['Model-1'].XsymmBC(createStepName='Step-1', localCsys=myAsm.datums[localCSys.id],
        name='radialSymm1', region=Region(faces=
        structureInstance.faces.findAt(((0.,bodyheight/2.,bodyradius),),) ))
    mdb.models['Model-1'].XsymmBC(createStepName='Step-1', localCsys=myAsm.datums[localCSys.id], 
        name='radialSymm2', 
        region=Region(faces=
        structureInstance.faces.findAt(((0.,(bodyheight+innerCircleHeight)/2.,bodyradius),),) ))
    mdb.models['Model-1'].ZsymmBC(createStepName='Step-1', localCsys=myAsm.datums[localCSys.id], 
        name='zSymm1', 
        region=Region(faces=structureInstance.faces.findAt(((0.,0.,innerCircleRadius/2.),),) ))
    mdb.models['Model-1'].ZsymmBC(createStepName='Step-1', localCsys=myAsm.datums[localCSys.id], 
        name='zSymm2', 
        region=Region(faces=structureInstance.faces.findAt(((0.,0.,(bodyradius+innerCircleRadius)/2.),),) ))
    #
    mdb.models['Model-1'].historyOutputRequests['H-Output-1'].setValues(rebar=EXCLUDE, 
        region=myAsm.sets['loadpoint'], 
        sectionPoints=DEFAULT, variables=('U1', 'U2', 'U3', 'RF1', 'RF2', 'RF3'))
    mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(rebar=EXCLUDE, 
        region=structureInstance.sets['structure'], 
    sectionPoints=DEFAULT, variables=('S', 'LE', 'U', 'CSTRESS', 'CDISP'))
    mdb.models['Model-1'].FieldOutputRequest(createStepName='Step-1', 
        name='F-Output-2', rebar=EXCLUDE, region=berkovichInstance.sets['indenter']
    , sectionPoints=DEFAULT, variables=('U', 'CSTRESS'))
    #
    # MESHING
    #
    # indenter
    berkovich.seedPart(deviationFactor=0.1, minSizeFactor=0.1, size=radius/4.)
    if elasticIndent:
        berkovich.setMeshControls(elemShape=TET, 
            regions=berkovich.cells[:], technique=FREE)
        berkovich.setElementType(elemTypes=(ElemType(
            elemCode=C3D8, elemLibrary=STANDARD), ElemType(elemCode=C3D6, 
            elemLibrary=STANDARD), ElemType(elemCode=C3D4, elemLibrary=STANDARD)), 
            regions=((berkovich.cells[:]),))
    else:
        berkovich.setElementType(elemTypes=(ElemType(elemCode=R3D4, elemLibrary=STANDARD), 
            ElemType(elemCode=R3D3, elemLibrary=STANDARD)), 
            regions=(berkovichInstance.faces[:],))
    berkovich.generateMesh()
    #
    # structure
    structurePart.setMeshControls(elemShape=TET, 
        regions=structurePart.cells[:], technique=FREE)
    structurePart.setElementType(elemTypes=(ElemType(
        elemCode=C3D20R, elemLibrary=STANDARD), ElemType(elemCode=C3D15, 
        elemLibrary=STANDARD), ElemType(elemCode=C3D10MH, elemLibrary=STANDARD)), 
        regions=((structurePart.cells[:]),))
    structurePart.seedPart(deviationFactor=0.1, 
        minSizeFactor=0.1, size=innerMeshsize)
    #
    structurePart.seedEdgeBySize(constraint=FINER, 
        deviationFactor=0.1, edges=(structurePart.edges.findAt((0.,bodyheight,innerCircleRadius),),),
        minSizeFactor=0.1, size=innerMeshsize)
    structurePart.seedEdgeBySize(constraint=FINER, 
        deviationFactor=0.1, edges=(structurePart.edges.findAt((0.,innerCircleHeight,innerCircleRadius),),),
        minSizeFactor=0.1, size=mediumMeshsize)
    structurePart.seedEdgeBySize(constraint=FINER, 
        deviationFactor=0.1, edges=(structurePart.edges.findAt((0.,innerCircleHeight,bodyradius),),),
        minSizeFactor=0.1, size=outerMeshsize)
    structurePart.seedEdgeBySize(constraint=FINER, 
        deviationFactor=0.1, edges=(structurePart.edges.findAt((0.,bodyheight,bodyradius),),),
        minSizeFactor=0.1, size=outerMeshsize)
    structurePart.seedEdgeBySize(constraint=FINER, 
        deviationFactor=0.1, edges=(structurePart.edges.findAt((0.,0.,bodyradius),),),
        minSizeFactor=0.1, size=outerMeshsize)
    structurePart.seedEdgeBySize(constraint=FINER, 
        deviationFactor=0.1, edges=(structurePart.edges.findAt((0.,0.,innerCircleRadius),),),
        minSizeFactor=0.1, size=outerMeshsize)
    structurePart.seedEdgeBySize(constraint=FINER, 
        deviationFactor=0.1, edges=(structurePart.edges.findAt((bodyradius,bodyheight/2.,0.),),),
        minSizeFactor=0.1, size=outerMeshsize)
    structurePart.seedEdgeBySize(constraint=FINER, 
        deviationFactor=0.1, edges=(structurePart.edges.findAt((bodyradius,(bodyheight+innerCircleHeight)/2.,0.),),),
        minSizeFactor=0.1, size=outerMeshsize)
    structurePart.generateMesh()
    #
    myAsm.regenerate()
    #
    # rotate structure instance
    #
    # asm.translate(instanceList=('structure-1', ), vector=(0.0, -bodyheight, 0.0))
    # asm.rotate(angle=90.0, axisDirection=(1.0, 0.0, 0.0), axisPoint=(0.0, 0.0, 0.0), 
        # instanceList=('structure-1', ))
    structureInstance.translate(vector=(0.0, -bodyheight, 0.0))
    structureInstance.rotateAboutAxis(angle=90.0, axisDirection=(1.0, 0.0, 0.0), axisPoint=(0.0, 0.0, 0.0))

    print "Model {0} has been generated".format(inputName)
    for instance in myAsm.instances.keys():
        nElements = len(myAsm.instances[instance].elements)
        nNodes = len(myAsm.instances[instance].nodes)
        print "    Instance {0}  contains {1} nodes and {2} elements".format(instance,nNodes,nElements)

    if inputstyle==0:
        mdb.models['Model-1'].setValues(noPartsInputFile=ON)

    
    if elasticIndent:
        indentString=" elastic "
    else:
        indentString=" rigid "
    description="Nano indentation simulation with"+indentString+"Indenter\n"
    description+="Indenter parameters:\n"
    description+="eindringtiefe="+repr(eindringtiefe)+"\n"
    description+="indenter face angle="+repr(angle)+"\n"
    description+="number of faces="+repr(faces)+"\n"
    description+="Geometry parameters:\n"
    description+="bodyradius="+repr(bodyradius)+"\n"
    description+="bodyheight="+repr(bodyheight)+"\n"
    description+="innerCircleHeightFraction="+repr(innerCircleHeightFraction)+"\n"
    description+="outerMeshsize="+repr(outerMeshsize)+"\n"
    description+="innerMeshsize="+repr(innerMeshsize)
    mdb.models['Model-1'].setValues(description)
    mdb.Job(atTime=None, contactPrint=OFF, description='', echoPrint=OFF, 
       explicitPrecision=SINGLE, getMemoryFromAnalysis=True, historyPrint=OFF, 
       memory=50, memoryUnits=PERCENTAGE, model='Model-1', modelPrint=OFF, 
       multiprocessingMode=DEFAULT, name=inputName, nodalOutputPrecision=
       SINGLE, numCpus=1, queue=None, scratch='', type=ANALYSIS, userSubroutine=''
       , waitHours=0, waitMinutes=0)
    mdb.jobs[inputName].writeInput()
    mdb.saveAs(pathName=inputName+'.cae')
    return
    
if __name__ == "__main__":
    eindringtiefe=1.
    angle=65.27
    faces=3
    createIndenter(eindringtiefe,angle,faces)