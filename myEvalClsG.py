try:
    from odbAccess import *
except ImportError:
    print("This module only makes sense within abaqus python\nExiting ...")
    exit()
from matplotlib import pyplot as plt
from sys import argv
from os import path
import miscEvaluate

print("\nATTENTION:")
print("This module provides the class history(), which is used to work on history curves within ABAQUS")
print("To get more information on the module, type:\n")
print("print(myEvalCls.history.__doc__)\n")

myEvalCls.replay.write("from myEvalClsG import history\n")

class history(myEvalCls.history):
    '''Extends myEvalCls.history by some plotting capability.
    Can be called with an odbname as parameter to automatically read all history curves

    methods:
    --------
    getFromOdb(odbname[,steps])
                    - returns a list with all history curves in the odb (for specified list of stepnumbers if given)

    combine(curve1,curve2)
                    - combines the y-values of two curves to a new curve, adding to curvelist
    join(curve1,curve2[,add2time])
                    - joins to curves (e.g. for different time steps),
                      adding the last x-value in curve1 to the x-value in curve 2 (if not add2time=False)
    evaluate(xExpressionString, yExpressionString)
                    - calculate new curve from existing curves, adding to curvelist
    derive(curve)   - calculating the numerical derivative of curve, adding to curvelist
    interpolate(curve, numbers[, minval][, maxval])
                    - interpolate to numbers values
    normalizeX(curve, [min, max]), normalizeY(curve, [min, max]), normalizeXY(curve)
                    - normalize y-values (or x-values, for normalizeX) between 0 and 1 if no min/max values are given

    # Working with curve selections:
    mark(*curves), markonly(*curves), markbylabel(string), markall(), marknone(), unmark(*curves)
                    - mark or unmark specified curves (for deletion, plotting or writing)
    write([header])
                    - writes the marked curves to file (by default to last odbname opened)
                      An additional header line may be given, which is written above all other header lines
    write1x([header])
                    - writes the y values of the marked curves to file (by default to last odbname opened),
                      with a single x value in the first column
                      An additional header line may be given, which is written above all other header lines
    delete()        - delete the marked curves

    # curve plotting
    plot(curve[,plotfilename])
                    - plot a single curve
                      and write thegraph to plotfilename if given
    plotmarked([plotfilename])
                    - plot all marked curves
                      and write the graph to plotfilename if given


    attributes
    ----------
    # general
    colseparator (string)
                    - the separator for the textfile to be written
    silent(bool)    - whether to print messages to screen or not
    rptfilename (string)
                    - filename for report file (by default current odb filename)

    # plotting
    xlabel, ylabel  - axis labels for plotting
    xmin,xmax,ymin,ymax
                    - axis min/max values for plotting
    showgrid (bool) - what the name says
    showlegend(bool)- what the name says
    '''

    def __init__(self, odb=None, steps=[]):
        myEvalCls.history.__init__(self, odb=odb, steps=steps)
        self.xlabel = "time"
        self.ylabel = "y-Value"
        self.__plotlimits = [None,None,None,None]
        self.showgrid = True
        self.showlegend = True


    def __set_xmin(self,value):
        self.__plotlimits[0] = value
    def __get_xmin(self):
        return self.__plotlimits[0]
    xmin = property(__get_xmin, __set_xmin)

    def __set_xmax(self,value):
        self.__plotlimits[1] = value
    def __get_xmax(self):
        return self.__plotlimits[1]
    xmax = property(__get_xmax, __set_xmax)

    def __set_ymin(self,value):
        self.__plotlimits[2] = value
    def __get_ymin(self):
        return self.__plotlimits[2]
    ymin = property(__get_ymin, __set_ymin)

    def __set_ymax(self,value):
        self.__plotlimits[3] = value
    def __get_ymax(self):
        return self.__plotlimits[3]
    ymax = property(__get_ymax, __set_ymax)

    def plot(self,curve,plotfilename=""):
        myEvalCls.replay.write("c.plot(%d, plotfilename=\"%s\")\n" % (curve, plotfilename))
        #
        # initialize figure
        fig,ax1=plt.subplots(figsize=(8.,4.))
        plt.grid(self.showgrid)
        #
        # set the scaling
        ax1.set_xlim(auto=True)
        ax1.set_ylim(auto=True)
        if self.__plotlimits[0]!=None: ax1.set_xlim(left=self.__plotlimits[0])
        if self.__plotlimits[1]!=None: ax1.set_xlim(right=self.__plotlimits[1])
        if self.__plotlimits[2]!=None: ax1.set_ylim(bottom=self.__plotlimits[2])
        if self.__plotlimits[3]!=None: ax1.set_ylim(top=self.__plotlimits[3])
        #
        # plot the curves
        (x,y,curvelabel) = self.curvelist[curve]
        labelstring = ""
        for ilabel in curvelabel:
            labelstring += labelstring+"|"
        ax1.plot(x, y, label=labelstring.strip("|"))
        if self.showlegend:
            ax1.legend()
        if plotfilename!="":
            plt.savefig(plotfilename,dpi=300)
        plt.show()
        return

    def plotmarked(self,plotfilename="",labellist=[],style=None):
        myEvalCls.replay.write("c.plotmarked(plotfilename=\"{:s}\", labellist={:s}, style={})\n".format(
                    plotfilename, repr(labellist), style))
        #
        # labels to be plotted
        if labellist==[]:
            labellist = range(len(self.curvelist[0][2]))
        #
        # style for each curve
        if type(style)==str:
            stylestring = style
        else:
            stylestring = "-"
        plotstyle = [stylestring] * len(self.curvelist)
        if type(style)==list:
            plotstyle = [plotstyle[i] if not self.marked[i] else style.pop(0) for i in range(len(self.marked))]
        #
        # initialize figure
        fig,ax1=plt.subplots(figsize=(8.,6.))
        plt.grid(self.showgrid)
        ax1.set_xlabel(self.xlabel)
        ax1.set_ylabel(self.ylabel)
        #
        # set the scaling
        ax1.set_xlim(auto=True)
        ax1.set_ylim(auto=True)
        if self.__plotlimits[0]!=None: ax1.set_xlim(left=self.__plotlimits[0])
        if self.__plotlimits[1]!=None: ax1.set_xlim(right=self.__plotlimits[1])
        if self.__plotlimits[2]!=None: ax1.set_ylim(bottom=self.__plotlimits[2])
        if self.__plotlimits[3]!=None: ax1.set_ylim(top=self.__plotlimits[3])
        #
        # plot the curves
        for i in range(len(self.curvelist)):
            if not self.marked[i]: continue
            (x,y,curvelabel) = self.curvelist[i]
            labelstring = ""
            for ilabel in labellist:
                labelstring += curvelabel[ilabel]+"|"
            ax1.plot(x, y, plotstyle[i], label=labelstring.strip("|"))
        if self.showlegend:
            ax1.legend()
        if plotfilename!="":
            plt.savefig(plotfilename,dpi=300)
        else:
            plt.show()
        return

    def test(self):
        myValue = repr(self.__repr__())
        print myValue
        print len(globals()), len(locals())
        print globals().keys()
        for var,values in locals().items():
            if var=="c":
                print values
                print repr(values)
            if repr(values)==myValue:
                return var

if __name__=="__main__":
    curvelist=[]
    c = history(argv[1])
    if len(argv)>1:
        for ar in argv[1:]:
            c.getFromOdb(ar)

