# -*- coding: utf-8 -*-
# from sys import argv
import numpy as np
"""
Created on Thu Aug 11 11:57:15 2011

@author: scheider
change log:
8.2.2017: Ability to read input files without assemblies (not finished)
6.4.2020: Read boundary conditions
"""


class Element:
    ElementFaces={"C3D4": [[1,2,3  ], [1,4,2  ], [2,4,3  ], [3,4,1  ]],
                  "C3D6": [[1,2,3  ], [4,6,5  ], [1,4,5,2], [2,5,6,3], [3,6,4,1]],
                  "C3D8": [[1,2,3,4], [5,8,7,6], [1,5,6,2], [2,6,7,3], [3,7,8,4], [4,8,5,1]],
                  "C3D10":[[1,2,3  ], [1,4,2  ], [2,4,3  ], [3,4,1  ]],
                  "C3D15":[[1,2,3  ], [4,6,5  ], [1,4,5,2], [2,5,6,3], [3,6,4,1]],
                  "C3D20":[[1,2,3,4], [5,8,7,6], [1,5,6,2], [2,6,7,3], [3,7,8,4], [4,8,5,1]]}

    def __init__(self, eltype, connectivity):
        self._eltype = eltype
        self._nodes = connectivity
        self._nnodes = len(connectivity)

    def __repr__(self):
        return str(self._nnodes)

    @property
    def eltype(self):
        return self._eltype

    @eltype.setter
    def eltype(self, string):
        self._eltype = string
        return

    @property
    def nodes(self):
        return self._nodes

    @nodes.setter
    def nodes(self, connectivity):
        self._nodes = connectivity
        self._nnodes = len(connectivity)

    def getFace(self, face):
        return

    def readFromLine(self, eltype, line):
        numlist = [eval(x) for x in line.split(",")]
        numlist.pop(0)
        self.eltype = eltype
        self.nodes = numlist
        return True


class Set:
    def __init__(self, name):
        self.name = name
        self.instances = []
        self.members = []

    def __repr__(self):
        setDict = {}
        for inst in self.instances:
            setDict[inst] = len(self.members)
        if len(self.instances)==0:
            setDict[0] = len(self.members)
        return repr(setDict)

    def countMembers(self):
        items = 0
        for memberlist in self.members:
            items += len(memberlist)
        return items

    def addMember(self, memberList, instance=""):
        if type(memberList)!=list:
            return False
        if instance=="":
            if len(self.instances)!=0:
                print("Cannot add instance-free members to instanced set ", self.name)
                return True
            else:
                pos = 0
        else:
            try:
                pos = self.instances.index(instance)
            except:
                self.instances.append(instance)
                pos = len(self.instances) - 1
        if (len(self.members)<=pos):
            self.members.append(memberList)
        else:
            self.members[pos] += memberList
        return True

    def getMember(self, instance=""):
        if instance=="":
            if len(self.instances)!=0:
                elemList = []
                for i in range(len(self.instances)):
                    for el in self.members[i]:
                        elemList.append(self.instances[i] + "." + str(el))
                return elemList
            else:
                pos = 0
        else:
            try:
                pos = self.instances.index(instance)
            except:
                return []
        return self.members[pos]

    def writeMember(self, file, instance=""):
        if instance=="":
            if len(self.instances)!=0:
                elemList = []
                for i in range(len(self.instances)):
                    for el in self.members[i]:
                        elemList.append(self.instances[i] + "." + str(el))
                return elemList
            else:
                pos = 0
        else:
            try:
                pos = self.instances.index(instance)
            except:
                return []
        return self.members[pos]


class Boundary:
    '''#=======================#
    #                       #
    #     Boundary CLASS    #
    #                       #
    #=======================#'''
    bcTypes={"XSYMM": (1,5,6),
             "YSYMM": (2,4,6),
             "ZSYMM": (3,4,5),
             "PINNED": (1,2,3),
             "ENCASTRE": (1,2,3,4,5,6)}

    def __init__(self,number):
        self.number=number
        self.set = None
        self.instance = ""
        self.dofs = []
        self.magnitude = 0.

    def addFromLine(self, line, assembly):
        ilist = line.split(",")
        bcregion = ilist[0].split(".")
        bcnode = 0
        bcinstance = "rootAssembly"
        if len(bcregion)>1:
            bcinstance = bcregion[0].strip().upper()
        try:
            # node number is given explicitly
            bcnode = eval(bcregion[-1])
            bcset = "_BC_" + repr(self.number)
        except:
            # node set is given
            bcset = bcregion[-1].strip().upper()
        # if node was given, then create a corresponding set
        if bcnode>0:
            if bcinstance=="":
                assembly.addNodesetLine(bcregion[-1], bcset, False)
            else:
                assembly.addNodesetLine(bcregion[-1], bcset, False, instance=bcinstance)
            print("Creating node set ", bcset)
        # Now set must exist!
        if len(ilist)>2:
            bcdofs = list(range(eval(ilist[1]), eval(ilist[2])+1))
            if len(ilist)>3:
                bcmagnitude = eval(ilist[3])
        else:
            bcType = ilist[1].strip().upper()
            if bcType not in Boundary.bcTypes: 
                return False
            bcdofs = Boundary.bcTypes(bcType)
            bcmagnitude = 0.0
        self.set = bcset
        self.instance = bcinstance
        self.dofs = bcdofs
        self.magnitude = bcmagnitude
        return True


class Step:
    '''#===================#
       #                   #
       #     STEP CLASS    #
       #                   #
       #===================#'''

    def __init__(self, stepname):
        self.name = stepname
        self.bconditions = []
        self.NLGEOM = False
        self.increments = 100

    def setNlgeom(self):
        self.NLGEOM = True
        return

    def setIncs(self, number):
        self.increments = number

    def readFromFile(self, inpfile, incfile, assembly):
        inpline = lineRead(inpfile, incfile)
        if inpline=="":
            return False
        while True:
            if (inpline[0]=="*"):
                comdict = commandRead(inpline)
                c = comdict["command"]
                if c=="*END STEP":
                    print("Reading step information finished")
                    return True
                elif c[:6]=="*BOUND":
                    while True:
                        inpline = lineRead(inpfile, incfile)
                        # weiß noch nicht, wie ich mit EOF umgehen soll
                        # if inpline=="": break
                        if (inpline[0]=="*"):
                            break
                        newbound = Boundary(len(self.bconditions))
                        if newbound.addFromLine(inpline, assembly):
                            self.bconditions.append(newbound)
                else:
                    inpline = lineRead(inpfile,incfile)
            else:
                inpline = lineRead(inpfile,incfile)


class Part:
    '''#====================#
       #                    #
       #     PART  CLASS    #
       #                    #
       #====================#'''
    partnames = []

    def __init__(self, partname):
        self.partnames.append(partname)
        self.name = partname
        self.nodes = {}
        self.element = {}
        self.nsets = {}
        self.elsets = {}
        self.sections = {}

    def __repr__(self):
        return self.name

    def getElemConnect(self,number):
        if number in self.element:
            return self.element[number].nodes
        else:
            print("Element number not available")
            return []

    def getElemType(self,number):
        if number in self.element:
            return self.element[number].eltype
        else:
            print("Element number not available")
            return ""

    def addElement(self, number, eltype, nodes):
        if type(nodes)==list and type(eltype)==str:
            self.element[number] = Element(eltype,nodes)
            return True
        else:
            return False

    def addElementLine(self, line, eltype):
        numlist = [eval(x) for x in line.split(",")]
        number = numlist.pop(0)
        self.addElement(number, eltype, numlist)
        return number

    def getNodeCoord(self, number):
        if number in self.nodes:
            return self.nodes[number]
        else:
            print("Node number not available")
            return []

    def addNode(self, number, coord):
        if type(coord)==list:
            self.nodes[number] = coord
            return True
        else:
            print("No coordinate list for node {0}".format(number))
            return False

    def addNodeLine(self, line):
        numlist=[eval(x) for x in line.split(",")]
        while len(numlist)<4:
            numlist += [0.]
        number = numlist.pop(0)
        self.addNode(number,numlist)
        return number

    def addNodesetLine(self, line, nodeset, generate):
        itemline = line.strip()
        if itemline[-1]==",":
            itemline = itemline[:-1]
        itemlist = itemline.split(",")
        if generate:
            itemlist = [eval(x) for x in itemlist]
            if len(itemlist)==2:
                itemlist += [1]
            itemlist = range(itemlist[0], (itemlist[1])+1, itemlist[2])
        if nodeset not in self.nsets:
            self.nsets[nodeset] = []
        for item in itemlist:
            try:
                nodeNumber = int(item)
            except:
                if item in self.nsets:
                    self.nsets[nodeset] += self.nsets[item]
                else:
                    print("Could not evaluate entry {0} for nodeset {1}: >>{2}<<".format(
                        item,nodeset,itemline))
                continue
            self.nsets[nodeset].append(nodeNumber)
        return

    def getNodeset(self, name):
        if name in self.nsets:
            return self.nsets[name]
        else:
            print("Nodeset not available")
            return []

    def addElsetLine(self, line, elset, generate):
        itemline = line.strip()
        if itemline[-1]==",":
            itemline = itemline[:-1]
        itemlist = itemline.split(",")
        if generate:
            itemlist = [eval(x) for x in itemlist]
            if len(itemlist)==2:
                itemlist += [1]
            itemlist = range(itemlist[0], (itemlist[1])+1, itemlist[2])
        if elset not in self.elsets:
            self.elsets[elset] = []
        for item in itemlist:
            try:
                elementNumber = int(item)
                self.elsets[elset].append(elementNumber)
            except:
                if item in self.elsets:
                    self.elsets[elset] += self.elsets[item]
                else:
                    print("Could not evaluate entry {0} for element set {1}".format(
                        item, elset))
        return

    def getElementset(self, name):
        if name in self.elsets:
            return self.elsets[name]
        else:
            print("Element set not available")
            return []

    def readFromFile(self, inpfile, incfile, inpline):
        if inpline=="":
            return False
        while True:
            if (inpline[0]=="*"):
                comdict = commandRead(inpline)
                c = comdict["command"]
                if c[:9] in ["*END INST", "*END PART"]:
                    print("Reading part/instance finished")
                    print("Summary:")
                    print("{0} nodes, {1} elements, {2} nodesets, {3} elementsets".format(
                            len(self.nodes), len(self.element), len(self.nsets), len(self.elsets)))
                    return True
                elif c[:4] in ["*MAT", "*BOU"]:
                    print("Reading assembly free structure finished")
                    print("Summary:")
                    print("{0} nodes, {1} elements, {2} nodesets, {3} elementsets".format(
                            len(self.nodes), len(self.element), len(self.nsets), len(self.elsets)))
                    return inpline
                elif c=="*NODE":
                    print("Reading nodes")
                    if "NSET" in comdict:
                        nset = comdict["NSET"]
                    else:
                        nset = ""
                    while True:
                        inpline = lineRead(inpfile, incfile)
                        if inpline=="":
                            return False
                        if (inpline[0]=="*"):
                            break
                        nodeNumber = self.addNodeLine(inpline)
                        if nset!="":
                            if nset in self.nsets:
                                self.nsets[nset].append(nodeNumber)
                            else:
                                self.nsets[nset] = [nodeNumber]
                    continue
                elif c=="*ELEMENT":
                    eltype = comdict["TYPE"]
                    print("Reading elements of type {0}".format(eltype))
                    if "ELSET" in comdict:
                        elset = comdict["ELSET"]
                    else:
                        elset = ""
                    while True:
                        inpline = lineRead(inpfile, incfile)
                        if inpline=="":
                            return False
                        if (inpline[0]=="*"):
                            break
                        elemNumber = self.addElementLine(inpline, eltype)
                        if elset!="":
                            if elset in self.elsets:
                                self.elsets[elset].append(elemNumber)
                            else:
                                self.elsets[elset] = [elemNumber]
                    continue
                elif c=="*NSET":
                    nset = comdict["NSET"]
                    generate = "GENERATE" in comdict
                    print("Reading node set {0}".format(nset))
                    while True:
                        inpline = lineRead(inpfile, incfile)
                        if inpline=="":
                            return False
                        if (inpline[0]=="*"):
                            break
                        self.addNodesetLine(inpline, nset, generate)
                    continue
                elif c=="*ELSET":
                    elset = comdict["ELSET"]
                    generate = "GENERATE" in comdict
                    print("Reading element set {0}".format(elset))
                    while True:
                        inpline = lineRead(inpfile, incfile)
                        if inpline=="":
                            return False
                        if (inpline[0]=="*"):
                            break
                        self.addElsetLine(inpline, elset, generate)
                    continue
                elif "SECTION" in c:
                    elset = comdict["ELSET"]
                    if elset not in self.elsets:
                        print("Could not find element numbers for section elset {0}".format(elset))
                        continue
                    element1 = self.elsets[elset][0]
                    self.sections[elset] = (self.element[element1].eltype,comdict["MATERIAL"])
            inpline = lineRead(inpfile, incfile)
            if inpline=="":
                return False


class Assembly(Part):
    '''#====================#
       #                    #
       #  ASSEMBLY CLASS    #
       #                    #
       #====================#
    '''

    def __init__(self, name):
        Part.__init__(self, name)
        self.instances = {}

    def __repr__(self):
        instlist = self.instances.keys()
        return "Instances: " + ",".join(instlist)

    def addInstance(self, name, part, translation, rotation):
        if name in self.instances:
            print("Instance {0} already exists".format(name))
            return False
        else:
            self.instances[name] = part
            t = np.array(translation)
            for inode in self.instances[name].nodes:
                self.instances[name].nodes[inode] += t
                #
                #-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                # Rotation not yet implemented
                #-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            return True

    def delInstance(self, name):
        if name in self.instances:
            del self.instances[name]
            return True
        else:
            print("Instance {0} does not exist".format(name))
            return False

    def addNodesetLine(self, line, nodeset, generate, instance="rootAssembly"):
        if nodeset not in self.nsets:
            self.nsets[nodeset] = Set(nodeset)
        itemlist = line.strip().split(",")
        if itemlist[-1]=="":
            del itemlist[-1]
        if generate:
            itemlist=[eval(x) for x in itemlist]
            if len(itemlist)==2:
                itemlist += [1]
            itemlist = range(itemlist[0], itemlist[1]+1, itemlist[2])
        if instance=="rootAssembly":
            for item in itemlist:
                try:
                    nodeNumber = int(item)
                    self.nsets[nodeset].addMember([nodeNumber], instance)
                except:
                    if (item.count(".")>0):
                        itempair = item.split(".")
                        inst = itempair[0]
                        if inst not in self.instances:
                            print("1: Could not evaluate entry {} for nodeset {}".format(
                                item, nodeset))
                        elif itempair[1] in self.instances[inst].nsets:
                            self.nsets[nodeset].addMember(
                                self.instances[inst].nsets[itempair[1]],
                                inst)
                        else:
                            try:
                                nodeNumber = int(itempair[1])
                                self.nsets[nodeset].addMember([nodeNumber], inst)
                            except:
                                print("2: Could not evaluate entry {} for assembly nodeset {}".format(
                                    item, nodeset))
                    else:
                        if item in self.nsets:
                            self.instances[inst].nsets[nodeset].addMember(self.nsets[item])
                        else:
                            print("3: Could not evaluate entry {} for assembly nodeset {}".format(
                                item, nodeset))
        else:
            if instance not in self.instances:
                return False
            for item in itemlist:
                try:
                    nodeNumber = int(item)
                    self.nsets[nodeset].addMember([nodeNumber], instance)
                except:
                    if item in self.nsets[nodeset].instances:
                        self.nsets[nodeset].addMember(
                            self.instances[instance].nsets[item].getMember(instance),
                            instance)
                    else:
                        print("4: Could not evaluate entry {0} for instanced nodeset {1}".format(
                            item, nodeset))
        return

    def addElsetLine(self, line, elset, generate, instance="rootAssembly"):
        if elset not in self.elsets:
            self.elsets[elset] = Set(elset)
        itemlist = line.strip().split(",")
        if itemlist[-1]=="":
            del itemlist[-1]
        if generate:
            itemlist = [eval(x) for x in itemlist]
            if len(itemlist)==2:
                itemlist += [1]
            itemlist = range(itemlist[0], itemlist[1]+1, itemlist[2])
        if instance=="rootAssembly":
            for item in itemlist:
                try:
                    elementNumber = int(item)
                    self.elsets[elset].addMember([elementNumber], instance)
                except:
                    if (item.count(".")>0):
                        itempair = item.split(".")
                        inst = itempair[0]
                        if inst not in self.instances:
                            print("1: Could not evaluate entry {0} for element set {1}".format(
                                item, elset))
                        elif itempair[1] in self.instances[itempair[0]].elsets:
                            self.elsets[elset].addMember(self.instances[inst].elsets[itempair[1]],
                                inst)
                        else:
                            try:
                                elementNumber = int(itempair[1])
                                self.elsets[elset].addMember([elementNumber], inst)
                            except:
                                print("2: Could not evaluate entry {0} for element set {1}".format(
                                    item, elset))
                    else:
                        if item in self.elsets:
                            self.elsets[elset] += self.elsets[item]
                        else:
                            print("3: Could not evaluate entry {0} for element set {1}".format(
                                item, elset))
        else:
            for item in itemlist:
                try:
                    elementNumber = int(item)
                    newMembers = [elementNumber]
                except:
                    if item in self.elsets:
                        newMembers = self.elsets[item].getMember(instance)
                    else:
                        print("4: Could not evaluate entry {0} for element set {1}".format(item, elset))
                        return
                #print(newMembers,instance,self.elsets[elset])
                self.elsets[elset].addMember(newMembers, instance)
        return

    def writeABQinput(self, incfilename):
        incfile=open(incfilename, "w")
        #
        #-----  first write empty parts (since instances are written with all information)
        userels = []
        for (iname, instance) in self.instances.items():
            incfile.write("*Part, name=P_{0}\n".format(iname))
            incfile.write("*End Part\n")
            for section in instance.sections.keys():
                (eltype, material) = instance.sections[section]
                if eltype[0]=="U":
                    if eltype not in userels:
                        userels.append(eltype)
                        element = instance.elsets[section][0]
                        elnodes = len(instance.element[element].nodes)
                        incfile.write("*User element, type={0}, nodes={1}".format(
                            eval(eltype[1:]), elnodes))
                        incfile.write(", coordinates=3, properties=0, iproperties=0\n")
                        incfile.write(" 1,2,3\n")
        #
        #-----  now start writing each instance separately
        incfile.write("*Assembly, name={0}\n".format(self.name))
        for inst in self.instances.keys():
            instance=self.instances[inst]
            incfile.write("*Instance, name={0}, part=P_{0}\n".format(inst))
            incfile.write("*Node\n")
            for (node,coords) in instance.nodes.items():
                coordString = "{0:8d}".format(node)
                for coo in coords:
                    coordString += ",{0:12.6g}".format(coo)
                incfile.write("{0}\n".format(coordString))
            for section in instance.sections.keys():
                (eltype,material) = instance.sections[section]
                incfile.write("*Element, elset={0}, type={1}\n".format(section, eltype))
                for el in instance.elsets[section]:
                    elList = [el] + instance.element[el].nodes
                    writeABQlist(elList,  incfile)
            for section in instance.sections.keys():
                (eltype, material) = instance.sections[section]
                if eltype[0]=="C":
                    incfile.write("*Solid Section, elset={0}, material={1}\n".format(
                        section, material))
                elif eltype[0]=="S":
                    incfile.write("*Shell Section, elset={0}, material={1}\n".format(
                        section, material))
                elif eltype[0]=="U":
                    incfile.write("*UEL property, elset={0}, material={1}\n".format(
                        section, material))
            for (nset, nodes) in instance.nsets.items():
                incfile.write("*Nset, nset={0}\n".format(nset))
                writeABQlist(nodes, incfile)
            for (elset, elements) in instance.elsets.items():
                if elset in instance.sections.keys():
                    continue
                incfile.write("*Elset, elset={0}\n".format(elset))
                writeABQlist(elements,incfile)
            incfile.write("*End Instance\n")
        #
        #-----  now write assembly entities and sets
        if len(self.nodes)>0:
            incfile.write("*Node\n")
            for (node, coords) in self.nodes.items():
                coordString = "{0:8d}".format(node)
                for coo in coords:
                    coordString += ",{0:12.6g}".format(coo)
                incfile.write("{0}\n".format(coordString))
        for elNumber in self.element.keys():
            eltype = self.element[elNumber].eltype
            incfile.write("*Element, type={0}\n".format(eltype))
            cstr = map(str, self.element[elNumber].nodes)
            elemString = "{0},".format(elNumber) + ",".join(cstr)
            incfile.write(elemString)
        for nname in self.nsets.keys():
            nset = self.nsets[nname]
            for i in range(len(nset.instances)):
                incfile.write("*Nset, nset={0}, instance={1}\n".format(
                    nset.name, nset.instances[i]))
                writeABQlist(nset.members[i], incfile)
        for ename in self.elsets.keys():
            elset = self.elsets[ename]
            for i in range(len(elset.instances)):
                incfile.write("*Elset, nset={0},instance={1}\n".format(
                    elset.name, elset.instances[i]))
                writeABQlist(elset.members[i], incfile)
        incfile.write("*End Assembly\n")
        incfile.close()

    def readFromFile(self, parts, inpfile, incfile):
        inpline = lineRead(inpfile, incfile)
        if inpline=="": 
            return False
        while True:
            if (inpline[0]=="*"):
                comdict = commandRead(inpline)
                c = comdict["command"]
                if c=="*END ASSEMBLY":
                    print("Reading assembly finished")
                    print("Summary:")
                    print("{} instances, {} nodes, {} elements, {} nodesets, {} elementsets".format(
                        len(self.instances), 
                        len(self.nodes),
                        len(self.element),
                        len(self.nsets),
                        len(self.elsets)))
                    return True
                elif c=="*NODE":
                    print("Reading nodes")
                elif c=="*ELEMENT":
                    print("Reading elements of type {}".format(comdict["TYPE"]))
                elif c=="*NSET":
                    nodeset = comdict["NSET"]
                    print("Reading node set {}".format(nodeset))
                    generate = "GENERATE" in comdict
                    while True:
                        inpline = lineRead(inpfile, incfile)
                        if inpline=="": 
                            return False
                        if (inpline[0]=="*"): 
                            break
                        if "INSTANCE" in comdict:
                            self.addNodesetLine(inpline, nodeset, generate, comdict["INSTANCE"])
                        else:
                            self.addNodesetLine(inpline, nodeset, generate)
                    continue
                elif c=="*ELSET":
                    elset = comdict["ELSET"]
                    generate = "GENERATE" in comdict
                    print("Reading element set {}".format(elset))
                    while True:
                        inpline = lineRead(inpfile, incfile)
                        if inpline=="": 
                            return False
                        if (inpline[0]=="*"): 
                            break
                        if "INSTANCE" in comdict:
                            self.addElsetLine(inpline, elset, generate, comdict["INSTANCE"])
                        else:
                            self.addElsetLine(inpline, elset, generate)
                    continue
                elif c=="*INSTANCE":
                    instancename = comdict["NAME"]
                    print("Reading instance {}".format(instancename))
                    # if there is a translation, read this
                    position = inpfile.tell()
                    inpline = lineRead(inpfile,incfile)
                    if inpline=="": 
                        break
                    if (inpline[0]=="*"):
                        inpfile.seek(position)
                        translation = [0.,0.,0.]
                        rotation = 7*[0.]
                    else:
                        translation=[eval(x) for x in inpline.split(",")]
                        while len(translation)<3:
                            translation += [0.]
                        # if there is a rotation, read this as well
                        position = inpfile.tell()
                        inpline = lineRead(inpfile, incfile)
                        if inpline=="": 
                            break
                        if (inpline[0]=="*"):
                            inpfile.seek(position)
                            rotation = 7*[0.]
                        else:
                            rotation=[eval(x) for x in inpline.split(",")]
                            while len(rotation)<7:
                                rotation += [0.]
                    partname = comdict["PART"]
                    if partname in parts:
                        self.addInstance(instancename,parts[partname],translation,rotation)
                    else:
                        print("Part {} could not be instanced".format(partname))
                        return False
                    inpline = lineRead(inpfile,incfile)
                    if not self.instances[instancename].readFromFile(inpfile, incfile, inpline):
                        return False
            inpline = lineRead(inpfile, incfile)
            if inpline=="":
                return False
        print("Assembly reading could not be finished properly")
        return False


#-----------------------------------------------------------------------
#  END OF CLASSES
#-----------------------------------------------------------------------
#-----------------------------------------------------------------------
#  BEGIN OF FUNCTION DEFINITIONS
#-----------------------------------------------------------------------
#
def writeABQlist(entities,incfile):
    valuesPerLine=12
    for i in range(1,len(entities)):
        incfile.write("{0:10}".format(entities[i-1]))
        if i%valuesPerLine==0:
            incfile.write("\n")
        else:
            incfile.write(",")
    incfile.write("{0:10}\n".format(entities[-1]))
    return

def lineRead(inpfile,writefile=False):
    while True:
        inpline = inpfile.readline()
        if writefile:
            writefile.write(inpline)
        if (inpline==""):
            break
        elif (inpline[:2]=="**"):
            continue
        elif (inpline.strip()==""):
            continue
        break
    return inpline

def commandRead(inpline):
    ilist = inpline.split(",")
    command = ilist.pop(0).strip().upper()
    comdict = {"command":command}
    # print(command)
    for iparam in ilist:
        iparpair = iparam.split("=")
        if len(iparpair)==2:
            comdict[iparpair[0].strip().upper()] = iparpair[1].strip()
        else:
            comdict[iparpair[0].strip().upper()] = None
    return comdict

def readABQinput(filename):
    inpfile = open(filename+".inp","r")
    newfile = open(filename+"_div.inp","w")
    incfile = open(filename+".inc","w")
    newfile.write("*INCLUDE,input="+filename+".inc\n")
    parts = {}
    steps = {}
    bconditions = []
    materials = []
    assembly = 0
    inpline = lineRead(inpfile, newfile)
    while True:
        if inpline=="":
            break
        if (inpline[0]=="*"):
            comdict = commandRead(inpline)
        else:
            inpline = lineRead(inpfile)
            continue
        c = comdict["command"]
        if c=="*PART":
            incfile.write(inpline)
            partname = comdict["NAME"]
            parts[partname]=Part(partname)
            print("Reading part {0}".format(partname))
            inpline = lineRead(inpfile, incfile)
            if not parts[partname].readFromFile(inpfile, incfile, inpline):
                break
            inpline = lineRead(inpfile)
        elif c=="*ASSEMBLY":
            incfile.write(inpline)
            assemblyname = comdict["NAME"]
            print("Reading assembly {0}".format(assemblyname))
            assembly=Assembly(assemblyname)
            if not assembly.readFromFile(parts, inpfile, incfile):
                break
            inpline = lineRead(inpfile)
        elif c=="*NODE":
            dummyPart="PART-1"
            assemblyname="Assembly"
            dummyInstance="PART-1-1"
            print("No assembly information available")
            print("Generating dummy part {0}".format(dummyPart))
            parts[dummyPart]=Part(dummyPart)
            modelread=parts[dummyPart].readFromFile(inpfile, incfile, inpline)
            if not modelread:
                break
            print("Generating dummy assembly")
            assembly=Assembly(assemblyname)
            translation = [0.,0.,0.]
            rotation = 7*[0.]
            assembly.addInstance(dummyInstance, parts[dummyPart], translation, rotation)
            inpline = modelread
        elif c[:6]=="*BOUND":
            newfile.write(inpline)
            while True:
                inpline = lineRead(inpfile,newfile)
                # weiß noch nicht, wie ich mit EOF umgehen soll
                # if inpline=="": break
                if (inpline[0]=="*"): 
                    break
                newbound=Boundary(len(bconditions))
                if newbound.addFromLine(inpline,assembly):
                    bconditions.append(newbound)
        elif c[:4]=="*MAT":
            newfile.write(inpline)
            materials.append(comdict["NAME"])
            inpline = lineRead(inpfile)
        elif c=="*STEP":
            stepname="STEP-1"
            if "NAME" in comdict:
                stepname = comdict["NAME"]
            steps[stepname] = Step(stepname)
            if "NLGEOM" in comdict:
                if comdict["NLGEOM"] in (None,"YES"):
                    steps[stepname].setNlgeom()
            if "INC" in comdict:
                steps[stepname].setIncs(comdict["INC"])
            steps[stepname].readFromFile(inpfile, newfile, assembly)
            inpline = lineRead(inpfile, newfile)
        else:
            print("Could not evaluate command {}".format(c))
            newfile.write(inpline)
            inpline = lineRead(inpfile)
    inpfile.close()
    incfile.close()
    newfile.close()
    return (assembly, parts, bconditions, materials, steps)
#
#-----------------------------------------------------------------------
#  END OF FUNCTION DEFINITIONS
#-----------------------------------------------------------------------
#-----------------------------------------------------------------------
#  BEGIN OF MAIN PROGRAM
#-----------------------------------------------------------------------
if __name__ == "__main__":
    parts = {}
    (assembly, parts, bconditions, materials, steps) = readABQinput("cubehex8")
    print("Reading finished for assembly {}".format(assembly.name))
    print("and {} part(s), {} model bcs, {} materials and {} step(s).".format(
           len(parts), len(bconditions), len(materials), len(steps)))
