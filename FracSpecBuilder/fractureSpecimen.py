# -*- coding: mbcs -*-
from part import *
from material import *
from section import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from job import *
from sketch import *
from visualization import *
from connectorBehavior import *
#
#######################################################
###  subroutine definition start
##########################################
def partition3D(myModel,myPart,cell,facedir,faceno,points,elLength):
    """ 
    myModel ist das Modell selbst
    myPart ist das zu partitionierende Part ( mdb.models[...].parts[...] )
    cell ist eine liste mit 2 3D-Koordinaten-Tupeln
    Die tats�chliche Zelle ergibt sich aus dem Object, welches ABAQUS mit findAt() findet...
    facedir ist ein string, der die Werte "x", "y" oder "z" haben kann,
    faceno ist ein integer, der 0 oder 1 sein kann (entspricht min und max der Fl�che)
    points ist eine Liste mit Koordinatentupeln (x,y) auf der Fl�che, die zusammen die Partitionierungslinie ergeben
    Die Richtung ist ebenfalls durch facedir gegeben.
    Die Elementkantenlaenge wird mit elLength definiert (fuer meshSeeds
    """
    #benoetigte Variablen
    direction = {"x":0, "y":1,"z":2}
    facedirid = direction[facedir.lower()]
    #
    dist = []
    cellpoint = cell[0][:]
    print cell
    for i in range(3):
        dist.append(cell[1][i]-cell[0][i])
        cellpoint[i] += 0.5*dist[i]
    #Herausfinden der Flaeche
    facepoint = cell[faceno][:]
    dir1 = (facedirid+1) % 3
    dir2 = (facedirid+2) % 3
    facepoint[dir1] = cell[0][dir1] + dist[dir1]*0.5
    facepoint[dir2] = cell[0][dir2] + dist[dir2]*0.5
    parFace = myPart.faces.findAt(facepoint, )
    print "Face to partition: ",parFace,facepoint
    #Herausfinden der Kante auf der rechten Seite senkrecht
    edgepoint = cell[faceno][:]
    edgepoint[dir2] = cell[0][dir2] + dist[dir2]*0.5
    parEdge = myPart.edges.findAt(edgepoint, )
    #
    #2D partitioning
    myModel.ConstrainedSketch(gridSpacing=1., name='partition', 
        sheetSize=100., transform=
        myPart.MakeSketchTransform(
        sketchPlane=parFace, 
        sketchPlaneSide=SIDE1, 
        sketchUpEdge=parEdge, 
        sketchOrientation=RIGHT, origin=(0.0, 0.0, 0.0)))
    myPart.projectReferencesOntoSketch(filter=
        COPLANAR_EDGES, sketch=myModel.sketches['partition'])
    #
    linecenter = []
    for i in range(1,len(points)):
        p1 = points[i-1]
        p2 = points[i]
        myModel.sketches['partition'].Line(point1=p1, point2=p2)
        #make 3D linepoints for later 3D partitioning
        linecenter.append(cell[faceno][:])
        for j in (range(2)):
            linecenter[-1][(j+facedirid+1) % 3] = 0.5*(p1[j]+p2[j])
    #
    myPart.PartitionFaceBySketch(faces=parFace, 
        sketch=myModel.sketches['partition'], 
        sketchUpEdge=parEdge)
    del myModel.sketches['partition']
    #
    # 3D partitioning
    parEdgeList = []
    for i in range(len(linecenter)):
        parEdgeList.append(myPart.edges.findAt(linecenter[i],))
        if (elLength>1.e-08):
            myPart.seedEdgeBySize(constraint=FIXED, edges=(parEdgeList[-1],), size=elLength)
    #
    if (elLength>1.e-08):
        p1 = points[0]
        p2 = points[-1]
        #make 3D linepoints for later 3D partitioning
        lineadd1=cell[faceno][:]
        lineadd2=cell[faceno][:]
        for j in (range(2)):
            lineadd1[(j+facedirid+1) % 3] = 0.99*p1[j]+0.01*p2[j]
            lineadd2[(j+facedirid+1) % 3] = 0.01*p1[j]+0.99*p2[j]
        myPart.seedEdgeBySize(constraint=FIXED, edges=(myPart.edges.findAt(lineadd1, ),), size=elLength)
        myPart.seedEdgeBySize(edges=(myPart.edges.findAt(lineadd2, ),), size=elLength)
#
    sweepDir=FORWARD
    if faceno==1: sweepDir=REVERSE
    myPart.PartitionCellByExtrudeEdge(
        cells=myPart.cells.findAt(cellpoint, ), 
        edges=parEdgeList,
        line=myPart.datums.values()[facedirid],
        sense=sweepDir )
    #~ myPart.PartitionCellBySweepEdge(
        #~ cells=myPart.cells.findAt(cellpoint, ), 
        #~ edges=parEdgeList,
        #~ sweepPath=myPart.edges.findAt((0.,0.,Thickness/2.), )  )
    return
#######################################################
###  subroutine definition end
##########################################

def FracSpecBuilder(height=112.5,width=75.,a0=25.,DAregion =20.,Thickness=2.075,
    ligadist1st =1.,elmin = 0.2,elmax = 4.0,elnumThick = 6,partitions = 2,elthickRatio = 2.):

    #Global Variables
    ligadist = [ligadist1st]
    meshsize = [elmin]
    for n in range(1,partitions):
        geomfac = pow((elmax/elmin),(float(n)/float(partitions)))
        meshsize.append(elmin*geomfac)
        ligadist.append(ligadist1st*geomfac)
    #
    czmheight = 0.02
    #
    myModel=mdb.models['Model-1']
    myModel.setValues(noPartsInputFile=ON)
    #
    #3D Model (simple Hexagon) generation
    myModel.ConstrainedSketch(name='specimen', sheetSize=200.0)
    myModel.sketches['specimen'].rectangle(point1=(0.,-czmheight), 
        point2=(width,height))
    myModel.Part(dimensionality=THREE_D, name='Part-1', type=
        DEFORMABLE_BODY)
    myPart=myModel.parts['Part-1']
    myPart.BaseSolidExtrude(depth=Thickness, sketch=myModel.sketches['specimen'])
    del myModel.sketches['specimen']
    #
    #Define datum axes
    myPart.DatumAxisByPrincipalAxis(principalAxis=XAXIS)  #datums.values()[0]
    myPart.DatumAxisByPrincipalAxis(principalAxis=YAXIS)  #datums.values()[1]
    myPart.DatumAxisByPrincipalAxis(principalAxis=ZAXIS)  #datums.values()[2]

    #Define global mesh size
    myPart.seedPart(deviationFactor=0.1, size=elmax)

    # ###################
    # #  PARTITIONING  ##
    # ###################
    #Partitioning for cohesive model
    cell = [[0.,-czmheight,0.],[width,height,Thickness]]
    facedir = "z"
    faceno = 1
    points = []
    points.append((0.0, 0.0))
    points.append((width, 0.0))
    partition3D(myModel,myPart,cell,facedir,faceno,points,-1.)
    #
    cell = [[0.,-czmheight,0.],[width,0.0,Thickness]]
    facedir = "z"
    faceno = 1
    points = []
    points.append((a0, -czmheight))
    points.append((a0, 0.0))
    partition3D(myModel,myPart,cell,facedir,faceno,points,-1.)
    #
    cell = [[a0,-czmheight,0.],[width,0.0,Thickness]]
    facedir = "z"
    faceno = 1
    points = []
    points.append((a0+DAregion, -czmheight))
    points.append((a0+DAregion, 0.0))
    partition3D(myModel,myPart,cell,facedir,faceno,points,-1.)
    #
    #Partitioning for the continuum
    for ipart in range(partitions):
        cell = [[0.,0.,0.],[width,height,Thickness]]
        facedir = "z"
        faceno = 1
        points = []
        points.append((a0-ligadist[ipart], 0.0))
        points.append((a0-ligadist[ipart], ligadist[ipart]))
        points.append((a0+DAregion+ligadist[ipart], ligadist[ipart]))
        points.append((a0+DAregion+ligadist[ipart], 0.0))
        partition3D(myModel,myPart,cell,facedir,faceno,points,meshsize[ipart])
    #
    #new names for the cells
    czmcell = myPart.cells.findAt((a0+DAregion/2.,-czmheight/2.,Thickness/2.), )
    #
    # material definition
    #Metal
    myModel.Material(name='Metal')
    myModel.materials['Metal'].Elastic(table=((10000.0, 0.3), ))
    myModel.HomogeneousSolidSection(material='Metal', name=
        'continuum', thickness=None)
    for cell in myPart.cells:
        if cell<> czmcell: 
            myPart.SectionAssignment(offset=0.0, 
                offsetField='', offsetType=MIDDLE_SURFACE, region=(cell, ), sectionName='continuum')
    #Cohesive Model
    mdb.models['Model-1'].Material(name='TSL')
    mdb.models['Model-1'].materials['TSL'].Elastic(table=((1000.0, 1000.0, 1000.0), 
        ), type=TRACTION)
    myModel.CohesiveSection(initialThickness=1.0, 
        initialThicknessType=SPECIFY, material='TSL', name='CZE', 
        outOfPlaneThickness=None, response=TRACTION_SEPARATION)
    myPart.SectionAssignment(offset=0.0, 
        offsetField='', offsetType=MIDDLE_SURFACE, region=(czmcell, ), sectionName='CZE')
    #
    #Assembly definition
    myModel.rootAssembly.DatumCsysByDefault(CARTESIAN)
    myModel.rootAssembly.Instance(dependent=ON, name='Part-1-1', part=myPart)
    #
    #Step definition
    myModel.StaticStep(initialInc=0.1, maxInc=0.1, maxNumInc=1000, 
        name='Step-1', nlgeom=ON, previous='Initial')
    #
    # BC definition
    #
    # Mesh controls for cohesive model
    myPart.setMeshControls(algorithm=ADVANCING_FRONT, regions=(czmcell, ), technique=SWEEP)
    myPart.setSweepPath(edge=
        myPart.edges.findAt((a0,-czmheight/2.,Thickness), ), region=czmcell, sense=REVERSE)
    myPart.setElementType(elemTypes=(
        ElemType(elemCode=COH3D8, elemLibrary=STANDARD), 
        ElemType(elemCode=COH3D6, elemLibrary=STANDARD), 
        ElemType(elemCode=UNKNOWN_TET, elemLibrary=STANDARD)), 
        regions=(czmcell, ) )
    myPart.seedEdgeBySize(constraint=FIXED,edges=(myPart.edges.findAt((a0+DAregion/2.,0.0,Thickness),),), size=elmin)
    # seeds in thickness direction
    myPart.seedEdgeByBias(constraint=FIXED,end2Edges=(myPart.edges.findAt((a0,0.0,Thickness/2.),), ),
        number=elnumThick, ratio=elthickRatio)
    myPart.seedEdgeByBias(constraint=FIXED,end2Edges=(myPart.edges.findAt((a0-ligadist[0],ligadist[0],Thickness/2.),), ),
        number=elnumThick, ratio=elthickRatio)
    # meshing
    myPart.generateMesh()
    myPart.deleteMesh(regions=(myPart.cells.findAt((0.1,-czmheight/2.,Thickness/2.),), ))
    myPart.deleteMesh(regions=(myPart.cells.findAt((width-0.1,-czmheight/2.,Thickness/2.),), ))
    return