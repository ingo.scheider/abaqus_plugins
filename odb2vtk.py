'''
@author: Ingo Scheider, Qingbin Liu
E-mail ingo.scheider@hereon.de, liuqingb@mail2.sysu.edu.cn; liuqingb@pku.edu.cn
--------------------------------------------------------------------------------
The script is to convert the data from Abaqus output database format
to vtk file format for parallel visualization
Python script performs the following three major steps:
    1) reading Abaqus output file according to the architecture of ODB;
    2) data decomposition for parallel visualization (based on instances)
    3) writing VTK for Paraview.
'''

#import necessary modules to handle Abaqus output database, files and string
from odbAccess import *
from abaqusConstants import *
from os import path
from os import mkdir
import xml.etree.ElementTree as ET
import time
import numpy as np


# elementTypes is a dictionary, which contains the information or each ABAQUS element type as a tuple.
# The components of the tuple are
# - VTK element type
# - number of nodes
#
elementTypes = {"CPE3": (5,3),
                "CPS3": (5,3),
                "CAX3": (5,3),
                "R3D3": (5,3),
                "CPE4": (9,4),
                "CPS4": (9,4),
                "CAX4": (9,4),
                "R3D4": (9,4),
                "COH2D4": (9,4),
                "CPE6": (22,6),
                "CPS6": (22,6),
                "CAX6": (22,6),
                "CPE8": (23,8),
                "CPS8": (23,8),
                "CAX8": (23,8),
                "C3D4": (10,4),
                "C3D6": (13,6),
                "C3D8": (12,8),
                "C3D10": (24,10),
                "C3D15": (26,15),
                "C3D20": (25,20),
                "COH3D8": (12,8)
                }
tensorIndex = [0, 3, 4, 3, 1, 5, 4, 5, 2]


#Function
def checkFile(filename, suffix):
    if path.isfile(filename):
        return filename
    if suffix[0]==".":
    	suffix = suffix[1:]
    filename += "." + suffix
    if path.isfile(filename):
        return filename
    else:
        return False


def intListFromString(s, maxvalue=1):
    '''
    check input s:
        s==int: return [s]
        s==list: evaluate integer values, return list(int)
        s==string:
            if s contains ":":
                assume generator of type start:end:increment, create a range list
                start,end,incr = s.split(":")
                if end is not given: end = maxvalue
                if incr is not given: incr = 1
                if start is not gven: start = 0
            else:
                split(",")
                try: 
                   create a list(int)
    '''
    if type(s)==int:
        outlist = [s]
    elif type(s)==list:
        try:
            outlist = [int(ele.strip()) for ele in s]
        except:
            return False
    elif type(s)==str:
        if ":" in s:
            l = s.split(":")
            start = 0 if l[0].strip()=="" else int(l[0])
            end = maxvalue if l[1].strip()=="" else int(l[1])
            incr = int(l[2]) if len(l)==3 else 1
            outlist = range(start,end,incr)
        else:
            l = s.replace(","," ").split()
            try:
                outlist = [int(ele) for ele in l]
            except:
                return range(maxvalue)
    return outlist


def getParameterFromXml(xmlfile):
    tree = ET.parse(xmlfile)
    parameter = tree.getroot()
    inputParDict = {}
    #
    # odb
    currentTag=parameter.find("odb")
    if currentTag is not None:
        inputParDict["odb"] = currentTag.text
    #
    # vtk
    currentTag = parameter.find("vtk")
    if currentTag is not None:
        inputParDict["vtk"] = currentTag.text
    #
    # projectpath
    currentTag = parameter.find("projectpath")
    if currentTag is not None:
        inputParDict["projectpath"] = currentTag.text
    #
    # includeInstance
    currentTag = parameter.find("includeInstance")
    if currentTag is not None:
        if parameter.find("excludeInstance") is not None:
            print "You may only have either includeInstance or excludeInstance in the control file"
            exit(2)
        instancesTarget = currentTag.text.replace(",", " ")
        if len(instancesTarget)>0:
            inputParDict["includeInstance"] = [i.upper() for i in instancesTarget.split()]
    #
    # excludeInstance
    currentTag = parameter.find("excludeInstance")
    if currentTag is not None:
        instancesRemove = currentTag.text.replace(",", " ")
        if len(instancesTarget)>0:
            inputParDict["excludeInstance"] = [i.upper() for i in instancesRemove.split()]
    #
    # includeVar
    currentTag = parameter.find("includeVar")
    if currentTag is not None:
        varTarget = currentTag.text.split(",")
        if len(varTarget)>0:
            inputParDict["includeVar"] = [i.strip().upper() for i in varTarget]
    #
    # excludeVar
    currentTag = parameter.find("excludeVar")
    if currentTag is not None:
        varTarget = currentTag.text.split(",")
        if len(varTarget)>0:
            inputParDict["excludeVar"] = [i.strip().upper() for i in varTarget]
    #
    # step
    currentTag = parameter.find("step")
    if currentTag is not None:
        newstep = int(currentTag.text)
        if "step" in inputParDict:
            inputParDict["includeStep"].append(newstep)
        else:
            inputParDict["includeStep"] = [newstep]
    #
    # frame
    currentTag = parameter.find("frame")
    if currentTag is not None:
        if "step" not in currentTag.attrib:
            print "You have to provide the step number for which the frame defintion is valid"
            exit(2)
        step = int(currentTag.attrib["step"])
        if "includeFrame" in inputParDict:
            inputParDict["includeFrame"][step] = currentTag.text
        else:
            inputParDict["includeFrame"] = {step: currentTag.text}
    #
    return inputParDict

def checkVariableIncluded(var, inputParDict):
    if "includeVar" in inputParDict:
        for includes in inputParDict["includeVar"]:
            asterisk = includes.find("*")
            if asterisk==0:
                varlength = len(includes[1:])
                if includes[1:]==var[len(var)-varlength:]:
                    return True
            elif asterisk>0 and includes[:asterisk]==var[:asterisk]:
                return True
            elif includes==var:
                return True
        return False
    if "excludeVar" in inputParDict:
        for excludes in inputParDict["excludeVar"]:
            asterisk = excludes.find("*")
            if asterisk==0:
                varlength = len(excludes[1:])
                if excludes[1:]==var[len(var)-varlength:]:
                    return False
            if asterisk>0 and excludes[:asterisk]==var[:asterisk]:
                return False
            elif excludes==var:
                return False
    return True


def ConvertOdb2Vtk(filename = 'test.xml'):
    starttime = time.time()
    '''
    Part 1
    Interpretation of xml control file
    '''
    inputname = checkFile(filename, "xml")
    if not inputname:
        print 'Parameter file %s not found' % filename
        exit(2)
    inputParDict = getParameterFromXml(inputname)
    #get odb file name
    if "odb" in inputParDict:
        odbname = inputParDict["odb"]
    else:
        print "You have to provide an odb filename"
        exit(2)
    #read odb2vtk file to get parameter setting
    if "projectpath" in inputParDict:
        odb_path = inputParDict["projectpath"]
    else:
        odb_path = path.abspath(".")
    #get the output files' path (optional)
    if "vtk" in inputParDict:
        vtk_path = inputParDict["vtk"]
    else:
        vtk_path = odbname + "_vtk"
    #get the mesh type
    #get the quantity of pieces to partition
    if "partitions" in inputParDict:
        piecenum = inputParDict["partitions"]
    else:
        piecenum = 1
    #display the reading result of odb2vtk file
    print "control file reading finished, time elapsed: ", time.time()-starttime

    #open an ODB ( Abaqus output database )
    odbFullname = checkFile(path.join(odb_path,odbname),"odb")
    if not odbFullname:
        print "odb file " + odbname + " does not exist!"
        exit(2)
    odb = openOdb(odbFullname, readOnly=True)
    vtkFullpath = path.join(odb_path, vtk_path)
    print "ODB %s opened" % odbFullname
    if not path.exists(vtkFullpath):
        mkdir(vtkFullpath)
    print "Writing vtk files to ", vtkFullpath

    # steps to be included
    stepInOdb = np.arange(len(odb.steps), dtype=int)
    framesInStep = {}
    if "includeStep" in inputParDict:
        stepTarget = inputParDict["includeStep"]
        if max(stepTarget)>=len(odb.steps):
            print "Requested step larger than step in odb"
            exit(2)
    else:
        stepTarget = stepInOdb
    for stepNumber in stepTarget:
        odbstep = odb.steps.values()[stepNumber]
        framesInStep[stepNumber] = range(len(odbstep.frames))

    # frames
    frameTarget = {}
    if "includeFrame" in inputParDict:
        frameInput = inputParDict["includeFrame"]
        if type(frameInput)==dict:
            if np.any([s not in stepTarget for s in frameInput.keys()]):
                print "You have non-existing steps in your frame dictionary"
                exit(2)
            for stepNumber in stepTarget:
                if stepNumber in frameInput:
                    frameTarget[stepNumber] = intListFromString(frameInput[stepNumber], len(framesInStep[stepNumber]))
                else:
                    frameTarget[stepNumber] = framesInStep[stepNumber]
        elif type(frameInput)==list:
            if len(frameInput)!=len(stepTarget):
                print "include Frame entry as a list must correspond to required steps"
                exit(2)
            for i,stepNumber in stepTarget:
                frameTarget[stepNumber] = intListFromString(frameInput[i], framesInStep[stepNumber][-1])
        else:
            print "At this time frames can only be selcted as dictionary of step numbers"
    else:
        frameTarget = framesInStep
    '''
    Part 2
    Reading abaqus assembly data
    '''
    # instances to be included
    asm = odb.rootAssembly
    instancesInOdb = asm.instances.keys()
    if "includeInstance" in inputParDict:
        instancesTarget = inputParDict["includeInstance"]
        instanceFlag = [inst not in instancesInOdb for inst in instancesTarget]
        if np.any(instanceFlag):
            print "One of the given instances is not in the odb instances"
    else:
        instancesTarget = instancesInOdb
    if "excludeInstance" in inputParDict:
        excludedList = inputParDict["excludeInstance"].replace(",", " ").split()
        for excluded in excludedList:
            intancesTarget.remove(excluded.strip())
    piecenum = len(instancesTarget)

    # Read the ODB element and node data
    coordinates = []
    nodeVTKnumber = {}
    elements = []
    elementVTKnumber = {}
    elementtype = []
    nnode = 0
    nelement = 0
    instanceNodes = {}
    instanceElements = {}
    instanceNodeOffset = {}
    instanceElementOffset = {}
    for instance in instancesTarget:
        instanceNodes[instance] = []
        instanceElements[instance] = []
        instanceNodeOffset[instance] = nnode
        instanceElementOffset[instance] = nelement
        for node in asm.instances[instance].nodes:
            coordinates.append(node.coordinates)
            nodeVTKnumber[node.label + instanceNodeOffset[instance]] = nnode
            instanceNodes[instance].append(nnode)
            nnode += 1
        for element in asm.instances[instance].elements:
            connectivity = [nodeVTKnumber[node + instanceNodeOffset[instance]] for node in element.connectivity]
            elements.append(connectivity)
            eltype = element.type
            while eltype[-1] in ["R","H","I","T","M","V","P"]:
                eltype = eltype[:-1]
            elementtype.append(eltype)
            elementVTKnumber[element.label + instanceElementOffset[instance]] = nelement
            instanceElements[instance].append(nelement)
            nelement += 1
    with open(odbFullname[:-4]+".map","w") as mapfile:
        mapfile.write("Nodes map\n")
        for odbNod, vtkNod in nodeVTKnumber.items():
            mapfile.write("%d %d %s\n" % (odbNod, vtkNod, instance))
        mapfile.write("Element map\n")
        for odbEl, vtkEl in elementVTKnumber.items():
            mapfile.write("%d %d %s\n" % (odbEl, vtkEl, instance))
    print "Basic Information:"
    print "Model:",odbname
    print "Instances/blocks:", ",".join(instancesTarget)
    print "%d nodes, %d elements" % (nnode, nelement)
    for step in stepTarget:
        print "Step %d: Frames: %s" % (step + 1, ",".join([repr(s) for s in frameTarget[step]]))
    time1 = time.time()
    print "Time for reading model data: %.2f s" % (time1 - starttime)
    '''
    Part 3
    Reading results data
    '''
    #step cycle
    #
    for stepNumber in stepTarget:
        step = odb.steps.values()[stepNumber]
        maxframe = max(frameTarget[stepNumber])
        frameformat = "%0" + repr(int(np.log10(maxframe))) + "d"
        #
        # frame cycle
        #
        for frameNumber in frameTarget[stepNumber]:
            frame = step.frames[frameNumber]
            #
            # initialize outputdict
            outputTarget = []
            outputdict = {}
            descriptiondict = {}
            for var, output in frame.fieldOutputs.items():
                if not checkVariableIncluded(var, inputParDict):
                    continue
                outputTarget.append(var)
                vartype = output.type
                if vartype==TENSOR_3D_FULL:
                    outputdict[var] = np.zeros((nnode,6))
                    for scalar in output.validInvariants:
                        var_inv = var + "_" + scalar.getText()
                        if not checkVariableIncluded(var_inv, inputParDict):
                            continue
                        outputdict[var_inv] = np.zeros((nnode,1))
                elif vartype==VECTOR:
                    ncomponents = len(output.componentLabels)
                    outputdict[var] = np.zeros((nnode,ncomponents))
                elif vartype==SCALAR:
                    outputdict[var] = np.zeros((nnode,1))
            print "Evaluating the following variables in frame %d: %s" % (
                   frameNumber, ",".join(outputdict.keys()))
            # Access an output variable (name: var, repo: output)
            for instance in instancesTarget:
                time1 = time.time()
                nodeOffset = instanceNodeOffset[instance]
                for var in outputTarget:
                    output = frame.fieldOutputs[var].getSubset(region=asm.instances[instance])
                    try:
                        position = output.locations[0].position
                    except:
                        continue
                    description = output.name.replace(".","_").replace(" ","")
                    descriptiondict[var] = description
                    vartype = output.type
                    # check for integration point data:
                    # must be transferred to nodes, and
                    # averaged over all elements connected to this node
                    if position==INTEGRATION_POINT:
                        nodeoutput = output.getSubset(position=ELEMENT_NODAL)
                        nentries = np.zeros(nnode)
                        for entry in nodeoutput.values:
                            nodeLabel = entry.nodeLabel + nodeOffset
                            if nodeLabel not in nodeVTKnumber:
                                continue
                            node = nodeVTKnumber[nodeLabel]
                            outputdict[var][node] += entry.data
                            nentries[node] += 1
                        for nd in instanceNodes[instance]:
                            if nentries[nd] != 0:
                                outputdict[var][nd] /= nentries[nd]
                        # now write the invariants, etc., of tensor values
                        if vartype==TENSOR_3D_FULL:
                            for scalar in nodeoutput.validInvariants:
                                var_inv = var + "_" + scalar.getText()
                                if var_inv not in outputdict:
                                    continue
                                scalaroutput = nodeoutput.getScalarField(scalar)
                                description = scalaroutput.name.replace(".","_").replace(" ","")
                                descriptiondict[var_inv] = description
                                nentries = np.zeros(nnode)
                                for entry in scalaroutput.values:
                                    nodeLabel = entry.nodeLabel + nodeOffset
                                    if nodeLabel not in nodeVTKnumber:
                                        continue
                                    node = nodeVTKnumber[nodeLabel]
                                    outputdict[var_inv][node] += entry.data
                                    nentries[node] += 1
                                for nd in instanceNodes[instance]:
                                    if nentries[nd] != 0:
                                        outputdict[var_inv][nd] /= nentries[nd]
                    elif position==NODAL:
                        # nodal data is much easier ...
                        for entry in output.values:
                            nodeLabel = entry.nodeLabel + nodeOffset
                            if nodeLabel not in nodeVTKnumber:
                                continue
                            node = nodeVTKnumber[nodeLabel]
                            outputdict[var][node] = entry.data

                print "Time for evaluating instance %s: %.2f s" % (instance, time.time() - time1)
                time1 = time.time()
            # if "U" in outputdict:
            #     position = coordinates + outputdict['U']
            # else:
            '''
            Part 4
            Writing results data (per frame)
            '''
            # piece cycle, to partion the model and create each piece for vtk files
            vtufilenames = []
            for instance in instancesTarget:

                #create and open a VTK(.vtu) files
                if (piecenum > 1):
                    vtufilename = ("%s_%s_%d_" + frameformat + ".vtu") % (odbname,
                                                                          instance,
                                                                          stepNumber + 1,
                                                                          frameNumber)
                    vtufilenames.append(vtufilename)
                else:
                    vtufilename = ("%s_%d_" + frameformat + ".vtu") % (odbname,
                                                                      stepNumber + 1,
                                                                      frameNumber)
                print "Writing vtk file " + vtufilename
                vtufilename = path.join(vtkFullpath, vtufilename)
                outfile = open(vtufilename,"w")

                #<VTKFile>, including the type of mesh, version, and byte_order
                outfile.write('<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n')
                #<UnstructuredGrid>
                outfile.write('<UnstructuredGrid>\n')
                #<Piece>, including the number of points and cells
                nnd = len(instanceNodes[instance])
                nel = len(instanceElements[instance])
                outfile.write('<Piece NumberOfPoints="%d" NumberOfCells="%d">\n' % (nnd, nel))

                #<Points> Write nodes into vtk files
                outfile.write('<Points>\n')
                outfile.write('<DataArray type="Float64" NumberOfComponents="3" format="ascii">'+'\n')
                for node in instanceNodes[instance]:
                    x,y,z = coordinates[node]
                    outfile.write(' %11.8e  %11.8e  %11.8e\n' % (x, y, z))
                outfile.write('</DataArray>\n')
                outfile.write('</Points>\n')
                #</Points>

                outfile.write('<PointData>\n')

                #<DataArray> Write results into vtk files
                for varname, vardata in outputdict.items():
                    outstring = '<DataArray type="Float64" Name="%s" ' % descriptiondict[varname]
                    outstring += 'NumberOfComponents="%d" format="ascii">\n' % len(vardata[0])
                    outfile.write(outstring)
                    for node in instanceNodes[instance]:
                        outstring = ""
                        for v in vardata[node]:
                            outstring += " %11.8e" % v
                        outfile.write(outstring + '\n')
                    outfile.write("</DataArray>"+'\n')
                #</DataArray>

                outfile.write("</PointData>"+'\n')
                #</PointData>

                #<Cells> Write cells into vtk files
                outfile.write('<Cells>\n')
                #Connectivity
                nodeOffset = instanceNodeOffset[instance]
                outfile.write('<DataArray type="Int32" Name="connectivity" format="ascii">\n')
                for element in instanceElements[instance]:
                    outstring = " ".join([repr(nd - nodeOffset) for nd in elements[element]])
                    outfile.write('%s\n' % outstring)
                outfile.write('</DataArray>\n')
                #Offsets
                outfile.write('<DataArray type="Int32" Name="offsets" format="ascii">\n')
                offset = 0
                for element in instanceElements[instance]:
                    nodenumbers = elementTypes[elementtype[element]][1]
                    outfile.write('%d\n' % (offset+nodenumbers,))
                    offset += nodenumbers
                outfile.write('</DataArray>\n')
                #Type
                outfile.write('<DataArray type="UInt8" Name="types" format="ascii">\n')
                for element in instanceElements[instance]:
                    outfile.write('%d\n' % elementTypes[elementtype[element]][0])
                outfile.write('</DataArray>\n')
                outfile.write('</Cells>\n')
                #</Cells>

                #</Piece>
                outfile.write('</Piece>\n')
                #</UnstructuredGrid>
                outfile.write('</UnstructuredGrid>\n')
                #</VTKFile>
                outfile.write('</VTKFile>\n')

                outfile.close()

                '''====================================================================='''
            if ( piecenum > 1 ):
                #create .pvtu files for parallel visualization
                pvtufilename = ("%s_%d_" + frameformat + ".pvtu") % (odbname,
                                                                     stepNumber + 1,
                                                                     frameNumber)
                print "Writing pvtu file  %s for frame %d" % (pvtufilename, frameNumber)
                pvtufilename = path.join(vtkFullpath, pvtufilename)
                outfile = open(pvtufilename,'w')

                #write the basic information for .pvtu files
                outfile.write('<?xml version="1.0"?>\n')
                outfile.write('<VTKFile type="PUnstructuredGrid" version="0.1" byte_order="LittleEndian">\n')
                outfile.write('<PUnstructuredGrid GhostLevel="' + str(piecenum) + '">\n')
                #pointdata
                outfile.write('<PPointData>\n')
                for varname, vardata in outputdict.items():
                    outstring = '<PDataArray type="Float64" Name="%s" ' % descriptiondict[varname]
                    outstring += 'NumberOfComponents="%d" format="ascii"/>\n' % len(vardata[0])
                    outfile.write(outstring)
                outfile.write('</PPointData>\n')

                #points
                outfile.write('<PPoints>\n')
                outfile.write('<PDataArray type="Float64" NumberOfComponents="3"/>\n')
                outfile.write('</PPoints>\n')

                #write the path of each piece for reading it through the .pvtu file
                for vtufilename in vtufilenames:
                    outfile.write('<Piece Source="%s"/>\n' % (vtufilename,))
                outfile.write("</PUnstructuredGrid>"+'\n')
                outfile.write("</VTKFile>")
                outfile.close()
            print "Time for writing: %.2f s" %  (time.time() - time1, )

    odb.close()

    print "Total time elapsed: ", time.time() - starttime, "s"
