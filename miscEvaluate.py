try:
    from odbAccess import *
except ImportError:
    print("This module only makes sense within abaqus python\nExiting ...")
    exit()
from sys import argv
from os import path
import sympy
#
from convert2vtkCls import *

replayfile = "abaqus_myEval.rpy"
replay = open(replayfile,"w")
replay.write("from myEvalCls import history\n#\n")

class curve(object):
    '''class for defining methods and attributes of curves

    !!!!!!!!!!!!!!!!!!!!
    !!! not used yet !!!
    !!!!!!!!!!!!!!!!!!!!

    methods
    ---------
    mark()
    unmark()
    derive()   - calculating the numerical derivative of curve, adding to curvelist
    interpolate(numbers[, minval][, maxval])
                    - interpolate to numbers values
    normalizeX([min, max]), normalizeY([min, max]), normalizeXY()
                    - normalize y-values (or x-values, for normalizeX) between 0 and 1 if no min/max values are given
    plot(curve[,plotfilename])
                    - plot a single curve
                      and write the graph to plotfilename if given

    attributes
    ----------
    x(list)
    y(list)
    origin(list)
    label(string)
    ismarked(bool)
    '''
    def __init__(self,x,y,origin):
        self.x = x
        self.y = y
        self.origin = origin

class history(object):
    '''Primary module for some easy evaluation functions
    Can be called with an odbname as parameter to automatically read all history curves

    methods:
    --------
    getFromOdb(odbname[,steps])
                    - returns a list with all history curves in the odb (for specified list of stepnumbers if given)

    x(curve)        - returns the list of x-values of the given curvenumber
    y(curve)        - returns the list of y-values of the given curvenumber
    combine(curve1,curve2)
                    - combines the y-values of two curves to a new curve, adding to curvelist
    join(curve1,curve2[,add2time])
                    - joins to curves (e.g. for different time steps),
                      adding the last x-value in curve1 to the x-value in curve 2 (if not add2time=False)
    evaluate(xExpressionString, yExpressionString)
                    - calculate new curve from existing curves, adding to curvelist
                      The expression may contain any existing curve (x and y values) by
                      $<curvenumber>x and $<curvenumber>y
                      Example: c.evaluate("-$0x-$1x", "exp(-1.5*($2x+$3y))")
    derive(curve)   - calculating the numerical derivative of curve, adding to curvelist
    interpolate(curve, numbers[, minval][, maxval])
                    - interpolate to numbers values
    normalizeX(curve, [min, max]), normalizeY(curve, [min, max]), normalizeXY(curve)
                    - normalize y-values (or x-values, for normalizeX) between 0 and 1 if no min/max values are given

    # Working with curve selections:
    mark(*curves), markonly(*curves), markbylabel(string), markall(), marknone(), unmark(*curves)
                    - mark or unmark specified curves (for deletion, plotting or writing)
    write([header])
                    - writes the marked curves to file (by default to last odbname opened)
                      An additional header line may be given, which is written above all other header lines
    write1x([header])
                    - writes the y values of the marked curves to file (by default to last odbname opened),
                      with a single x value in the first column
                      An additional header line may be given, which is written above all other header lines
    delete()        - delete the marked curves

    attributes
    ----------
    colseparator (string)
                    - the separator for the textfile to be written
    silent(bool)    - whether to print messages to screen or not
    rptfilename (string)
                    - filename for report file (by default current odb filename)
    '''

    def __init__(self, odb=None, steps=[]):
        replay.write("c = history()\n")
        self.curvelist = []
        self.marked = []
        self.silent = False
        self.currentname = ""
        self.__rptfilename = ""
        self.colseparator = ","
        if odb!=None:
            openOK = self.getFromOdb(odb, steps)

    def __repr__(self):
        allcurvestring = ""
        for cnumber in range(len(self.curvelist)):
            allcurvestring += "* " if self.marked[cnumber] else "  "
            allcurvestring += self.showcurve(cnumber)+"\n"
        return allcurvestring

    def __len__(self):
       return len(self.curvelist)

    def getFromOdb(self,odbname,steps=[]):
        replay.write("c.getFromOdb(\"%s\", steps=%s)\n" % (odbname,repr(steps)))
        if self.currentname==self.__rptfilename:
            self.__rptfilename = odbname
        self.currentname = odbname
        try:
            odb=openOdb(odbname+".odb", readOnly=True)
        except OdbError:
            print("Could not open odb "+odbname+".odb")
            return False
        if steps==[]:
            steps = range(len(odb.steps))
        elif type(steps)==int:
            steps = [steps]
        stepnames = [odb.steps.keys()[istep] for istep in steps]

        for stepname in stepnames:
            for hregionName,hregion in odb.steps[stepname].historyRegions.items():
                houtput = hregion.historyOutputs

                for quant in houtput.keys():
                    curvelabel = [odbname,stepname,hregionName,quant]
                    hdata = houtput[quant].data
                    if hdata==None:
                        continue
                    x = [val[0] for val in hdata]
                    y = [val[1] for val in hdata]
                    self.curvelist.append((x, y, curvelabel))
                    self.marked.append(False)
                    if not self.silent:
                        print(self.showcurve(-1))
        return True

    def getrange(self,cnumber):
        curve = self.curvelist[cnumber]
        xrange = [min(curve[0]),max(curve[0])]
        yrange = [min(curve[1]),max(curve[1])]
        return xrange,yrange


    def x(self,cnumber):
        return self.curvelist[cnumber][0]


    def y(self,cnumber):
        return self.curvelist[cnumber][1]


    def showcurve(self,cnumber):
        if cnumber==-1:
            cnumber = len(self.curvelist) - 1
        elif cnumber>=len(self.curvelist):
            raise RuntimeError("Cannot show curve Nr. %d" % cnumber)
        xr,yr = self.getrange(cnumber)
        curve = self.curvelist[cnumber]
        label = "|".join(curve[2])
        return "%d: %d points, range: [%f,%f],[%f,%f], name %s" % (cnumber,len(curve[0]),xr[0],xr[1],yr[0],yr[1],label.strip("|"))

    def mark(self,*cnumber):
        replay.write("c.mark(%s)\n" % ",".join(map(repr,cnumber)))
        for c in cnumber:
            self.marked[c] = True

    def markbylabel(self,markstring):
        replay.write("c.markbylabel(\"%s\")\n" % markstring)
        for c in range(len(self.curvelist)):
            if markstring in self.showcurve(c):
                self.marked[c] = True
        if not self.silent:
            print repr(self)

    def markall(self):
        replay.write("c.markall()\n")
        for c in range(len(self.curvelist)):
            self.mark(c)

    def markonly(self,*cnumber):
        replay.write("c.markonly(%s)\n" % ",".join(map(repr,cnumber)))
        self.marknone()
        for c in cnumber:
            self.marked[c] = True

    def unmark(self,*cnumber):
        replay.write("c.unmark(%s)\n" % ",".join(map(repr,cnumber)))
        for c in cnumber:
            self.marked[c] = False

    def marknone(self):
        replay.write("# c.marknone()\n")
        self.unmark(*range(len(self.curvelist)))

    def delete(self):
        replay.write("c.delete()\n")
        ci = 0
        while ci<len(self.marked):
            if not self.marked[ci]:
                ci += 1
                continue
            self.__deletesingle(ci)

    def __deletesingle(self,curve):
        c = self.curvelist.pop(curve)
        c = self.marked.pop(curve)

    def join(self,c1number,c2number,addTime=True):
        replay.write("c.join({}, {}, addTime={})\n".format(c1number, c2number, addTime))
        x1,y1,leg1 = self.curvelist[c1number]
        x2,y2,leg2 = self.curvelist[c2number]
        if addTime:
            x1last = x1[-1]
            if x2[0]==x1last:
                dummy = x2.pop(0)
                dummy = y2.pop(0)
            for i in range(len(x2)):
                x2[i] += x1last
        x = x1 + x2
        y = y1 + y2
        legend = []
        for l1,l2 in zip(leg1,leg2):
            if l1!=l2:
                legend.append(l1+"+"+l2)
            else:
                legend.append(l1)
        self.curvelist.append((x,y,legend))
        self.marked.append(False)
        if not self.silent:
            print(self.showcurve(-1))

    def evaluate(self,XexprString,YexprString):
        replay.write("c.evaluate(\"%s\", \"%s\")\n" % (XexprString, YexprString))
        '''
        the curve numbers treated in exprString are stored in the list <curvenumbers>
        Curve numbers stored in curvenumbers[i] higher then the toal number of curves denote y-values of the curve
        The curve symbols in exprString are stored in the dictionary <symboldict>
        with the keys being indices of curvenumbers.
        The expressions for x and v values are treated separately
        '''
        totalcnumber = len(self.curvelist)

        def evalParser(exprString):
            #
            # parser
            #
            symboldict = {}
            sympystring = ""
            indx1 = 0
            indx = exprString.find("$",indx1)
            curvenumbers = []
            while indx>=0:
                sympystring += exprString[indx1:indx]
                indx2 = indx + 1
                while exprString[indx2].isdigit():
                    indx2 += 1
                try:
                    cnumber = eval(exprString[indx+1:indx2])
                except:
                    raise RuntimeError("Could not evaluate: <%s< at position %d" % (exprString,indx))
                if exprString[indx2].upper()=="X":
                    pass
                elif exprString[indx2].upper()=="Y":
                    cnumber += totalcnumber
                else:
                    raise RuntimeError("Could not evaluate: "+exprString)
                newCurve = cnumber not in curvenumbers
                if newCurve:
                    curvenumbers.append(cnumber)
                cindex = curvenumbers.index(cnumber)
                curvevar = "c"+repr(cindex)
                sympystring += curvevar
                if newCurve:
                    symboldict[cindex] = sympy.symbols(curvevar)
                indx1 = indx2 + 1
                if (indx1 >= len(exprString)):
                    break
                indx = exprString.find("$",indx1)
            sympystring += exprString[indx1:]
            expression = sympy.sympify(sympystring)
            #
            # tests
            # tests
            #
            cilength = [len(self.curvelist[i%totalcnumber][0]) for i in curvenumbers]
            if max(cilength)!=min(cilength):
                print [(i,len(self.curvelist[i%totalcnumber][0])) for i in curvenumbers]
                raise RuntimeError("Not all curves have the same length")
            #
            if not self.silent:
                print expression,symboldict,curvenumbers

            return expression, symboldict, curvenumbers

        def evalCalculate(expression,symboldict,curvenumbers):
            #
            # calculate new curves
            #
            newvalue = []
            breakflag = False
            values2calc = len(self.curvelist[curvenumbers[0]%totalcnumber][0])
            for i in range(values2calc):
                substituteList = []
                for index,var in symboldict.items():
                    cnumber = curvenumbers[index]
                    curve = self.curvelist[cnumber%totalcnumber]
                    try:
                        xvalue = curve[0][i]
                        yvalue = curve[1][i]
                    except:
                        print i,values2calc,len(curve[0]),len(curve[1]),curve[2]
                        breakflag = True
                        break
                    value = yvalue if cnumber>(totalcnumber-1) else xvalue
                    substituteList.append((var,value))
                if breakflag:
                    break
                newvalue.append(expression.subs(substituteList))
            return newvalue
        #
        # xValues
        xExpression,xSymboldict,xcurvenumbers = evalParser(XexprString)
        x = evalCalculate(xExpression,xSymboldict,xcurvenumbers)
        # yValues
        yExpression,ySymboldict,ycurvenumbers = evalParser(YexprString)
        y = evalCalculate(yExpression,ySymboldict,ycurvenumbers)
        #
        self.curvelist.append((x,y,[XexprString,YexprString,"",""]))
        self.marked.append(False)
        if not self.silent:
            print(self.showcurve(-1))
        #
        return

    def derive(self,cnumber):
        replay.write("c.derive(%d)\n" % cnumber)
        curve = self.curvelist[cnumber]

        xlist = curve[0]
        ylist = curve[1]
        x=[xlist[0]]
        y=[0.]
        old = 0
        new = 0
        for xi,yi in zip(xlist[1:],ylist[1:]):
            new += 1
            xold = xlist[old]
            dx = xi - xlist[old]
            dy = yi - ylist[old]
            if abs(dx)<1.e-8:
                continue
            old = new
            x.append(xi)
            y.append(dy/dx)
        curvename = curve[2]
        dyname = curvename[:-1]+["D"+curvename[-1]+"/DT"]
        self.curvelist.append((x,y,dyname))
        self.marked.append(False)
        if not self.silent:
            print(self.showcurve(-1))
        return

    def normalizeCurve(self, curve, crange, normmin, normmax):
        xmin,xmax = crange
        factor = (normmax-normmin)/(xmax-xmin)
        newcurve = []
        for value in curve:
            newcurve.append((value-xmin) * factor + normmin)
        return newcurve

    def normalizeY(self, cnumber, minval=0., maxval=1.):
        replay.write("c.normalizeY(%d, minval=%f, maxval=%f)\n" % (curve, minval, maxval))
        crange = self.getrange(cnumber)
        curve = self.curvelist[cnumber]
        ynew = self.normalizeCurve(curve[1],crange[1],minval,maxval)
        curvename = curve[2]
        normname = curvename[:-1]+[curvename[-1]+"normY"]
        self.curvelist.append((curve[0],ynew,normname))
        self.marked.append(False)
        if not self.silent:
            print(self.showcurve(-1))

    def normalizeX(self, cnumber, minval=0., maxval=1.):
        replay.write("c.normalizeX(%d, minval=%f, maxval=%f)\n" % (curve, minval, maxval))
        crange = self.getrange(cnumber)
        curve = self.curvelist[cnumber]
        xnew = self.normalizeCurve(curve[0],crange[0],minval,maxval)
        curvename = curve[2]
        normname = curvename[:-1]+[curvename[-1]+"normX"]
        self.curvelist.append((xnew,curve[1],normname))
        self.marked.append(False)
        if not self.silent:
            print(self.showcurve(-1))

    def normalizeXY(self, cnumber):
        replay.write("c.normalizeXY(%d)\n" % curve)
        crange = self.getrange(cnumber)
        curve = self.curvelist[cnumber]
        xnew = self.normalizeCurve(curve[0], crange[0], 0., 1.)
        ynew = self.normalizeCurve(curve[1], crange[1], 0., 1.)
        curvename = curve[2]
        normname = curvename[:-1]+[curvename[-1]+"normXY"]
        self.curvelist.append((xnew,ynew,normname))
        self.marked.append(False)
        if not self.silent:
            print(self.showcurve(-1))

    def interpolate(self, cnumber, xnumber, minval=None, maxval=None):
        replay.write("c.interpolate({:d}, {:d}, minval={}, maxval={})\n".format(curve, xnumber, minval, maxval))
        xrange, yrange = self.getrange(cnumber)
        if minval==None:
            minval = xrange[0]
        if maxval==None:
            maxval = xrange[1]
        (x,y,curvename) = self.curvelist[cnumber]
        newxlist = [minval+(maxval-minval)*i/(xnumber-1) for i in range(xnumber)]
        newylist = []
        iold = 0
        oldx1 = x[0]
        inew = 0
        while oldx1>newxlist[inew]:
            inew += 1
            newylist.append(y[0])
        for newx in newxlist[inew:]:
            oldx = x[iold]
            while oldx<=newx and iold<len(x)-1:
                iold += 1
                oldx = x[iold]
                oldx1 = x[iold-1]
            if oldx<=newx:
                newy = y[-1]
            else:
                newy = y[iold-1] + (newx-oldx1)/(oldx-oldx1) * (y[iold]-y[iold-1])
            newylist.append(newy)
        interpolame = curvename[:-1] + [curvename[-1]+"interpol"]
        self.curvelist.append((newxlist, newylist, interpolame))
        self.marked.append(False)
        if not self.silent:
            print(self.showcurve(-1))

    def combine(self,c1number,c2number,equal=True):
        replay.write("c.combine({:d}, {:d}, equal={})\n".format(c1number, c2number, equal))
        curve1 = self.curvelist[c1number]
        curve2 = self.curvelist[c2number]
        if equal:
            x = curve1[1]
            y = curve2[1]
            legend1 = curve1[2]
            legend2 = curve2[2]
            legend=[]
            for l1,l2 in zip(legend1,legend2):
                if l1==l2:
                    legend.append(l1)
                else:
                    legend.append(l1+"-"+l2)
        else:
            print("You have to interpolate the curves first!")
            return (None, None, ["nocurve"])
        self.curvelist.append((x, y, legend))
        self.marked.append(False)
        if not self.silent:
            print(self.showcurve(-1))
        return


    def __set_rptfilename(self,namestring):
        if path.isfile(namestring) and not self.silent:
            answer = raw_input("File exists! Overwrite? (y/n) ")
            if answer.upper()[0]!="Y":
                print("Leaving rptfilename as "+self.__rptfilename)
                return
        self.__rptfilename = namestring
    def __get_rptfilename(self):
        return self.__rptfilename
    rptfilename = property(__get_rptfilename, __set_rptfilename)


    def write(self,header=""):
        replay.write("c.write(header=\"%s\"\n" % (header))
        if max(self.marked)==False:
            return
        writelist = [i for i in range(len(self.curvelist)) if self.marked[i]]
        filename = self.__rptfilename + ".txt"
        rpt = open(filename,"w")
        if not self.silent:
            print "Now writing the following curves:",writelist
            for curve in writelist:
                print(self.showcurve(curve))
        #
        # write header
        if header!="":
            rpt.write("# "+header+"\n")
        for i in range(len(self.curvelist[0][2])):
            writestring = "#"
            for j in writelist:
                writestring += self.curvelist[j][2][i] + self.colseparator + self.colseparator
            rpt.write(writestring[:-1]+"\n")
        #
        # write data
        i = 0
        something2write = True
        while something2write:
            something2write = False
            writestring = " "
            for j in writelist:
                x,y,label = self.curvelist[j]
                if i>=len(x):
                    writestring += "%12s"%" "+self.colseparator
                    writestring += "%12s"%" "+self.colseparator
                else:
                    writestring += "%12.6g"%x[i]+self.colseparator
                    writestring += "%12.6g"%y[i]+self.colseparator
                    something2write = True
            if something2write:
                rpt.write(writestring[:-1]+"\n")
                i += 1
        rpt.close()

    def write1x(self,header=""):
        replay.write("c.write1x(header=\"%s\"\n" % (header))
        if max(self.marked)==False:
            return
        writelist = [i for i in range(len(self.curvelist)) if self.marked[i]]
        filename = self.__rptfilename + ".txt"
        rpt = open(filename,"w")
        x0 = self.curvelist[writelist[0]][0]
        lenx0 = len(x0)
        minx0 = min(x0)
        maxx0 = max(x0)
        for curveID in writelist[1:]:
            curve = self.curvelist[curveID]
            if lenx0 != len(curve[0]) and minx0!=min(curve[0]) and maxx0!=max(curve[0]):
                print("Cannot write with single x-Values since curves are not equal!")
                print("You have to interpolate the curves first.")
                return
        if not self.silent:
            print "Now writing the following curves:"
            for curve in writelist:
                print(self.showcurve(curve))
        #
        # write header
        if header!="":
            rpt.write("# "+header+"\n")
        for i in range(len(self.curvelist[0][2])):
            writestring = "#" + self.colseparator
            for j in writelist:
                writestring += self.curvelist[j][2][i] + self.colseparator
            rpt.write(writestring[:-1]+"\n")
        #
        # write data
        i = 0
        something2write = True
        while something2write:
            something2write = False
            writestring = " "
            for j in writelist:
                x,y,label = self.curvelist[j]
                if i>=len(x):
                    writestring += "%12s"%" " + self.colseparator
                else:
                    xvalue = x[i]
                    writestring += "%12.6g"%y[i] + self.colseparator
                    something2write = True
            if something2write:
                writestring = "%12.6g"%xvalue + self.colseparator + writestring
                rpt.write(writestring[:-1]+"\n")
                i += 1
        rpt.close()

    def test(self):
        myValue = repr(self.__repr__())
        print myValue
        print len(globals()), len(locals())
        print globals().keys()
        for var,values in locals().items():
            if var=="c":
                print values
                print repr(values)
            if repr(values)==myValue:
                return var

if __name__=="__main__":
    curvelist=[]
    c = history(argv[1])
    if len(argv)>1:
        for ar in argv[1:]:
            c.getFromOdb(ar)

