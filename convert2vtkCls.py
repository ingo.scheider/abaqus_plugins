'''
Copyright 2021 Helmholtz-Zentrum Hereon

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author: Ingo Scheider, Qingbin Liu
E-mail ingo.scheider@hereon.de, liuqingb@mail2.sysu.edu.cn; liuqingb@pku.edu.cn
--------------------------------------------------------------------------------
The script is to convert the data from Abaqus output database format
to vtu or pvtu file format for parallel visualization, respectively.
Python script performs the following three major steps:
    1) reading Abaqus output file according to the architecture of ODB;
    2) data decomposition for parallel visualization (based on instances)
    3) writing VTU (and VTK) for Paraview.
'''

#import necessary modules to handle Abaqus output database, files and string
from odbAccess import *
from abaqusConstants import *
from os import path
from os import mkdir, rename, getcwd
import xml.etree.ElementTree as ET
from xml.dom import minidom
import time
import numpy as np


def checkFile(filename, suffix):
    if path.isfile(filename):
        return filename
    if suffix[0]==".":
        suffix = suffix[1:]
    filename += "." + suffix
    if path.isfile(filename):
        return filename
    else:
        return ""


def intListFromString(s, maxvalue=1):
    '''
    check input s:
        s==int: return [s]
        s==list: evaluate integer values, return list(int)
        s==string:
            if s contains ":":
                assume generator of type start:end:increment, create a range list
                start,end,incr = s.split(":")
                if end is not given: end = maxvalue
                if incr is not given: incr = 1
                if start is not gven: start = 0
            else:
                split(",")
                try:
                   create a list(int)
    '''
    if type(s)==int:
        outlist = [s]
    elif type(s)==list:
        try:
            outlist = [int(ele.strip()) for ele in s]
        except:
            return False
    elif type(s)==str:
        if ":" in s:
            l = s.split(":")
            start = 0 if l[0].strip()=="" else int(l[0])
            end = maxvalue if l[1].strip()=="" else int(l[1])
            incr = int(l[2]) if len(l)==3 else 1
            outlist = range(start,end,incr)
        else:
            l = s.replace(","," ").split()
            try:
                outlist = [int(ele) for ele in l]
            except:
                return range(maxvalue)
    return outlist


def printMessage(msgtype, msgstring):
    typelist = ["Note", "Warning", "Error"]
    if type(msgtype)!=int or msgtype<0 or msgtype>=len(typelist):
        printMessage(2,"Message type %s is not available" % repr(msgtype))
    else:
        print "** %7s ************************************" % typelist[msgtype]
        print msgstring
        print "***********************************************\n"


class Parameter():

    def __init__(self, xml=None):
        self.__version__ = "1.3"
        self.odb = ""
        self.vtupath = ""
        self.vtkpath = ""
        self.odbpath = ""
        self.includeInstance = []
        self.excludeInstance = []
        self.includeVar = []
        self.excludeVar = []
        self.steps = []
        self.framesXml = {}
        self.frames = {}
        self.legacy = False

        if xml:
            self.getFromXml(xml)


    def getFromXml(self, xmlfile):
        tree = ET.parse(xmlfile)
        xmlroot = tree.getroot()
        #
        # version
        currentTag=xmlroot.find("version")
        if currentTag is not None:
            self.version = currentTag.text
            xmlversionlist = [int(i) for i in self.version.split(".")]
            if len(xmlversionlist)==1:
                xmlversionlist.append(0)
            __version__list = [int(i) for i in self.__version__.split(".")]
            if xmlversionlist[0]>__version__list[0]:
                print("Be careful, your major version number is HIGHER than the current implemented version")
                print("You may get unexpected results or even a program crash")
                print("The current version is : %s" % self.__version__)
            elif xmlversionlist[0]<__version__list[0]:
                print("Be careful, your major version number is LOWER than the current implemented version")
                print("You may get unexpected results or even a program crash")
                print("The current version is : %s" % self.__version__)
            else:
                if xmlversionlist[1]>__version__list[1]:
                    print("Be careful, your minor version number is HIGHER than the current implemented version")
                    print("You may get unexpected results or even a program crash")
                elif xmlversionlist[1]<__version__list[1]:
                    print("Be careful, your minor version number is LOWER than the current implemented version")
                    print("You may get unexpected results or even a program crash")
        #
        # odb
        currentTag=xmlroot.find("odb")
        if currentTag is not None:
            self.odb = currentTag.text
        #
        # vtu
        currentTag = xmlroot.find("vtu")
        if currentTag is not None:
            self.vtupath = currentTag.text
        else:
            self.vtupath = self.odb + ".vtu"
        #
        # vtk
        currentTag = xmlroot.find("vtk")
        if currentTag is not None:
            if currentTag.text:
                self.vtkpath = currentTag.text
            else:
                self.vtkpath = self.odb + ".vtk"
            self.legacy = True
        #
        # projectpath
        currentTag = xmlroot.find("projectpath")
        if currentTag is not None:
            self.odbpath = currentTag.text
        else:
            self.odbpath = path.abspath(".")
        #
        # includeInstance
        currentTag = xmlroot.find("includeInstance")
        if currentTag is not None:
            if xmlroot.find("excludeInstance") is not None:
                print "You may only have either includeInstance or excludeInstance in the control file"
                exit(2)
            instancesTarget = currentTag.text.replace(",", " ")
            if len(instancesTarget)>0:
                self.includeInstance = [i.upper() for i in instancesTarget.split()]
        #
        # excludeInstance
        currentTag = xmlroot.find("excludeInstance")
        if currentTag is not None:
            instancesRemove = currentTag.text.replace(",", " ")
            if len(instancesTarget)>0:
                self.excludeInstance = [i.upper() for i in instancesRemove.split()]
        #
        # includeVar
        currentTag = xmlroot.find("includeVar")
        if currentTag is not None:
            varTarget = currentTag.text.split(",")
            if len(varTarget)>0:
                self.includeVar = [i.strip().upper() for i in varTarget]
        #
        # excludeVar
        currentTag = xmlroot.find("excludeVar")
        if currentTag is not None:
            varTarget = currentTag.text.split(",")
            if len(varTarget)>0:
                self.excludeVar = [i.strip().upper() for i in varTarget]
        #
        # steps
        try:
            for currentTag in xmlroot.findall("step"):
                newstep = int(currentTag.text)
                self.steps.append(newstep)
                self.frames[newstep] = []
        except TypeError:
            pass
        #
        # frames
        try:
            for currentTag in xmlroot.findall("frame"):
                if "step" not in currentTag.attrib:
                    print "You have to provide the step number for which the frame defintion is valid"
                    exit(2)
                step = int(currentTag.attrib["step"])
                self.framesXml[step] = currentTag.text
        except:
               pass
        #
        #get the quantity of pieces to partition (not yet used)
        currentTag = xmlroot.find("partitions")
        if currentTag is not None:
            self.partitions = int(currentTag.text)
        else:
            self.partitions = 1
        #
        # Check for existence of odb file
        self.checkValidity()
        #
        print "control file reading finished"
        return


    def checkValidity(self):
        #
        # check odb file and path
        if not self.odb:
            print "No odb file provided!"
            exit(2)
        odb = checkFile(path.join(self.odbpath,self.odb),"odb")
        if not odb:
            print "odb file " + self.odb + " does not exist!"
            exit(2)
        else:
            self.odbFullname = odb
        #
        # set vtu and vtk paths
        self.vtuFullpath = path.join(self.odbpath, self.vtupath)
        self.vtkFullpath = path.join(self.odbpath, self.vtkpath)
        return True


    def checkVariableIncluded(self, var):
        if len(self.includeVar)>0:
            for includes in self.includeVar:
                asterisk = includes.find("*")
                if asterisk==0:
                    varlength = len(includes[1:])
                    if includes[1:]==var[len(var)-varlength:]:
                        return True
                elif asterisk>0 and includes[:asterisk]==var[:asterisk]:
                    return True
                elif includes==var:
                    return True
            return False
        for excludes in self.excludeVar:
            asterisk = excludes.find("*")
            if asterisk==0:
                varlength = len(excludes[1:])
                if excludes[1:]==var[len(var)-varlength:]:
                    return False
            if asterisk>0 and excludes[:asterisk]==var[:asterisk]:
                return False
            elif excludes==var:
                return False
        return True


class Modeldata():
    """
    This class is supposed to hold all the model data, 
    which can then transferred from any to any other format
    It stores:
        nodal coordinates
        element:
            connectivity
            type
            material
        instance:
            Names
            nodes
            elements
    """
    def __init__(self):
        self.coordinates = []  # list: VTK node coordinates
        self.elements = []     # list: VTK element connectivity list (VTK node numbers)
        self.elementtype = []       # list: abq element type on VTK element indices
        self.elementMaterial = []
        self.instances = []
        self.instanceNodes = {}
        self.instanceElements = {}
        self.maxNodenumber = 0
        self.maxElementnumber = 0
        self.nnode = 0
        self.nelement = 0
        self.nconnectEntries = 0
        
class ConvertVTK():
    """Converter class for odb files to put into vtu and optional vtk files

    Attributes
    ----------
    parameter : Class Parameter()
        an instance of the Parameter class that holds all parameters necessary for the conversion

    Methods
    --------
    getParameterFromXml(xmlfilename)
        Initializes all parameters for conversion from the xml control file
    getParameterFromOdb(odbfilename)
    showParamerer()
        prints the current parameter set for conversion to stdout
    readOdb()
        reads the general odb information (instances, steps, frames)
    showElementTypes()
        prints the currently support element types to stdout
    addElementType(abqname, vtknumber, nodes)
        adds an abaqus element type to the list of supported element type (only for the current session)
    writeXml()
        writes the xml control file with the currently active parameters to the xml file.
        The name is taken from the odb file with suffix .xml instead of .odb.
        If this file already exists, the existing is renamed to <odbname><2digit-number>.xml>
        and the new xml control file is then written to <odbname>.xml
    do()
        converts the odb file to a set of vtu files, which are stored in
        parameter.vtupath/*.vtu
    """
    __version__ = "0.01"
    intPerRow = 1
    intPerRowVTK = 16
    zero = 1.e-20
    floatFormat = " %14.7e"
    replaceVarnames = {"S": "STRESS", "E": "STRAIN"}
    numberRepresentation = ["ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE"]
    elementTypes = {"CPE3": (5,3),
                    "CPS3": (5,3),
                    "CAX3": (5,3),
                    "R3D3": (5,3),
                    "S3": (5,3),
                    "CPE4": (9,4),
                    "CPS4": (9,4),
                    "CAX4": (9,4),
                    "R3D4": (9,4),
                    "S4": (9,4),
                    "M3D4": (9,4),
                    "COH2D4": (9,4),
                    "CPE6": (22,6),
                    "CPS6": (22,6),
                    "CAX6": (22,6),
                    "CPE8": (23,8),
                    "CPS8": (23,8),
                    "CAX8": (23,8),
                    "S8": (23,8),
                    "M3D3": (23,8),
                    "C3D4": (10,4),
                    "C3D6": (13,6),
                    "C3D8": (12,8),
                    "C3D10": (24,10),
                    "C3D15": (26,15),
                    "C3D20": (25,20),
                    "DC2D3": (5,3),
                    "DC2D4": (9,4),
                    "DC2D6": (22,6),
                    "DC2D8": (23,8),
                    "DC3D4": (10,4),
                    "DC3D6": (13,6),
                    "DC3D8": (12,8),
                    "DC3D10": (24,10),
                    "DC3D15": (26,15),
                    "DC3D20": (25,20),
                    "COH3D8": (12,8),
                    "T2D2": (3,2),
                    "T3D2": (3,3),
                    "T2D3": (21,3),
                    "T3D3": (21,3),
                    "B21": (3,2),
                    "B23": (3,2),
                    "PIPE21": (3,2),
                    "B22": (21,3),
                    "PIPE22": (21,3),
                    "B31": (3,2),
                    "B33": (3,2),
                    "PIPE31": (3,2),
                    "B32": (21,3),
                    "PIPE32": (21,3)
                    }
    abaqusBCdef = {"XSYMM": (1,0,0),
                   "YSYMM": (0,1,0),
                   "ZSYMM": (0,0,1),
                   "PINNED": (1,1,1),
                   "ENCASTRE": (1,1,1),
                   "XASYMM": (0,1,1),
                   "YASYMM": (1,0,1),
                   "ZASYMM": (1,1,0)
                   }


    def __init__(self, xml=None, odb=None):
        self.odbChecked = False
        if xml and odb:
            msgstring = "You may only enter either an xml file or an odb file!\n" + \
                "Please run <instance>.getParameterFromXml or <instance>.getParameterFromOdb"
            printMessage(1,msgstring)
        elif not (xml or odb):
            msgstring = "You should specify either an xml file or an odb file!\n" + \
                "Please run <instance>.getParameterFromXml or <instance>.getParameterFromOdb"
            printMessage(0,msgstring)
        elif xml:
            self.parameter = self.getParameterFromXml(xml)
            if self.parameter==None:
                msgstring = "No parameters defined yet"
                printMessage(1,msgstring)
            elif self.parameter.checkValidity():
                self.readOdb()
            else:
                msgstring = "The validity check of the odb was not successful!"
                printMessage(2,msgstring)
        elif odb:
            self.parameter = self.getParameterFromOdb(odb)
            if self.parameter.checkValidity():
                self.readOdb()
            else:
                msgstring = "The validity check of the odb was not successful!"
                printMessage(2,msgstring)


    def getParameterFromXml(self, xmlfile):
        xmlfilename = checkFile(xmlfile, "xml")
        if xmlfilename:
            return Parameter(xmlfilename)
        else:
            msgstring = 'Parameter file %s not found' % xmlfile
            printMessage(2, msgstring)
        return


    def getParameterFromOdb(self, odbfile):
        odbfilename = checkFile(odbfile, "odb")
        if not odbfilename:
            msgstring = 'odb file %s not found' % odbfile
            printMessage(2, msgstring)
            return
        odbpath,odbname = path.split(odbfilename)
        if odbpath == "":
            odbpath = "."
        par = Parameter()
        par.odb = odbname
        par.vtupath = odbname[:-3] + "vtu"
        par.odbpath = path.abspath(odbpath)
        return par
    
    @property
    def version(self):
        return self.__version__
    @version.setter
    def version(self,versionString):
        self.__version__ = versionString

        

    def showParameter(self):
        print "Odb: " + self.parameter.odb
        print "VTU directory: " + self.parameter.vtupath
        print "project path: " + self.parameter.odbpath
        if self.parameter.includeInstance==[]:
            print "Instances included: all"
        else:
            print "Instances included: " + ",".join(self.parameter.includeInstance)
        if self.parameter.excludeInstance==[]:
            print "Instances excluded: None"
        else:
            print "Instances excluded: " + ",".join(self.parameter.excludeInstance)
        if self.parameter.includeVar==[]:
            print "Variables included: all"
        else:
            print "Variables included: " + ",".join(self.parameter.includeVar)
        if self.parameter.excludeVar==[]:
            print "Variables excluded: None"
        else:
            print "Variables excluded: " + ",".join(self.parameter.excludeVar)
        print "Steps included: " + ",".join(map(repr,self.parameter.steps))
        print "Frame definitions:"
        if self.odbChecked:
            for step in self.parameter.frames:
                print "Step " + repr(step) + ": " + ",".join(map(repr,self.parameter.frames[step]))
            print "\nThe odb has been checked."
        else:
            for step in self.parameter.framesXml:
                print "Step " + repr(step) + ": " + self.parameter.framesXml[step]
            print "\nThe odb has not been checked."
        if self.parameter.legacy:
            print "Model data will also be written to vtk file ",self.parameter.vtkpath
        else:
            print "Model data will not be written to vtk files"


    def writeMapfile(self, nodeVTKnumber, elementVTKnumber):
        with open(self.parameter.odbFullname[:-4]+".map","w") as mapfile:
            mapfile.write("Nodes map\n")
            mapfile.write("#%7s, %8s" % ("abaqus", "vtk"))
            for odbNod, vtkNod in nodeVTKnumber.items():
                mapfile.write("%8d %8d\n" % (odbNod, vtkNod))
            mapfile.write("Element map\n")
            mapfile.write("#%7s, %8s" % ("abaqus", "vtk"))
            for odbEl, vtkEl in elementVTKnumber.items():
                mapfile.write("%8d %8d\n" % (odbEl, vtkEl))
        return


    def checkOdbdata(self):
        checkSuccess = True
        #
        # check steps
        stepsInOdb = self.framesInOdb.keys()
        if len(self.parameter.steps)==0:
            self.parameter.steps = stepsInOdb
        else:
            if np.any([s not in stepsInOdb for s in self.parameter.steps]):
                print "Requested step is not in odb"
                checkSuccess = False
        # check frame data
        if np.any([s not in stepsInOdb for s in self.parameter.framesXml.keys()]):
            print "You have non-existing steps in your frame dictionary"
            checkSuccess = False
        else:
            for stepNumber in self.parameter.steps:
                if stepNumber in self.parameter.framesXml.keys():
                    self.parameter.frames[stepNumber] = intListFromString(self.parameter.framesXml[stepNumber], len(self.framesInOdb[stepNumber]))
                else:
                    self.parameter.frames[stepNumber] = self.framesInOdb[stepNumber]
        #
        # Check instance data
        # instances to be included:
        if len(self.parameter.includeInstance) > 0:
            instanceFlag = [inst not in self.instancesInOdb for inst in self.parameter.includeInstance]
            if np.any(instanceFlag):
                print "One of the instances to be included is not in the odb instances"
                checkSuccess = False
        else:
            self.parameter.includeInstance = self.instancesInOdb
        # instances to be excluded:
        if len(self.parameter.excludeInstance) > 0:
            excludedList = self.parameter.excludeInstance.replace(",", " ").split()
            for excluded in excludedList:
                self.parameter.includeInstance.remove(excluded.strip())

        return checkSuccess


    def readOdb(self):
        #open ODB ( Abaqus output database )
        self.odb = openOdb(self.parameter.odbFullname, 
                           readInternalSets = True,
                           readOnly = True)
        print "ODB %s opened" % self.parameter.odbFullname
        print "Writing vtu files to ", self.parameter.vtuFullpath
        #
        # steps to be included
        self.framesInOdb = {}
        for step in self.odb.steps.values():
            self.framesInOdb[step.number] = range(len(step.frames))
        #
        # instances to be included
        self.instancesInOdb = self.odb.rootAssembly.instances.keys()

        self.odbChecked = self.checkOdbdata()
        return


    def showElementTypes(self):
        print "The following element types are currently supported:"
        print "\n%10s|%10s|%10s" % ("ABQ name", "vtk element", "nodes")
        for name,eltypeValue in ConvertVTK.elementTypes.items():
            print "%10s|%7d   |%5d" % (name, eltypeValue[1], eltypeValue[0])
        return


    def addElementType(self, elementName, vtkElement, nodes):
        try:
            ConvertVTK.elementTypes[elementName] = (int(vtkElement), int(nodes))
        except ValueError:
            msgstring = "Could not evaluate vtkElement number from"
            mesgstring += repr(vtkElement)," or node number" + repr(nodes)
            printMessage(2, msgstring)


    def writeXml(self):
        if not self.odbChecked:
            msgstring = "Check odb first (use <instance>.readOdb() ) \n"
            msgstring += "or add an odb name to the function call (<instance>.writeXml(<odbname>) )"
            printMessage(2, msgstring)
            return
        root = ET.Element("parameter")
        par = ET.SubElement(root, "version")
        par.text = repr(self.version)
        par = ET.SubElement(root, "odb")
        par.text = self.parameter.odb
        if par.text[-4:]==".odb":
            par.text = par.text[:-4]
        par = ET.SubElement(root, "vtu")
        par.text = self.parameter.vtupath
        root.append(ET.Comment("vtk"))
        par = ET.SubElement(root, "projectpath")
        par.text = self.parameter.odbpath
        par = ET.SubElement(root, "includeInstance")
        par.text = ",".join(self.instancesInOdb)
        root.append(ET.Comment("excludeInstance"))
        root.append(ET.Comment("includeVar"))
        root.append(ET.Comment("excludeVar"))
        for step in self.framesInOdb:
            par = ET.SubElement(root, "step")
            par.text = repr(step)
            par = ET.SubElement(root, "frame")
            par.attrib["step"] = repr(step)
            par.text = "0:%d:1" % (self.framesInOdb[step][-1] + 1, )
        #
        # create pretty xml
        
        xmlstring = ET.tostring(root, "utf-8")
        reparsed = minidom.parseString(xmlstring)
        #
        # write to new file (renaming existing files)
        xmlfilebase = self.parameter.odbFullname[:-4]
        xmlfilename = xmlfilebase + ".xml"
        print(xmlfilename)
        i = 0
        while path.isfile(xmlfilename):
            print(xmlfilename + " exists!")
            xmlfilename = "%s%02d.xml" % (xmlfilebase, i)
            i += 1
        if i>0:
            os.rename(xmlfilebase + ".xml", xmlfilename)
        with open(xmlfilebase + ".xml","w") as xmlstream:
            xmlstream.write(reparsed.toprettyxml(indent="  "))




    def do(self):
        starttime = time.time()
        if not self.odbChecked:
            msgstring = "Cannot convert since odb has not been read successfully."
            printMessage(2, msgstring)
            return
        if not path.exists(self.parameter.vtuFullpath):
            mkdir(self.parameter.vtuFullpath)

        asm = self.odb.rootAssembly

        piecenum = len(self.parameter.includeInstance)

        '''
        Part 1
        Reading abaqus node and element data
        '''
        #
        # reading materials and sections
        materials = dict([(name,[]) for name in self.odb.materials.keys()])
        print "Available materials: ",",".join(materials.keys())
        sections = {}
        for section in self.odb.sections.keys():
            try:
                secMaterial = self.odb.sections[section].material
                sections[section] = materials.keys().index(secMaterial)
            except:
                sections[section] = 0
        #
        # reading nodes and elements
        coordinates = []  # list: VTK node coordinates
        nodeVTKnumber = {}  # dict: correlation between abq node number (key) and VTK node number (value)
        nodeABQnumber = []  # list: correlation between VTK node number (index) and abq node number (value)
        elements = []     # list: VTK element connectivity list (VTK node numbers)
        elementVTKnumber = {}  # dict: correlation between abq element number (key) and VTK element number (value)
        elementABQnumber = []  # list: correlation between VTK element number (index) and abq element number (value)
        elementtype = []       # list: abq element type on VTK element indices
        elementtypeNotImplemented = {}  # dictionary of the non-implemeneted element types
        elementMaterial = []
        instances = []
        instanceNodes = {}
        instanceElements = {}
        instanceNodeOffset = {}
        instanceVtkNodeOffset = {}
        instanceElementOffset = {}
        maxNodenumber = 0
        maxElementnumber = 0
        nnode = 0
        nelement = 0
        nconnectEntries = 0
        for instance in self.parameter.includeInstance:
            ABQinstance = asm.instances[instance]
            instances.append(instance)
            instanceNodes[instance] = []
            instanceElements[instance] = []
            instanceNodeOffset[instance] = maxNodenumber
            instanceVtkNodeOffset[instance] = nnode
            instanceElementOffset[instance] = maxElementnumber
            for node in ABQinstance.nodes:
                coordinates.append(node.coordinates)
                abqNode = node.label + instanceNodeOffset[instance]
                nodeABQnumber.append(node.label)
                nodeVTKnumber[abqNode] = nnode
                instanceNodes[instance].append(nnode)
                nnode += 1
            maxNodenumber = max(nodeVTKnumber.keys())
            for element in ABQinstance.elements:
                eltype = element.type
                while eltype[-1] in ["R","H","I","T","M","V","P"]:
                    eltype = eltype[:-1]
                if eltype not in ConvertVTK.elementTypes:
                    if eltype not in elementtypeNotImplemented:
                        elementtypeNotImplemented[eltype] = 1
                    else:
                        elementtypeNotImplemented[eltype] += 1                        
                    continue
                elementtype.append(eltype)
                elementMaterial.append(0)
                connectivity = [nodeVTKnumber[node + instanceNodeOffset[instance]] for node in element.connectivity]
                elements.append(connectivity)
                nconnectEntries += len(connectivity) + 1
                elementVTKnumber[element.label + instanceElementOffset[instance]] = nelement
                elementABQnumber.append(element.label)
                instanceElements[instance].append(nelement)
                nelement += 1
            if len(instanceElements[instance])==0:
                instances.pop()
                del instanceNodes[instance]
                del instanceElements[instance]
                del instanceNodeOffset[instance]
                del instanceVtkNodeOffset[instance]
                del instanceElementOffset[instance]
                continue
            maxElementnumber = max(elementVTKnumber.keys())
            #
            for section in ABQinstance.sectionAssignments:
                secname = section.sectionName
                matnumber = sections[secname]
                for element in section.region.elements:
                    elnumber = elementVTKnumber[element.label + instanceElementOffset[instance]]
                    elementMaterial[elnumber] = matnumber
        # end instance for loop
        totalNodes = nnode
        totalElements = nelement
        #
        # extract boundary conditions
        bcondition = [(0,0,0) for _ in range(totalNodes)]
        for nset, values in asm.nodeSets.items():
            tmplist = nset.split()
            if tmplist[0] != "Step":
                continue
            bcinstance = values.instanceNames[0]
            if bcinstance not in self.parameter.includeInstance:
                continue
            try:
                dispindex = tmplist.index("Disp")
            except ValueError:
                continue
            if tmplist[dispindex+1]=="dof":
                bc = [0,0,0]
                dof = eval(tmplist[dispindex+2])
                if dof>3:
                    continue
                bc[dof-1] = 1
                bc = tuple(bc)
            elif tmplist[dispindex+1] in ConvertVTK.abaqusBCdef:
                bc = ConvertVTK.abaqusBCdef[tmplist[dispindex+1]]
            else:
                printMessage(1,
                             "Boundary condition type in  %s cannot be evaluated" % nset)
                continue
            printMessage(0, "Found boundary condition " + nset)
            for nd in values.nodes[0]:
                abqNode = nd.label + instanceNodeOffset[bcinstance]
                bc1 = bcondition[nodeVTKnumber[abqNode]]
                bcondition[nodeVTKnumber[abqNode]] = \
                    tuple([n1 | n2 for n1,n2 in zip(bc,bc1)])
        #
        # output of basic information
        print "============================="
        print "Basic Information:"
        print "Model:",self.parameter.odb
        print "Instances/blocks"
        for instance in instances:
            print "Instance %s: %d nodes (offset %d), %d elements (offset %d)" % (
                instance,
                len(instanceNodes[instance]),
                instanceNodeOffset[instance],
                len(instanceElements[instance]),
                instanceElementOffset[instance])
        print "Sum: %d nodes, %d elements" % (totalNodes, totalElements)
        if len(elementtypeNotImplemented) > 0:
            print "-----------------------"
            print "Caution: The following element types are ignored:"
            for eltypename, elementsOfType in elementtypeNotImplemented.items():
                print "Type %s: %d elements" % (eltypename, elementsOfType)
        for step,frames in self.parameter.frames.items():
            print "Step %d: Frames: %s" % (step, ",".join([repr(s) for s in frames]))
        self.writeMapfile(nodeVTKnumber, elementVTKnumber)
        time1 = time.time()
        print "Time for reading model data: %.2f s" % (time1 - starttime)
        '''
        Part 1a
        Writing legacy file(s)
        '''
        if self.parameter.legacy:
            vtkfilenames = []
            vtkfilename = self.parameter.vtkFullpath
            if vtkfilename[-4:]!=".vtk":
                vtkfilename += ".vtk"
            outfile = open(vtkfilename,"w")
            # write Header
            outfile.write("# vtk DataFile Version 3.0\n")
            if self.odb.analysisTitle=="":
                outfile.write("Data from ABAQUS odb "+self.parameter.odb+"\n")
            else:
                outfile.write(self.odb.analysisTitle + "\n")
            outfile.write("ASCII\n")
            outfile.write("DATASET UNSTRUCTURED_GRID\n")
            #<Points> Write nodes into vtk files
            outfile.write('POINTS %d double\n' % totalNodes)
            for instance in instances:
                for node in instanceNodes[instance]:
                    for coo in coordinates[node]:
                        coordmod = coo if abs(coo)>ConvertVTK.zero else 0.0
                        outfile.write(ConvertVTK.floatFormat % coordmod)
                    outfile.write('\n')
            outfile.write('\n')
            #<Cells> Write cells into vtk files
            outfile.write('CELLS %d %d\n' % (totalElements, nconnectEntries))
            for instance in instances:
                vtkNodeOffset = instanceVtkNodeOffset[instance]
                print("Now write element data for instance %s, node offset %d" % (instance, vtkNodeOffset))
                for element in instanceElements[instance]:
                    # elementConn = [nd - vtkNodeOffset for nd in elements[element]]
                    outstring = " ".join(map(repr, elements[element]) )   # elementConn) )
                    outfile.write(' %d %s\n' % (len(elements[element]), outstring))
            outfile.write('\n')
            #Type
            outfile.write('CELL_TYPES %d\n' % totalElements)
            for instance in instances:
                for i,element in enumerate(instanceElements[instance]):
                    outfile.write(' %2d' % ConvertVTK.elementTypes[elementtype[element]][0])
                    if ((i+1) % ConvertVTK.intPerRowVTK) == 0:
                        outfile.write("\n")
                if (i+1) % ConvertVTK.intPerRowVTK != 0:
                    outfile.write("\n")
            outfile.write('\n')

            #<PointData> Write boundary conditions as vectors into vtk files
            outfile.write('POINT_DATA %d\n' % totalNodes)
            outfile.write('VECTORS boundaryConditions float\n')
            for bc in bcondition:
                outfile.write("%d %d %d\n" % bc)
            #<Cell data>
            outfile.write('CELL_DATA %d\n' % totalElements)
            # material ids
            outfile.write('SCALARS MaterialID int 1\n LOOKUP_TABLE default\n')
            for instance in instances:
                for i, element in enumerate(instanceElements[instance]):
                    outfile.write(" %2d" % elementMaterial[element])
                    if ((i+1) % ConvertVTK.intPerRowVTK) == 0:
                        outfile.write("\n")
                if (i+1) % ConvertVTK.intPerRowVTK != 0:
                    outfile.write("\n")
            outfile.write('\n')
            outfile.close()

        '''
        Part 2
        Reading results data
        '''
        #step cycle
        #
        for stepNumber in self.parameter.frames.keys():
            for step in self.odb.steps.values():
                if step.number == stepNumber:
                    break
            maxframe = max(self.parameter.frames[stepNumber]+[1])
            frameformat = "%0" + repr(int(np.log10(maxframe)+1.)) + "d"
            #
            # frame cycle
            #
            for frameNumber in self.parameter.frames[stepNumber]:
                print "Starting step %d, frame %d" % (stepNumber, frameNumber)
                frame = step.frames[frameNumber]
                variableTarget = []
                for var in frame.fieldOutputs.keys():
                    if self.parameter.checkVariableIncluded(var):
                        variableTarget.append(var)
                #
                # initialize output information
                descriptiondict = {}
                vtufilenames = []
                # Access an output variable (name: var, repo: output)
                for instance in instances:
                    time1 = time.time()
                    print "Evaluating instance " + instance
                    outputdict = {}
                    nodeOffset = instanceNodeOffset[instance]
                    vtkNodeOffset = instanceVtkNodeOffset[instance]
                    nodesInInstance = len(instanceNodes[instance])
                    for var in variableTarget:
                        outputglobal = frame.fieldOutputs[var]
                        position = outputglobal.locations[0].position
                        output = frame.fieldOutputs[var].getSubset(region=asm.instances[instance])
                        try:
                            position = output.locations[0].position
                        except:
                            pass
                            #continue
                        description = output.name.replace(".","_").replace(" ","")
                        ncomponents = max(1, len(output.componentLabels))
                        if description in ConvertVTK.replaceVarnames:
                            description = ConvertVTK.replaceVarnames[description]
                        descriptiondict[var] = (description, ncomponents)
                        vartype = output.type
                        # check for integration point data:
                        # must be transferred to nodes, and
                        # averaged over all elements connected to this node
                        if position==INTEGRATION_POINT:
                            nodeoutput = output.getSubset(position=ELEMENT_NODAL)
                            outputdict[var] = np.zeros((nodesInInstance,ncomponents))
                            nentries = np.zeros(nodesInInstance)
                            for entry in nodeoutput.values:
                                nodeLabel = entry.nodeLabel + nodeOffset
                                if nodeLabel not in nodeVTKnumber:
                                    continue
                                node = nodeVTKnumber[nodeLabel] - vtkNodeOffset
                                outputdict[var][node] += entry.data
                                nentries[node] += 1
                            for nd in range(nodesInInstance):
                                if nentries[nd] != 0:
                                    outputdict[var][nd] /= nentries[nd]
                            # now write the invariants, etc., of tensor values
                            if vartype==TENSOR_3D_FULL:
                                for scalar in nodeoutput.validInvariants:
                                    var_inv = var + "_" + scalar.getText()
                                    if not self.parameter.checkVariableIncluded(var_inv):
                                        continue
                                    description_inv = description + "_" + scalar.getText()
                                    descriptiondict[var_inv] = (description_inv, 1)
                                    outputdict[var_inv] = np.zeros((nodesInInstance,1))
                                    try:
                                        scalaroutput = nodeoutput.getScalarField(scalar)
                                    except OdbError:
                                        continue
                                    nentries = np.zeros(nodesInInstance)
                                    for entry in scalaroutput.values:
                                        nodeLabel = entry.nodeLabel + nodeOffset
                                        if nodeLabel not in nodeVTKnumber:
                                            continue
                                        node = nodeVTKnumber[nodeLabel] - vtkNodeOffset
                                        outputdict[var_inv][node] += entry.data
                                        nentries[node] += 1
                                    for nd in range(nodesInInstance):
                                        if nentries[nd] != 0:
                                            outputdict[var_inv][nd] /= nentries[nd]
                        elif position==NODAL:
                            # nodal data is much easier ...
                            outputdict[var] = np.zeros((nodesInInstance,ncomponents))
                            for entry in output.values:
                                nodeLabel = entry.nodeLabel + nodeOffset
                                if nodeLabel not in nodeVTKnumber:
                                    print "why is ",entry.nodeLabel," not in nodeVTKnumber?"
                                    continue
                                node = nodeVTKnumber[nodeLabel] - vtkNodeOffset
                                outputdict[var][node] = entry.data
                    '''
                    Part 3
                    Writing results data (per frame)
                    '''
                    # piece cycle, to partion the model and create each piece for vtu files

                    instanceNumber = instances.index(instance)
                    #create and open a (.vtu) files
                    if (piecenum > 1):
                        vtufilename = ("%s_%s_%d_" + frameformat + ".vtu") % (self.parameter.odb,
                                                                              instance,
                                                                              stepNumber,
                                                                              frameNumber)
                        vtufilenames.append(vtufilename)
                    else:
                        vtufilename = ("%s_%d_" + frameformat + ".vtu") % (self.parameter.odb,
                                                                          stepNumber,
                                                                          frameNumber)
                    vtufilename = path.join(self.parameter.vtuFullpath, vtufilename)
                    outfile = open(vtufilename,"w")

                    #<VTKFile>, including the type of mesh, version, and byte_order
                    outfile.write('<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n')
                    #<UnstructuredGrid>
                    outfile.write('<UnstructuredGrid>\n')
                    #<Piece>, including the number of points and cells
                    nel = len(instanceElements[instance])
                    outfile.write('<Piece NumberOfPoints="%d" NumberOfCells="%d">\n' % (nodesInInstance, nel))

                    #<Points> Write nodes into vtu files
                    outfile.write('<Points>\n')
                    outfile.write('<DataArray type="Float32" NumberOfComponents="3" format="ascii">'+'\n')
                    for node in instanceNodes[instance]:
                        for coo in coordinates[node]:
                            coordmod = coo if abs(coo)>ConvertVTK.zero else 0.0
                            outfile.write(ConvertVTK.floatFormat % coordmod)
                        outfile.write('\n')
                    outfile.write('</DataArray>\n')
                    outfile.write('</Points>\n')
                    #</Points>

                    #scalars = ""
                    #vectors = ""
                    #tensors = ""
                    #for varname, (vardescription, varlength) in descriptiondict.items():
                        #if varlength==1:
                            #scalars += vardescription + ","
                        #elif varlength==3:
                            #vectords += vardescription + ","
                        #elif varlength==6:
                            #tensors += vardescription + ","
                    #outfile.write('<PointData scalars="%s" vectors="%s" tensors="%s">\n'
                                  #% (scalars[:-1], vectors[:-1], tensors[:-1])
                                 #)
                    outfile.write('<PointData>\n')
                    #<DataArray> Write results into vtu files
                    for varname, (vardescription, varlength) in descriptiondict.items():
                        if varname not in outputdict:
                            continue
                        for i in range(10):
                            if repr(i) in vardescription:
                                idx = vardescription.index(repr(i))
                                vardescription = vardescription[:idx] + ConvertVTK.numberRepresentation[i] + vardescription[idx+1:]
                        outfile.write('<DataArray type="Float32" Name="%s" ' % vardescription)
                        outfile.write('NumberOfComponents="%d" format="ascii">\n' % varlength)
                        for node in instanceNodes[instance]:
                            nodevalue = outputdict[varname][node - vtkNodeOffset]
                            for v in nodevalue:
                                vmod = v if abs(v) > ConvertVTK.zero else 0.0
                                outfile.write(ConvertVTK.floatFormat % vmod)
                            outfile.write('\n')
                        outfile.write("</DataArray>"+'\n')
                    # abaqus node numbers
                    #outfile.write('<DataArray type="Int32" Name="ABQNODENUMBER" ')
                    #outfile.write('NumberOfComponents="1" format="ascii">\n')
                    #for i, node in enumerate(instanceNodes[instance]):
                        #outfile.write("%7d " % nodeABQnumber[node])
                        #if ((i+1) % ConvertVTK.intPerRow) == 0:
                            #outfile.write("\n")
                    #if (i+1) % ConvertVTK.intPerRow != 0:
                        #outfile.write("\n")
                    #outfile.write("</DataArray>"+'\n')
                    # instance number is only necessary if more than one instance exists
                    #if piecenum>1:
                        #outfile.write('<DataArray type="UInt8" Name="NODEINSTANCE" ')
                        #outfile.write('NumberOfComponents="1" format="ascii">\n')
                        #for i in range(nodesInInstance):
                            #outfile.write("%4d " % instanceNumber)
                            #if ((i+1) % ConvertVTK.intPerRow) == 0:
                                #outfile.write("\n")
                        #if (i+1) % ConvertVTK.intPerRow != 0:
                            #outfile.write("\n")
                        #outfile.write("</DataArray>"+'\n')
                    outfile.write("</PointData>"+'\n')
                    #</PointData>

                    #<Cells> Write cells into vtu files
                    outfile.write('<Cells>\n')
                    #Connectivity
                    outfile.write('<DataArray type="Int32" Name="connectivity" format="ascii">\n')
                    #maxnd = 0
                    #allnodes = []
                    for element in instanceElements[instance]:
                        elementConn = [nd - vtkNodeOffset for nd in elements[element]]
                        outstring = " ".join(map(repr, elementConn) )
                        outfile.write(' %s\n' % outstring)
                    outfile.write('</DataArray>\n')
                    #Offsets
                    outfile.write('<DataArray type="Int32" Name="offsets" format="ascii">\n')
                    offset = 0
                    for element in instanceElements[instance]:
                        nodenumbers = ConvertVTK.elementTypes[elementtype[element]][1]
                        outfile.write('%d\n' % (offset+nodenumbers,))
                        offset += nodenumbers
                    outfile.write('</DataArray>\n')
                    #Type
                    outfile.write('<DataArray type="UInt8" Name="types" format="ascii">\n')
                    for element in instanceElements[instance]:
                        outfile.write('%d\n' % ConvertVTK.elementTypes[elementtype[element]][0])
                    outfile.write('</DataArray>\n')
                    outfile.write('</Cells>\n')
                    #</Cells>

                    #<Cell data>
                    outfile.write('<CellData>\n')
                    # abaqus element numbers
                    outfile.write('<DataArray type="Int32" Name="ABQELEMENTNUMBER" format="ascii">\n')
                    for i, element in enumerate(instanceElements[instance]):
                        outfile.write("%7d " % elementABQnumber[element])
                        if ((i+1) % ConvertVTK.intPerRow) == 0:
                            outfile.write("\n")
                    if (i+1) % ConvertVTK.intPerRow != 0:
                        outfile.write("\n")
                    outfile.write("</DataArray>"+'\n')
                    # instance number is only necessary if more than one instance exists
                    if piecenum>1:
                        outfile.write('<DataArray type="UInt8" Name="ELEMENTINSTANCE" format="ascii">\n')
                        for i in range(len(instanceElements[instance])):
                            outfile.write("%3d " % instanceNumber)
                            if ((i+1) % ConvertVTK.intPerRow) == 0:
                                outfile.write("\n")
                        if (i+1) % ConvertVTK.intPerRow != 0:
                            outfile.write("\n")
                        outfile.write("</DataArray>"+'\n')
                    outfile.write('</CellData>\n')
                    #</CellData>
                    #
                    #</Piece>
                    outfile.write('</Piece>\n')
                    #</UnstructuredGrid>
                    outfile.write('</UnstructuredGrid>\n')
                    #</VTKFile>
                    outfile.write('</VTKFile>\n')

                    outfile.close()

                    print "    took %.2f s" % (time.time() - time1)
                    time1 = time.time()
                    '''====================================================================='''
                if ( piecenum > 1 ):
                    #create .pvtu files for parallel visualization
                    pvtufilename = ("%s_%d_" + frameformat + ".pvtu") % (self.parameter.odb,
                                                                         stepNumber,
                                                                         frameNumber)
                    pvtufilename = path.join(self.parameter.vtuFullpath, pvtufilename)
                    outfile = open(pvtufilename,'w')

                    #write the basic information for .pvtu files
                    outfile.write('<?xml version="1.0"?>\n')
                    outfile.write('<VTKFile type="PUnstructuredGrid" version="0.1" byte_order="LittleEndian">\n')
                    outfile.write('<PUnstructuredGrid GhostLevel="0">\n')
                    # outfile.write('<PUnstructuredGrid GhostLevel="' + str(piecenum) + '">\n')
                    #pointdata
                    outfile.write('<PPointData>\n')
                    for varname, varlength in descriptiondict.values():
                        for i in range(10):
                            if repr(i) in varname:
                                idx = varname.index(repr(i))
                                varname = varname[:idx] + ConvertVTK.numberRepresentation[i] + varname[idx+1:]
                        outfile.write('<PDataArray type="Float32" Name="%s" ' % varname)
                        outfile.write('NumberOfComponents="%d" format="ascii"/>\n' % varlength)
                    #outfile.write('<PDataArray type="Int32" Name="abqNodenumber" ')
                    #outfile.write('NumberOfComponents="1" format="ascii"/>\n')
                    #outfile.write('<PDataArray type="UInt8" Name="nodeInstance" ')
                    #outfile.write('NumberOfComponents="1" format="ascii"/>\n')
                    outfile.write('</PPointData>\n')

                    #celldata
                    outfile.write('<PCellData>\n')
                    outfile.write('<PDataArray type="Int32" Name="ABQELEMENTNUMBER" ')
                    outfile.write('NumberOfComponents="1" format="ascii"/>\n')
                    outfile.write('<PDataArray type="UInt8" Name="ELEMENTINSTANCE" ')
                    outfile.write('NumberOfComponents="1" format="ascii"/>\n')
                    outfile.write('</PCellData>\n')

                    #points
                    outfile.write('<PPoints>\n')
                    outfile.write('<PDataArray type="Float32" NumberOfComponents="3"/>\n')
                    outfile.write('</PPoints>\n')

                    #write the path of each piece for reading it through the .pvtu file
                    for vtufilename in vtufilenames:
                        outfile.write('<Piece Source="%s"/>\n' % (vtufilename,))
                    outfile.write("</PUnstructuredGrid>"+'\n')
                    outfile.write("</VTKFile>")
                    outfile.close()

        self.odb.close()

        print "Total time elapsed: ", time.time() - starttime, "s"

