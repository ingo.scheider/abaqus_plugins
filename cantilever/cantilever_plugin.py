from abaqusGui import getAFXApp, Activator, AFXMode
from abaqusConstants import ALL
import os
thisPath = os.path.abspath(__file__)
thisDir = os.path.dirname(thisPath)

toolset = getAFXApp().getAFXMainWindow().getPluginToolset()
toolset.registerGuiMenuButton(
    buttonText='cantilever beam', 
    object=Activator(os.path.join(thisDir, 'cantileverDB.py')),
    kernelInitString='import microbeam',
    messageId=AFXMode.ID_ACTIVATE,
    icon=None,
    applicableModules=ALL,
    version='N/A',
    author='Ingo Scheider, Hereon',
    description='Creation of a pentagram shaped micro cantilever beam',
    helpUrl='N/A'
)
