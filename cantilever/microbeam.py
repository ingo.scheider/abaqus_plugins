# -*- coding: mbcs -*-
from part import *
from material import *
from section import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from optimization import *
from job import *
from sketch import *
from visualization import *
from connectorBehavior import *
#
# variables in the model
#
#
def createCantilever(diamondShape,cantiwidth,cantiheight,cantilength,loaddistance,indenterLoading,
	precracked,cracklength,crackdistance,supportFlag,supportwidth,supportheight,
	displacement,inputName,meshfactor):
    #
    if precracked:
        # crackdistance=cantilength/50.
        # cracklength=(cantiheight-cantiwidth/2.)/3.
        czheight=cantilength/500.0
    if not indenterLoading:
        loaddistance=cantilength
    #
    # part: endblock
    if supportFlag:
        mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
        mdb.models['Model-1'].sketches['__profile__'].rectangle(point1=(-supportwidth/2., 0.0), 
            point2=(supportwidth/2., -supportheight))
        endblock=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='endblock', type=
            DEFORMABLE_BODY)
        endblock.BaseSolidExtrude(depth=supportwidth/2., sketch=
            mdb.models['Model-1'].sketches['__profile__'])
        del mdb.models['Model-1'].sketches['__profile__']
    #
    # part: cantilever
    #
    mdb.models['Model-1'].ConstrainedSketch(name='cantiSketch', sheetSize=200.0)
    if diamondShape:
        headheight=cantiheight-cantiwidth/2.
        headwidth=cantiwidth-2.*headheight
        mdb.models['Model-1'].sketches['cantiSketch'].Line(point1=(-headwidth/2., 0.0), 
            point2=(headwidth/2., 0.0))
        mdb.models['Model-1'].sketches['cantiSketch'].Line(point1=(headwidth/2., 0.0), 
            point2=(cantiwidth/2., -headheight))
        mdb.models['Model-1'].sketches['cantiSketch'].Line(point1=(cantiwidth/2., -headheight), 
            point2=(0.0, -cantiheight))
        mdb.models['Model-1'].sketches['cantiSketch'].Line(point1=(0.0, -cantiheight), 
            point2=(-cantiwidth/2., -headheight))
        mdb.models['Model-1'].sketches['cantiSketch'].Line(point1=(-cantiwidth/2., -headheight), 
            point2=(-headwidth/2., 0.0))
    else:
        headwidth=cantiwidth
        mdb.models['Model-1'].sketches['cantiSketch'].Line(point1=(-cantiwidth/2., 0.0), 
            point2=(cantiwidth/2., 0.0))
        mdb.models['Model-1'].sketches['cantiSketch'].Line(point1=(cantiwidth/2., 0.0), point2=
            (0.0, -cantiheight))
        mdb.models['Model-1'].sketches['cantiSketch'].Line(point1=(0.0, -cantiheight), point2=
            (-cantiwidth/2., 0.0))
    cantilever=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='cantilever', 
        type=DEFORMABLE_BODY)
    cantilever.BaseSolidExtrude(depth=cantilength, sketch=
        mdb.models['Model-1'].sketches['cantiSketch'])
    del mdb.models['Model-1'].sketches['cantiSketch']

    if indenterLoading:
        # partitioning cantilever
        fineradius=headwidth/4.
        mdb.models['Model-1'].ConstrainedSketch(gridSpacing=0.81, name='__profile__', 
            sheetSize=32.57, transform=cantilever.MakeSketchTransform(sketchPlane=cantilever.faces[0], 
            sketchPlaneSide=SIDE1, sketchUpEdge=cantilever.edges[0], 
            sketchOrientation=RIGHT, origin=(0.0, 0.0, 0.0)))
        cantilever.projectReferencesOntoSketch(filter=COPLANAR_EDGES, 
            sketch=mdb.models['Model-1'].sketches['__profile__'])
        mdb.models['Model-1'].sketches['__profile__'].CircleByCenterPerimeter(center=(
            loaddistance, 0.0), point1=(loaddistance+fineradius, 0.0))
        cantilever.PartitionFaceBySketch(faces=cantilever.faces.findAt((0.,0.,loaddistance),), 
            sketch=mdb.models['Model-1'].sketches['__profile__'], 
            sketchUpEdge=cantilever.edges[0])
        datum=cantilever.DatumPointByCoordinate(coords=(-headwidth/2., 0.0, loaddistance))
        cantilever.PartitionEdgeByPoint(edge=cantilever.edges[3], point=cantilever.datums[datum.id])
        del mdb.models['Model-1'].sketches['__profile__']
    #
    # for a crack:
    if precracked:
        crackdatum1=cantilever.DatumPointByCoordinate(coords=(0.0, 0.0, crackdistance))
        crackdatum2=cantilever.DatumPointByCoordinate(coords=(0.0, 0.0, crackdistance+czheight))
        crackdatum3=cantilever.DatumPointByCoordinate(coords=(0.0, -cracklength, crackdistance))
        crackdirection=cantilever.DatumAxisByPrincipalAxis(principalAxis=YAXIS)
        cracknormal=cantilever.DatumAxisByPrincipalAxis(principalAxis=ZAXIS)
        cantilever.PartitionCellByPlanePointNormal(
            cells=cantilever.cells.findAt(((0.,-cantiheight/2.,cantilength/2.),),), 
            normal=cantilever.datums[cracknormal.id], point=cantilever.datums[crackdatum1.id])
        cantilever.PartitionCellByPlanePointNormal(
            cells=cantilever.cells.findAt(((0.,-cantiheight/2.,cantilength/2.),),), 
            normal=cantilever.datums[cracknormal.id], 
            point=cantilever.datums[crackdatum2.id])
        cantilever.PartitionCellByPlanePointNormal(
            cells=cantilever.cells.findAt(((0.,-cantiheight/2.,crackdistance+czheight/2.),),), 
            normal=cantilever.datums[crackdirection.id], point=cantilever.datums[crackdatum3.id])
    #
    # assembly
    mdb.models['Model-1'].rootAssembly.DatumCsysByDefault(CARTESIAN)
    asm=mdb.models['Model-1'].rootAssembly
    cantiInstance=asm.Instance(dependent=ON, name='cantilever-1', 
        part=cantilever)
    if supportFlag:
        endblockInst=asm.Instance(dependent=ON, name='endblock-1', 
            part=endblock)
        asm.translate(instanceList=('endblock-1', ), 
            vector=(0.0, 0.0, -supportwidth/2.))
    asm.Set(faces=cantiInstance.faces.findAt((
        (0.0, -cantiheight/2., cantilength), )), name='endface')
    asm.Set(faces=cantiInstance.faces.findAt((
        (0.0, -cantiheight/2., 0.0), )), name='supportface')
    #
    # material
    cantilever.Set(cells=cantilever.cells[:], name='cantilever')
    mdb.models['Model-1'].Material(name='average')
    mdb.models['Model-1'].materials['average'].Hyperelastic(materialType=ISOTROPIC, 
        table=((26000.0, 2.0, 1.25e-5), ), testData=OFF, type=OGDEN, 
        volumetricResponse=VOLUMETRIC_DATA)
    mdb.models['Model-1'].HomogeneousSolidSection(material='average', 
        name='structure', thickness=None)
    cantilever.SectionAssignment(offset=0.0, 
        offsetField='', offsetType=MIDDLE_SURFACE, region=cantilever.sets['cantilever'], 
        sectionName='structure', thicknessAssignment=FROM_SECTION)
    if precracked:
        czcell=cantilever.cells.findAt(((0.,-cantiheight/2.,crackdistance+czheight/2.),),)
        czcellCrack=cantilever.cells.findAt(((0.,-cracklength/2.,crackdistance+czheight/2.),),)
        mdb.models['Model-1'].Material(name='cohesive')
        mdb.models['Model-1'].materials['cohesive'].Elastic(table=((1.0e7, 1.0e7, 1.0e7), ),  
            type=TRACTION)
        mdb.models['Model-1'].CohesiveSection(material='cohesive', name='cohesive', 
            outOfPlaneThickness=None, response=TRACTION_SEPARATION)
        mdb.models['Model-1'].materials['cohesive'].MaxsDamageInitiation(table=((
            1000.0, 1000.0, 1000.0), ))
        mdb.models['Model-1'].materials['cohesive'].maxsDamageInitiation.DamageEvolution(
            table=((2.e-6, ), ), type=DISPLACEMENT)
        cantilever.Set(cells=czcell,name='cohesive')
        cantilever.Set(cells=czcellCrack,name='crack')
        cantilever.SectionAssignment(offset=0.0, 
            offsetField='', offsetType=MIDDLE_SURFACE, region=cantilever.sets['cohesive'], 
            sectionName='cohesive', thicknessAssignment=FROM_SECTION)
        cantilever.SectionAssignment(offset=0.0, 
            offsetField='', offsetType=MIDDLE_SURFACE, region=cantilever.sets['crack'], 
            sectionName='cohesive', thicknessAssignment=FROM_SECTION)
    if supportFlag:
        endblock.Set(cells=endblock.cells[:], name='block')
        endblock.SectionAssignment(offset=0.0, 
            offsetField='', offsetType=MIDDLE_SURFACE, region=
            endblock.sets['block'], 
            sectionName='structure', thicknessAssignment=FROM_SECTION)
    #
    # step
    if indenterLoading or precracked:
        mdb.models['Model-1'].StaticStep(initialInc=0.001, maxInc=0.01, maxNumInc=10000, 
            minInc=1e-12, name='Step-1', nlgeom=ON, previous='Initial')
    else:
        mdb.models['Model-1'].StaticStep(initialInc=0.02, maxInc=0.02, maxNumInc=10000, 
            minInc=1e-12, name='Step-1', nlgeom=ON, previous='Initial')

    # MESHING
    #
    cantiElSize=headwidth*3./meshfactor
    blockElSize=supportwidth/meshfactor
    # meshing block
    if supportFlag:
        endblock.seedPart(deviationFactor=0.1, 
            minSizeFactor=0.1, size=blockElSize)
        endblock.generateMesh()
        asm.regenerate()
    # meshing cantilever    
    cantilever.seedPart(deviationFactor=0.1, 
        minSizeFactor=0.1, size=cantiElSize)
    if indenterLoading:
        cantilever.seedEdgeByNumber(constraint=FINER, 
            edges=cantilever.edges.findAt(((0.,0.,loaddistance-fineradius),),), number=50)
        cantilever.setMeshControls(elemShape=TET, regions=cantilever.cells[:], technique=FREE)
        cantilever.setElementType(elemTypes=(ElemType(
            elemCode=C3D20R, elemLibrary=STANDARD), ElemType(elemCode=C3D15, 
            elemLibrary=STANDARD), ElemType(elemCode=C3D10MH, elemLibrary=STANDARD)), 
            regions=cantilever.sets['cantilever'])
#
    else:
        cantilever.setMeshControls(algorithm=ADVANCING_FRONT, elemShape=HEX, 
            regions=cantilever.cells[:], technique=SWEEP)
        cantilever.setSweepPath(edge=cantilever.edges.findAt((0.,-cantiheight,cantilength/2.),), 
            region=cantilever.cells.findAt((0.,-cantiheight/2.,cantilength/2.),), sense=REVERSE)
#
    if precracked:
        cantilever.setElementType(elemTypes=(ElemType(
            elemCode=COH3D8, elemLibrary=STANDARD), ElemType(elemCode=COH3D6, 
            elemLibrary=STANDARD), ElemType(elemCode=UNKNOWN_TET, 
            elemLibrary=STANDARD)), regions=cantilever.sets['cohesive'])
        cantilever.setElementType(elemTypes=(ElemType(
            elemCode=COH3D8, elemLibrary=STANDARD), ElemType(elemCode=COH3D6, 
            elemLibrary=STANDARD), ElemType(elemCode=UNKNOWN_TET, 
            elemLibrary=STANDARD)), regions=cantilever.sets['crack'])
        if indenterLoading:
            cantilever.setMeshControls(elemShape=WEDGE, regions=czcell,
                technique=SWEEP)
            cantilever.setMeshControls(elemShape=WEDGE, regions=czcellCrack,
                technique=SWEEP)
        else:
            cantilever.setMeshControls(elemShape=HEX, regions=czcell,
                technique=SWEEP)
            cantilever.setMeshControls(elemShape=HEX, regions=czcellCrack,
                technique=SWEEP)
        cantilever.setSweepPath(edge=cantilever.edges.findAt((0.,-cantiheight,crackdistance+czheight/2.),), 
            region=czcell[0], sense=FORWARD)
        cantilever.seedEdgeBySize(constraint=FINER, deviationFactor=0.1, 
            edges=cantilever.edges.findAt(((0.0, -cracklength, crackdistance),),), 
            minSizeFactor=0.1, size=cantiElSize/2.)
        #
        # surface ligament
        cantilever.seedEdgeByBias(biasMethod=SINGLE, constraint=FINER, 
            end2Edges=cantilever.edges.findAt(
            ((headwidth/2.+headheight*0.9, -headheight*0.9, crackdistance), )), 
            maxSize=cantiElSize, minSize=cantiElSize/4.)
        cantilever.seedEdgeByBias(biasMethod=SINGLE, constraint=FINER, 
            end1Edges=cantilever.edges.findAt( 
            ((-headwidth/2.-headheight*0.9, -headheight*0.9, crackdistance), )), 
            maxSize=cantiElSize, minSize=cantiElSize/4.)
        # crack flancs
        cantilever.seedEdgeBySize(constraint=FINER, deviationFactor=0.1, 
            edges=cantilever.edges.findAt(
            ((headwidth/2.+headheight*0.1, -headheight*0.1, crackdistance), )), 
            minSizeFactor=0.1, size=cantiElSize/2.)
        cantilever.seedEdgeBySize(constraint=FINER, deviationFactor=0.1, 
            edges=cantilever.edges.findAt(
            ((-headwidth/2.-headheight*0.1, -headheight*0.1, crackdistance), )), 
            minSizeFactor=0.1, size=cantiElSize/2.)
        #
    cantilever.generateMesh()
    if precracked:
        cantilever.deleteMesh(regions=cantilever.cells.findAt(((0.0, 
            -cracklength/2., crackdistance+czheight/2.), )))
    # Save by scheider on 2013_12_16-11.51.57; build 6.13-1 2013_05_16-04.28.56 126354
    asm.regenerate()

    #
    # boundary condition + tie
    if supportFlag:
        mdb.models['Model-1'].Tie(adjust=ON, 
            master=Region(side1Faces=endblockInst.faces.findAt(((0.,-cantiheight/2.,0.),),)), 
            name='Constraint-1', positionToleranceMethod=COMPUTED, 
            slave=Region(side1Faces=cantiInstance.faces.findAt(((0.,-cantiheight/2.,0.),),)), 
            thickness=ON, tieRotations=ON)
        mdb.models['Model-1'].EncastreBC(createStepName='Step-1', localCsys=None, 
            name='encastre', region=Region(
            faces=endblockInst.faces.getSequenceFromMask(mask=('[#27 ]', ), )))
    else:
        mdb.models['Model-1'].EncastreBC(createStepName='Step-1', localCsys=None, name='encastre', 
            region=Region(faces=cantiInstance.faces.findAt(((0.,-cantiheight/2.,0.),),)) )
        
    asm.regenerate()
    #
    #
    # Save by scheider on 2013_12_16-12.03.42; build 6.13-1 2013_05_16-04.28.56 126354
    #
    # indenter
    #
    if indenterLoading:
        angle=65.27
        faces=3
        beta=360./faces
        radius=headwidth/2.
        realheight=radius/tan(degreeToRadian(angle))
        height=realheight*1.1
        print "indenterheight=",height
        indentermeshsize=radius/5.
        mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
        mdb.models['Model-1'].sketches['__profile__'].CircleByCenterPerimeter(center=(0., 0.), 
            point1=(radius, 0.0))
        berkovichBase=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='berkovichBase', 
            type=DEFORMABLE_BODY)
        berkovichBase.BaseSolidExtrude(depth=height, sketch=mdb.models['Model-1'].sketches['__profile__'])
        del mdb.models['Model-1'].sketches['__profile__']

        mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
        mdb.models['Model-1'].sketches['__profile__'].rectangle(point1=(-2*radius, -2*radius), 
            point2=(2*radius, 2*radius))
        cuttingBody=mdb.models['Model-1'].Part(dimensionality=THREE_D, name='cuttingBody', type=DEFORMABLE_BODY)
        cuttingBody.BaseSolidExtrude(depth=2*height, sketch=mdb.models['Model-1'].sketches['__profile__'])
        del mdb.models['Model-1'].sketches['__profile__']
        # ASSEMBLY
        berk0=asm.Instance(dependent=ON, name='berk0-1', part=berkovichBase)
        cut1=asm.Instance(dependent=ON, name='cuttingBody-1', part=cuttingBody)
        cut1.translate(vector=(0.0, 0.0, -2*height))
        cut1.rotateAboutAxis(angle=(90.-angle), axisDirection=(1.0, 0.0, 0.0), 
            axisPoint=(0.0, 0.0, 0.0))
        asm.InstanceFromBooleanCut(cuttingInstances=(cut1, ), 
            instanceToBeCut=asm.instances['berk0-1'], name='berk1', originalInstances=DELETE)

        for face in range(1,faces):
            rotationAxis=asm.DatumAxisByRotation(angle=face*beta, axis=asm.datums[1].axis3, 
                line=asm.datums[1].axis1)
            cut1=asm.Instance(dependent=ON, name='cuttingBody-1', part=cuttingBody)
            cut1.translate(vector=(0.0, 0.0, -2*height))
            cut1.rotateAboutAxis(angle=(90.-angle), axisDirection=asm.datums[rotationAxis.id].direction,
                axisPoint=(0.0, 0.0, 0.0))
            asm.InstanceFromBooleanCut(cuttingInstances=(cut1, ), 
                instanceToBeCut=asm.instances['berk'+repr(face)+'-1'], name='berk'+repr(face+1), originalInstances=DELETE)

        berkovich=mdb.models['Model-1'].parts['berk'+repr(faces)]
        berkovichInstance=asm.instances['berk'+repr(faces)+'-1']
        berkovichInstance.translate(vector=(0.0, 0.0, loaddistance))
        berkovichInstance.rotateAboutAxis(angle=-90., axisDirection=(1.0, 0.0, 0.0), 
            axisPoint=(0.0, 0.0, loaddistance))

        asm.Set(name='loadface', faces=berkovichInstance.faces.getByBoundingBox(yMin=height*0.99) )
        asm.Surface(name='IndenterSurf', side1Faces=berkovichInstance.faces.getByBoundingBox(yMax=height*0.99) )

        # indenter material
        mdb.models['Model-1'].Material(name='dummy')
        mdb.models['Model-1'].materials['dummy'].Elastic(table=((1000000.0, 0.3), ))
        mdb.models['Model-1'].HomogeneousSolidSection(material='dummy', name='dummy', 
            thickness=None)
        berkovich.Set(cells=berkovich.cells[:], name='dummy')
        berkovich.SectionAssignment(offset=0.0, 
            offsetField='', offsetType=MIDDLE_SURFACE, region=berkovich.sets['dummy'], sectionName='dummy',
            thicknessAssignment=FROM_SECTION)
        asm.regenerate()
        #
        # contact
        mdb.models['Model-1'].ContactProperty('indent')
        mdb.models['Model-1'].interactionProperties['indent'].NormalBehavior(
            allowSeparation=ON, constraintEnforcementMethod=DEFAULT, 
            pressureOverclosure=HARD)
        mdb.models['Model-1'].interactionProperties['indent'].TangentialBehavior(
            dependencies=0, directionality=ISOTROPIC, elasticSlipStiffness=None, 
            formulation=PENALTY, fraction=0.005, maximumElasticSlip=FRACTION, 
            pressureDependency=OFF, shearStressLimit=None, slipRateDependency=OFF, 
            table=((0.1, ), ), temperatureDependency=OFF)
        asm.Surface(name='cantiSurface', 
            side1Faces=asm.instances['cantilever-1'].faces.findAt(((0.,0.,loaddistance),),))
        mdb.models['Model-1'].SurfaceToSurfaceContactStd(adjustMethod=NONE, 
            clearanceRegion=None, createStepName='Step-1', datumAxis=None, 
            initialClearance=OMIT, interactionProperty='indent', 
            master=asm.surfaces['cantiSurface'], name='indent', 
            slave=asm.surfaces['IndenterSurf'], sliding=FINITE, thickness=ON)
    #
    # loading
    #
    if indenterLoading:
        asm.ReferencePoint(point=berkovichInstance.vertices[1])
    else:
        asm.ReferencePoint(point=cantiInstance.vertices.findAt( (0.,-cantiheight,cantilength), ))
    refPointId=asm.referencePoints.keys()[0]
    asm.Set(name='loadpoint', referencePoints=(asm.referencePoints[refPointId], ) )
    if indenterLoading:
        asm.Surface(name='loadface', side1Faces=berkovichInstance.faces.getByBoundingBox(yMin=height*0.99))
        asm.Set(name='sidePoint', 
            vertices=cantiInstance.vertices.findAt(((-headwidth/2., 0.0, loaddistance),),) )

        # indenter meshing
        indentElSize=radius/4.
        berkovich.seedPart(deviationFactor=0.1, minSizeFactor=0.1, size=indentElSize)
        berkovich.setMeshControls(elemShape=TET, 
            regions=berkovich.cells[:], technique=FREE)
        berkovich.setElementType(elemTypes=(ElemType(
            elemCode=C3D8, elemLibrary=STANDARD), ElemType(elemCode=C3D6, 
            elemLibrary=STANDARD), ElemType(elemCode=C3D4, elemLibrary=STANDARD)), 
            regions=((berkovich.cells[:]),))
        berkovich.generateMesh()
    else:
        asm.Surface(name='loadface', side1Faces=cantiInstance.faces.findAt(((0.,-cantiheight/2.,loaddistance),),) )
    mdb.models['Model-1'].Coupling(controlPoint=asm.sets['loadpoint'], couplingType=KINEMATIC, 
        influenceRadius=WHOLE_SURFACE, localCsys=None, name='Constraint-2', 
        surface=asm.surfaces['loadface'], 
        u1=ON, u2=ON, u3=ON, ur1=ON, ur2=ON, ur3=ON)
    mdb.models['Model-1'].DisplacementBC(amplitude=UNSET, createStepName='Step-1', 
        distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=None, name=
        'loadpoint', region=asm.sets['loadpoint'], 
        u1=UNSET, u2=displacement, u3=UNSET, ur1=UNSET, ur2=UNSET, ur3=UNSET)
    asm.regenerate()
    # Save by scheider on 2013_12_16-13.44.32; build 6.13-1 2013_05_16-04.28.56 126354



    mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(variables=('S', 'LE', 'U'))
    mdb.models['Model-1'].historyOutputRequests['H-Output-1'].setValues(rebar=EXCLUDE, 
        region=asm.sets['loadpoint'], sectionPoints=DEFAULT, variables=('RF2', 'U2'))
    if indenterLoading:
        mdb.models['Model-1'].HistoryOutputRequest(createStepName='Step-1', name='H-Output-2', 
            rebar=EXCLUDE, region=asm.sets['sidePoint'], sectionPoints=DEFAULT,
            variables=('U2', ))
    if precracked:
        mdb.models['Model-1'].FieldOutputRequest(createStepName='Step-1', name='F-Output-2', 
            rebar=EXCLUDE, region=asm.allInstances['cantilever-1'].sets['cohesive'], 
            sectionPoints=DEFAULT, variables=('SDEG', 'STATUS', 'MAXSCRT'))
    mdb.Job(atTime=None, contactPrint=OFF, description='', echoPrint=OFF, 
        explicitPrecision=SINGLE, getMemoryFromAnalysis=True, historyPrint=OFF, 
        memory=90, memoryUnits=PERCENTAGE, model='Model-1', modelPrint=OFF, 
        multiprocessingMode=DEFAULT, name=inputName, nodalOutputPrecision=SINGLE
        , numCpus=1, numGPUs=0, queue=None, scratch='', type=ANALYSIS, 
        userSubroutine='', waitHours=0, waitMinutes=0)
    mdb.jobs[inputName].writeInput()
    return

if __name__ == "__main__":
    diamondShape=True
    cantiwidth=0.0078
    cantiheight=0.0057
    cantilength=0.030
    loaddistance=0.030
    supportFlag=True
    indenterLoading=False
    precracked=True
    cracklength=0.0009
    supportwidth=0.02
    supportheight=0.016
    displacement=-0.002
    inputName="pentaFine05"
    meshfactor=35.
    createCantilever(diamondShape,cantiwidth,cantiheight,cantilength,loaddistance,indenterLoading,precracked,cracklength,
        supportFlag,supportwidth,supportheight,displacement,inputName, meshfactor)