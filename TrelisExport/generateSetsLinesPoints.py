import numpy as np

partname='PART-1'
instancename=partname+'-1'
part=mdb.models['FCC_SPHER_325_hollow_DP_UT_incomp'].parts[partname]
asm=mdb.models['FCC_SPHER_325_hollow_DP_UT_incomp'].rootAssembly
instance=asm.instances[instancename]
setlist=['XMIN','YMIN','ZMIN','XMAX','YMAX','ZMAX']
linesets=[]
for i1,set1 in enumerate(setlist):
    nodes1=[part.sets[set1].nodes[i1node].label for i1node in range(len(part.sets[set1].nodes))]
    for set2 in setlist[i1+1:]:
        linename=set1+"_"+set2
        nodes2=[part.sets[set2].nodes[i2node].label for i2node in range(len(part.sets[set2].nodes))]
        linenodes=np.intersect1d(nodes1,nodes2)
        if len(linenodes)>0:
            linesets.append(linename)
            part.SetFromNodeLabels(name=linename,nodeLabels=linenodes)
            print("Created line nodeset ",linename," with ",len(linenodes)," nodes")
pointsets=[]
for i1,set1 in enumerate(linesets):
    nodes1=[part.sets[set1].nodes[i1node].label for i1node in range(len(part.sets[set1].nodes))]
    for set2 in linesets[i1+1:]:
        pointname=set1+"_"+set2
        i
        nodes2=[part.sets[set2].nodes[i2node].label for i2node in range(len(part.sets[set2].nodes))]
        pointnodes=np.intersect1d(nodes1,nodes2)
        if len(pointnodes)>0:
            pointsets.append(pointname)
            part.SetFromNodeLabels(name=pointname,nodeLabels=pointnodes)
            print("Created point nodeset ",pointname," with ",len(pointnodes)," nodes")

